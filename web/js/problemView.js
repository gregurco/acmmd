$(function(){
    var $solutionUploadMode = $('#Solution_upload_mode'),
        $solutionCompiler   = $('#Solution_compiler'),
        $textSolutionDiv    = $('#text_solution_div'),
        $fileSolutionDiv    = $('#file_solution_div'),
        $solutionDiv        = $('#solution_div'),
        file_ext;

    $('#Solution_file').change(function(e){
        file_ext = $(this).val().substring($(this).val().lastIndexOf("."), $(this).val().length);

        switch(file_ext){
            case '.pas':
                $solutionCompiler.val('FPC');
                break;
            case '.c':
                $solutionCompiler.val('GCC');
                break;
            case '.cpp':
                $solutionCompiler.val('G++');
                break;
            case '.pl':
                $solutionCompiler.val('Prolog');
                break;
            case '.java':
                $solutionCompiler.val('Java');
                break;
        }
    });

    $solutionUploadMode.change(function(e) {
        if (this.value === 'file'){
            $textSolutionDiv.hide();
            $fileSolutionDiv.show();
            $solutionDiv.removeClass("col-md-12").addClass('col-md-8');
        }else{
            $fileSolutionDiv.hide();
            $textSolutionDiv.show();
            $solutionDiv.removeClass("col-md-8").addClass('col-md-12');
        }
    }).change();

});