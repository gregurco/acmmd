var GeneratePassword = function(){
    var length   = 7,
        charset  = "abcdefghjknprstuwxzABCDEFGHJKMNPRSTUVWXYZ2345678",
        $userPassword = $("#User_password");

    // Обнуление поля
    $userPassword.val('');

    // Генерация пароля
    for (var i = 0, n = charset.length; i < length; ++i) {
        $userPassword.val($userPassword.val() + charset.charAt(Math.floor(Math.random() * n)));
    }
};

function AddTableRow(jQtable){
    var $this;

    $('#example tr').each(function(row, e) {
        $this = $(this);
        if ($this.css('display') == 'none'){
            $this.css({'display' : 'table-row'});
            if ($this.hasClass('row-data')){
                return false;
            }
        }
    });
}