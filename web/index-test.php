<?php

if (!in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1'))){
    die();
}

if (isset($_POST['clock']) && isset($_POST['ajax'])){
    die(date('H:i:s'));
}

// change the following paths if necessary
require_once(dirname(__FILE__) . '/protected/vendor/yiisoft/yii/framework/yii.php');
$config=dirname(__FILE__).'/protected/config/test.php';

Yii::createWebApplication($config)->run();

