<?php

if (isset($_POST['clock']) && isset($_POST['ajax'])){
    die(date('H:i:s'));
}

// change the following paths if necessary
require_once(dirname(__FILE__) . '/../protected/vendor/yiisoft/yii/framework/yii.php');
$config = dirname(__FILE__) . '/../protected/config/main.php';

require_once('../protected/vendor/autoload.php');

Yii::createWebApplication($config)->run();

