<?php

class ArticleController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'   => array('index','view',),
                'users'     => array('*'),
            ),
            array('deny',  // deny all users
                'users'     => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => MArticle::loadPublicModel($id),
        ));
    }

    /**
     * @throws CHttpException
     */
    public function actionIndex()
    {
        if (!Yii::app()->config->get('ARTICLE.ALLOW')) {
            throw new CHttpException(404, 'Статьи временно недоступны.');
        } else {
            $this->render('index',array(
                'groupArticles' => MArticle::getPublicCollection(),
            ));
        }
    }
}
