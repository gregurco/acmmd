<?php

class NewsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class'     => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/profile/pages'
            // They can be accessed via: index.php?r=profile/page&view=FileName
            'page' =>array(
                'class' => 'CViewAction',
            ),
        );
    }

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'   => array('index','view','captcha'),
				'users'     => array('*'),
			),
			array('deny',  // deny all users
				'users'     => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model = MNews::loadPublicModel($id);
        $newsComment = new NewsComment('create');

        if (Yii::app()->request->hasPost('NewsComment')) {
            $newsComment = MNews::addComment($newsComment, Yii::app()->request->post('NewsComment'), $model);
        }

        $this->render('view', array(
            'model'         => $model,
            'newsComment'   => $newsComment,
            'comments'      => MNews::getPublicComments($model),
        ));
    }

    public function actionIndex()
    {
        $this->render('index', array(
            'dataProvider'  => MNews::getPublicCollection(),
        ));
    }
}
