<?php

class SolutionController extends Controller
{
    public $layout='//layouts/column2';

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view', 'download'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	public function actionIndex($pid = NULL)
	{
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/libs/bootstrap-datepicker/locales/bootstrap-datepicker.en-GB.min.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/libs/bootstrap-datepicker/css/bootstrap-datepicker3.min.css');

        $model = new Solution('searchPlainForUser');
        $model->unsetAttributes();  // clear any default values

        $model->u_id = Yii::app()->user->id;
        if ($pid) {
            $model->p_id = $pid;
        }

        if (Yii::app()->request->hasGet('Solution')) {
            $model->attributes = Yii::app()->request->get('Solution');
        }

        $this->render('index',array(
            'model' => $model,
        ));
	}

	public function actionView($id)
	{
        // Connect HighLighter
        Yii::app()->syntaxhighlighter->addHighlighter();
        //Connect ZeroClipboard
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/libs/ZeroClipboard/ZeroClipboard.min.js');

        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * @param $id
     * @return mixed
     * @throws CHttpException
     */
    public function actionDownload($id){
        $model = $this->loadModel($id);
        return Yii::app()->getRequest()->sendFile($model->problem->name . $model->getExtension(), $model->file_text);
    }

    /**
     * @param $id
     * @return Solution
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Solution::model()->findByAttributes(array('id' => $id, 'u_id' => Yii::app()->user->id));
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}