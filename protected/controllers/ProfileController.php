<?php

class ProfileController extends Controller
{

    public $layout='/layouts/column2';

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/profile/pages'
			// They can be accessed via: index.php?r=profile/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('register','login','captcha', 'view', 'reminder', 'recoveryPassword'),
                'users'=>array('?'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('logout','settings','index','changePassword','changeProfile', 'view'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
                'message'=>'Access Denied.',
            ),
        );
    }

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function actionRegister()
    {
        if (!Yii::app()->config->get('REGISTR.ALLOW')) {
            $this->render('registerClose');
        } else {
            $model=new User('register');

            if (Yii::app()->request->hasPost('User')) {
                $model->attributes=Yii::app()->request->post('User');
                if ($model->save()) {
                    if ($model->email) {
                        $new_user = Yii::app()->request->post('User');
                        $email_text = $this->renderPartial('//email/register', array('modal' => $model, 'password' => $new_user['password']), true);
                        $data = array('text' => $email_text, 'subject' => 'Регистрация', 'to_email' => $model->email);
                        Yii::app()->yiiMandrill->sendMessage($data);
                    }

                    Yii::app()->user->setFlash('login',Yii::t('interface','Регистрация прошла успешно!') . '<br>' . Yii::t('interface','Вы можете авторизоваться как') . ": $model->login.");
                    $this->redirect('login');
                }
            }

            $this->render('register',array(
                'model'=>$model,
            ));
        }
    }

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model = new LoginForm;

		// collect user input data
		if (Yii::app()->request->hasPost('LoginForm')) {
			$model->attributes = Yii::app()->request->post('LoginForm');
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login()) {
                $this->redirect(Yii::app()->homeUrl);
            }
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

    public function actionIndex()
    {
        $this->render('index',array(
            'model'       => $this->loadModel(Yii::app()->user->id),
            'score'       => Solution::userScore(Yii::app()->user->id),
            'problem_arr' => MProfile::getLinkedProblems(),
        ));
    }

    public function actionView($id)
    {
        $this->render('view',array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionChangePassword()
    {
        $model = $this->loadModel(Yii::app()->user->id);
        $model->setScenario('changePassword');

        if (Yii::app()->request->hasPost('User')) {
            $model->attributes = Yii::app()->request->post('User');
            if ($model->save()) {
                Yii::app()->user->setFlash('password', Yii::t('interface', "Пароль был изменен."));
            }
        }

        $model->password = $model->password_repeat = '';

        $this->render('changePassword', array('model' => $model));
    }

    /**
     * @param $id
     * @return User
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=User::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function actionChangeProfile()
    {
        $model=$this->loadModel(Yii::app()->user->id);
        $model->setScenario('changeProfile');

        if (Yii::app()->request->hasPost('User')) {
            $model->attributes = Yii::app()->request->post('User');
            if ($model->validate() && $model->save()) {
                Yii::app()->user->setFlash('profile', Yii::t('interface', "Данные были изменены."));
            }
        }

        $this->render('changeProfile', array('model' => $model));
    }

    public function actionReminder(){
        $model = new User('reminder');

        if(Yii::app()->request->hasPost('User')) {
            $model->attributes = Yii::app()->request->post('User');
            if ($model->validate()) {
                $user = User::model()->findByAttributes(array('email' => $model->email));
                if ($user) {
                    $user->token = uniqid('rmnd_', TRUE);
                    $user->save();

                    $email_text = $this->renderPartial('//email/reminder', array('modal' => $user), true);
                    $data = array('text' => $email_text, 'subject' => 'Восстановление пароля', 'to_email' => $user->email);
                    Yii::app()->yiiMandrill->sendMessage($data);

                    Yii::app()->user->setFlash('login', Yii::t('interface',"Письмо с дальнейшими указаниями было высланно на введенный электронный адрес!"));
                    $this->redirect('login');
                } else {
                    Yii::app()->user->setFlash('reminder', Yii::t('interface',"Не был найден пользователь с введенным электронным адресом!"));
                }
            }
        }

        $this->render('reminder', array('model' => $model));
    }

    public function actionRecoveryPassword($token){
        $model = User::model()->findByAttributes(array('token' => $token));

        if (!$model) {
            throw new CHttpException(404,'The specified token cannot be found.');
        } else {
            $model->setScenario('recoveryPassword');
            if(Yii::app()->request->hasPost('User')) {
                $model->attributes=Yii::app()->request->post('User');
                if ($model->validate()) {
                    $model->token = NULL;
                    if ($model->save()) {
                        Yii::app()->user->setFlash('login', Yii::t('interface',"Пароль был удачно заменен. Теперь вы можете авторизироваться"));
                        $this->redirect($this->createUrl('profile/login'));
                    }
                }
            }

            $this->render('recoveryPassword', array('model' => $model));
        }
    }

}