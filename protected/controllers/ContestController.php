<?php

class ContestController extends Controller
{
	public function actionIndex()
	{
        $model=new Contest('search');
        $model->unsetAttributes();  // clear any default values
        if (Yii::app()->request->hasGet('Contest')) {
            $model->attributes = Yii::app()->request->get('Contest');
        }

        $this->render('index',array(
            'model'=>$model,
        ));
	}

    public function actionView($id)
    {
        $contest = MContest::loadPublicContest($id);

        $contest_user = ContestUser::model()->findByAttributes(array('contest_id' => $contest->id, 'user_id' => Yii::app()->getUser()->getId()));

        $contest_problems = ContestProblem::model()->findAllByAttributes(array('contest_id' => $contest->id, 'grade' =>$contest_user->grade));

        $this->render('view',array(
            'contest'           => $contest,
            'contest_user'      => $contest_user,
            'contest_problems'  => $contest_problems,
            'accepted_problems' => MContest::countAcceptedProblems($contest_problems),
        ));
    }

    public function actionViewProblem($id){
        $contest_problem = MContest::loadPublicContestProblem($id);
        $contest_user = MContest::loadPublicContestUserByContestProblem($contest_problem);

        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/problemView.js');
        $this->layout='//layouts/column2';

        $modelForm = new Solution('sendSolutionText');

        if (Yii::app()->request->hasPost('Solution') && Yii::app()->config->get('SEND.SOLUTION')){
            $modelForm->attributes = Yii::app()->request->getPost('Solution');
            if ($modelForm->upload_mode == 'file') {
                $modelForm->file_solution = CUploadedFile::getInstance($modelForm, 'file_solution');
                $modelForm->scenario = 'sendSolutionFile';
            }
            $modelForm->p_id = $contest_problem->problem->id;
            if ($modelForm->validate()){
                $modelForm->createNewSolution(true, $contest_problem->id);
                $this->redirect(array('contest/view','id'=>$contest_problem->contest_id));
            }
        }

        $this->render('view_problem',array(
            'contest_problem'   => $contest_problem,
            'sendForbidden'     => MProblem::getSendForbiddenMessage(),
            'modelForm'         => $modelForm,
        ));
    }
}