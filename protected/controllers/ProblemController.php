<?php

class ProblemController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model = MProblem::loadPublicModel($id);
        $modelForm = new Solution('sendSolutionText');

        if (Yii::app()->request->hasPost('Solution') && Yii::app()->config->get('SEND.SOLUTION')){
            $modelForm->attributes = Yii::app()->request->getPost('Solution');
            if ($modelForm->upload_mode == 'file') {
                $modelForm->file_solution = CUploadedFile::getInstance($modelForm, 'file_solution');
                $modelForm->scenario = 'sendSolutionFile';
            }
            $modelForm->p_id = $model->id;
            if ($modelForm->validate()){
                $modelForm->createNewSolution();
                $this->redirect(array('solution/index','pid'=>$model->id));
            }
        }

        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/problemView.js');
		$this->render('view',array(
			'model'         => $model,
            'modelForm'     => $modelForm,
            'sendForbidden' => MProblem::getSendForbiddenMessage(),
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new Problem('search');
		$model->unsetAttributes();  // clear any default values
		if (Yii::app()->request->hasGet('Problem')) {
            $model->attributes = Yii::app()->request->get('Problem');
        }

		$this->render('index',array(
			'model'=>$model,
		));
	}
}
