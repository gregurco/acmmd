<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 *
 * @property string $type
 * @property string $file
 * @property int $ordinalNumber
 */
class UploadTestForm extends CFormModel
{
	public $type;

	public $file;

	public $ordinalNumber = 1;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			array('type, file, ordinalNumber', 'required'),
			array('file', 'file', 'types'=>'txt', 'safe' => false),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'type'			=> Yii::t('interface', 'Тип'),
			'file'			=> Yii::t('interface', 'Файл'),
			'ordinalNumber'	=> Yii::t('interface', 'Порядковый номер'),
		);
	}

	/**
	 * @param int $max
	 * @return array
	 */
	public function getOrdinalNumberValues($max = 10, $max_pre = 3)
	{
		$result = [];

		for ($i = 1; $i <= $max_pre; $i++) {
			$result['pre_' . $i] = 'pre_' . $i;
		}

		for ($i = 1; $i <= $max; $i++) {
			$result[$i] = $i;
		}

		return $result;
	}
}
