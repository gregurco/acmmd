<?php

/**
 * This is the model class for table "{{contest_solution}}".
 *
 * The followings are the available columns in table '{{contest_solution}}':
 * @property integer $id
 * @property integer $contest_problem_id
 * @property integer $solution_id
 * @property integer $grade
 *
 * The followings are the available model relations:
 * @property Contest $contest
 * @property Solution $solution
 */
class ContestSolution extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{contest_solution}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contest_problem_id, solution_id', 'required'),
			array('contest_problem_id, solution_id,', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, contest_problem_id, solution_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contestProblem' => array(self::BELONGS_TO, 'ContestProblem', 'contest_problem_id'),
			'solution'       => array(self::BELONGS_TO, 'Solution', 'solution_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'                 => 'ID',
			'contest_problem_id' => 'Contest Problem',
			'solution_id'        => 'Solution'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('contest_problem_id',$this->contest_problem_id);
		$criteria->compare('solution_id',$this->solution_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ContestSolution the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
