<?php

/**
 * This is the model class for table "{{solution}}".
 *
 * The followings are the available columns in table '{{solution}}':
 * @property integer $id
 * @property string $u_id
 * @property string $p_id
 * @property integer $time_send
 * @property string $result
 * @property string $tests
 * @property string $log_compile
 * @property integer $status
 * @property string $compiler
 * @property string $file_name
 * @property string $file_text
 * @property string $problemName
 * @property string $userLogin
 * @property string $file_solution
 * @property string $text_solution
 * @property string $upload_mode
 * @property integer $max_result
 * @property Problem $problem
 * @property User $user
 */
class Solution extends CActiveRecord
{
    const STATUS_WAITING_COMPILING = 1;
    const STATUS_COMPILING = 2;
    const STATUS_WAITING_TESTING = 3;
    const STATUS_TESTING = 4;
    const STATUS_FINISH = 5;
    const STATUS_COMPILE_ERROR = 10;

    const STATUS_CONTEST_WAITING_PRE_COMPILING = 21;
    const STATUS_CONTEST_PRE_COMPILING = 22;
    const STATUS_CONTEST_COMPILE_ERROR = 23;
    const STATUS_CONTEST_WAITING_PRE_TESTING = 24;
    const STATUS_CONTEST_PRE_TESTING = 25;
    const STATUS_CONTEST_FAIL_PRE_TESTING = 26;
    const STATUS_CONTEST_ACCEPT_PRE_TESTING = 27;
    const STATUS_CONTEST_WAITING_COMPILING = 28;
    const STATUS_CONTEST_COMPILING = 29;
    const STATUS_CONTEST_WAITING_TESTING = 30;
    const STATUS_CONTEST_TESTING = 31;
    const STATUS_CONTEST_FINISH = 32;

    public $problemName;

    public $userLogin;

    public $file_solution;

    public $text_solution;

    public $upload_mode;

    public $max_result = 0;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'a_solution';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('time_send, status', 'numerical', 'integerOnly'=>true),
			array('u_id, p_id, result', 'length', 'max'=>10),
			array('tests, log_compile, compiler, file_text', 'safe'),
            array('file_solution', 'required', 'on' => 'sendSolutionFile'),
            array('text_solution', 'required', 'on' => 'sendSolutionText'),
            array('compiler, upload_mode', 'required', 'on' => 'sendSolutionText, sendSolutionFile'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, u_id, p_id, time_send, result, tests, log_compile, status, compiler, problemName, userLogin', 'safe', 'on'=>'search'),
			array('id, u_id, p_id, time_send, result, status, compiler, problemName, userLogin', 'safe', 'on'=>'searchPlainForUser'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'user'=>array(self::BELONGS_TO, 'User', 'u_id'),
            'problem'=>array(self::BELONGS_TO, 'Problem', 'p_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'            => 'ID',
			'u_id'          => 'U',
			'p_id'          => 'P',
			'time_send'     => 'Time Send',
			'result'        => 'Result',
			'tests'         => 'Tests',
			'log_compile'   => 'Log Compile',
			'status'        => 'Status',
            'compiler'      => Yii::t('interface','Компилятор'),
            'file_solution' => Yii::t('interface','Файл'),
            'upload_mode'   => Yii::t('interface','Метод'),
            'text_solution' => Yii::t('interface','Решение'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('u_id',$this->u_id);
		$criteria->compare('p_id',$this->p_id);
		$criteria->compare('result',$this->result);
		$criteria->compare('tests',$this->tests);
		$criteria->compare('log_compile',$this->log_compile);
		$criteria->compare('status',$this->status);
        $criteria->compare('compiler',$this->compiler);
        $criteria->compare('problem.name',$this->problemName);
        $criteria->compare('user.login',$this->userLogin);
        $criteria->with = array('problem', 'user');

        $time_send = DateTime::createFromFormat('j.m.Y', $this->time_send);
        if ($time_send) {
            $criteria->addBetweenCondition('time_send', $time_send->getTimestamp(), $time_send->modify('+1 day')->getTimestamp());
        } else {
            $criteria->compare('time_send',$this->time_send);
        }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder'=>array(
                    'id'=>"DESC"
                )
            ),
		));
	}

    /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchPlainForUser()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('u_id',$this->u_id);
		$criteria->compare('p_id',$this->p_id);
		$criteria->compare('result',$this->result);
		$criteria->compare('status',$this->status);
        $criteria->compare('compiler',$this->compiler);
        $criteria->compare('problem.name',$this->problemName);
        $criteria->compare('user.login',$this->userLogin);
        $criteria->with = array('problem', 'user');

        $time_send = DateTime::createFromFormat('j.m.Y', $this->time_send);
        if ($time_send) {
            $criteria->addBetweenCondition('time_send', $time_send->getTimestamp(), $time_send->modify('+1 day')->getTimestamp());
        } else {
            $criteria->compare('time_send',$this->time_send);
        }

        $criteria->addBetweenCondition('status', Solution::STATUS_WAITING_COMPILING, Solution::STATUS_COMPILE_ERROR);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder'=>array(
                    'id'=>"DESC"
                )
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Solution the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @return bool
     */
    protected function beforeSave(){
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->time_send = time();
            } else {
                $this->tests = json_encode($this->tests);
            }
            return true;
        }
        return false;
    }

    /**
     * Method that handle sent solutions
     *
     * @param bool $contest
     * @param null $contest_problem_id
     * @return bool
     */
    public function createNewSolution($contest = false, $contest_problem_id = null){
        $this->u_id = Yii::app()->user->id;
        $this->file_name = $this->filename();

        if ($this->upload_mode == 'file') {
            if (mb_detect_encoding(file_get_contents($this->file_solution->tempName), 'UTF-8', true) === false) {
                $this->file_text = iconv('windows-1251', 'utf-8', file_get_contents($this->file_solution->tempName));
            } else {
                $this->file_text = file_get_contents($this->file_solution->tempName);
            }
        }else{
            $this->file_text = $this->text_solution;
        }

        if ($contest) {
            $this->status = self::STATUS_CONTEST_WAITING_PRE_COMPILING;
        }else{
            $this->status = self::STATUS_WAITING_COMPILING;
        }

        $this->save();

        if ($contest){
            $contest_solution = new ContestSolution();
            $contest_solution->contest_problem_id = $contest_problem_id;
            $contest_solution->solution_id = $this->id;
            $contest_solution->save();
        }

        return true;
    }

    /**
     * @return string
     */
    private function filename(){
        switch($this->compiler){
            case 'FPC':
                return '1.pas';
            case 'GCC':
                return '1.c';
            case 'Prolog':
                return '1.pl';
            case 'G++':
                return '1.cpp';
            case 'Java':
                return 'Main.java';
            default:
                return '1.undefined';
        }
    }

    /**
     * @return string
     */
    public function getExtension(){
        switch($this->compiler){
            case 'FPC':
                return '.pas';
            case 'GCC':
                return '.c';
            case 'Prolog':
                return '.pl';
            case 'G++':
                return '.cpp';
            case 'Java':
                return '.java';
            default:
                return '.undefined';
        }
    }

    /**
     * @return string
     */
    public function getStatusName(){
        switch($this->status){
            case self::STATUS_WAITING_COMPILING:
            case self::STATUS_CONTEST_WAITING_PRE_COMPILING:
                return Yii::t('interface','Ожидание');
            case self::STATUS_COMPILING:
            case self::STATUS_CONTEST_PRE_COMPILING:
                return Yii::t('interface','Компилирование');
            case self::STATUS_WAITING_TESTING:
            case self::STATUS_CONTEST_WAITING_PRE_TESTING:
                return Yii::t('interface','Ожидание тестирования');
            case self::STATUS_CONTEST_WAITING_TESTING:
                return Yii::t('interface','Ожидание тестирования (ф)');
            case self::STATUS_TESTING:
            case self::STATUS_CONTEST_PRE_TESTING:
                return Yii::t('interface','Тестирование');
            case self::STATUS_CONTEST_TESTING:
                return Yii::t('interface','Тестирование (ф)');
            case self::STATUS_FINISH:
            case self::STATUS_CONTEST_FINISH:
                return Yii::t('interface','Завершено');
            case self::STATUS_COMPILE_ERROR:
            case self::STATUS_CONTEST_COMPILE_ERROR:
                return Yii::t('interface','Ошибка компиляции');
            case self::STATUS_CONTEST_ACCEPT_PRE_TESTING:
                return Yii::t('interface','Принято');
            case self::STATUS_CONTEST_FAIL_PRE_TESTING:
                return Yii::t('interface','Неправильное решение');
            case self::STATUS_CONTEST_WAITING_COMPILING:
                return Yii::t('interface','Ожидание компилирования (ф)');
            case self::STATUS_CONTEST_COMPILING:
                return Yii::t('interface','Компилирование (ф)');
            default:
                return "";
        }
    }

    protected function afterFind(){
        $this->tests = json_decode($this->tests, true);

        parent::afterFind();
    }

    /**
     * @param $u_id
     * @param bool $link
     * @return array
     */
    public static function finishedProblem($u_id, $link = true){
        $criteria = new CDbCriteria;
        $criteria->group = 'p_id';
        $criteria->select = 'p_id';
        $criteria->with = array('problem');
        $criteria->condition = '`result` = problem.`tests` * 10 AND `u_id` = ' . $u_id . ' AND `status` BETWEEN ' . Solution::STATUS_WAITING_COMPILING . ' AND ' . Solution::STATUS_COMPILE_ERROR;

        $model = Solution::model()->findAll($criteria);

        $result = array();

        foreach($model as $one){
            if ($link)
                $result[] = CHtml::link($one->p_id, array('problem/view', 'id' => $one->p_id));
            else
                $result[] = $one->p_id;
        }
        return $result;
    }

    /**
     * @param $u_id
     * @param bool $link
     * @return array
     */
    public static function unFinishedProblem($u_id, $link = true){
        $criteria = new CDbCriteria;
        $criteria->compare('u_id', $u_id);
        $criteria->group = 'p_id';
        $criteria->select = 'p_id';
        $criteria->addNotInCondition("p_id", Solution::finishedProblem($u_id, false));
        $criteria->addBetweenCondition('status', Solution::STATUS_WAITING_COMPILING, Solution::STATUS_COMPILE_ERROR);

        $model = Solution::model()->findAll($criteria);

        $result = array();

        foreach($model as $one){
            if ($link)
                $result[] = CHtml::link($one->p_id, array('problem/view', 'id' => $one->p_id));
            else
                $result[] = $one->p_id;
        }
        return $result;
    }

    /**
     * @param $u_id
     * @return mixed
     */
    public static function userScore($u_id)
    {
        $sql='SELECT SUM(c.rslt) score FROM (SELECT MAX(s.result) rslt FROM a_solution s WHERE s.u_id = :u_id AND `status` BETWEEN :status_start AND :status_end GROUP BY s.p_id) c';
        $status_start = Solution::STATUS_WAITING_COMPILING;
        $status_end = Solution::STATUS_COMPILE_ERROR;

        $rawData = Yii::app()->db->createCommand($sql);
        $rawData->bindParam('u_id', $u_id, PDO::PARAM_INT);
        $rawData->bindParam('status_start', $status_start, PDO::PARAM_INT);
        $rawData->bindParam('status_end', $status_end, PDO::PARAM_INT);
        $raw = $rawData->queryRow();

        return $raw['score'];
    }

    /**
     * @return array
     */
    public static function listOfCompilers(){
        return array(
            'FPC'   => 'Free Pascal 2.4.4',
            'GCC'   => 'GCC 4.7.2',
            'G++'   => 'G++ 4.7.2',
            'Prolog'=> 'Prolog 5.10.4',
            'Java'  => 'Java 1.6.0_27'
        );
    }

    /**
     * @return string
     */
    public function getLanguageByCompiler(){
        switch ($this->compiler){
            case 'FPC':
                return 'pascal';
            case 'GCC':
                return 'c';
            case 'G++':
                return 'cpp';
            case 'Prolog':
                return 'prolog';
            case 'Java':
                return 'java';
            default:
                return '';
        }
    }

    /**
     * @param $n_row
     * @return string
     */
    public function getTrClassByResult($n_row){
        if (is_array($this->tests) && array_key_exists($n_row, $this->tests)){
            if ($this->tests[$n_row][0] == 'ok'){
                return 'success';
            }else{
//                return 'danger';
            }
        }
        return '';
    }

    /**
     * @param $n_row
     * @return string
     */
    public function getResultStatusName($n_row){
        if (is_array($this->tests) && array_key_exists($n_row, $this->tests)){
            if ($this->tests[$n_row][0] == 'ok') {
                return Yii::t('interface', 'Успешно');
            }elseif ($this->tests[$n_row][0] == 'wrongAnswer'){
                return Yii::t('interface', 'Неправильный ответ');
            }elseif ($this->tests[$n_row][0] == 'outputDoesntExist'){
                return Yii::t('interface', 'Выходной файл не найден');
            }elseif ($this->tests[$n_row][0] == 'memory'){
                return Yii::t('interface', 'Лимит памяти');
            }elseif ($this->tests[$n_row][0] == 'time'){
                return Yii::t('interface', 'Лимит времени');
            }else{
                return $this->tests[$n_row][0];
            }

        }
        return '';
    }

    /**
     * @return string||null
     */
    public function getNotification(){
        if (substr_count($this->file_text, 'readkey')){
            return Yii::t('notification','Внимание! Readkey в коде может быть причиной time limit');
        }
    }
}
