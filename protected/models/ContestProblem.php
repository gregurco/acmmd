<?php

/**
 * This is the model class for table "{{contest_problem}}".
 *
 * The followings are the available columns in table '{{contest_problem}}':
 * @property integer $id
 * @property integer $contest_id
 * @property integer $problem_id
 *
 * The followings are the available model relations:
 * @property Contest $contest
 * @property Problem $problem
 *  @property ContestSolution[] $contestSolution
 */
class ContestProblem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{contest_problem}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contest_id, problem_id, grade', 'required'),
			array('contest_id, problem_id, grade', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, contest_id, problem_id, grade', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contest'           => array(self::BELONGS_TO, 'Contest', 'contest_id'),
			'problem'           => array(self::BELONGS_TO, 'Problem', 'problem_id'),
            'contestSolution'   => array(
                self::HAS_MANY, 'ContestSolution', 'contest_problem_id',
                'with'      => 'solution',
                'condition' => 'solution.u_id = ' . Yii::app()->getUser()->getId(),
                'order'     => 'contestSolution.id ASC'
            ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'        => 'ID',
			'contest_id'=> 'Contest',
			'problem_id'=> 'Problem',
			'grade'     => 'Grade',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('contest_id',$this->contest_id);
		$criteria->compare('problem_id',$this->problem_id);
		$criteria->compare('grade',$this->grade);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ContestProblem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @return Solution
     */
    public function getLastSolution(){
        if (count($this->contestSolution)){
            return $this->contestSolution[count($this->contestSolution) - 1]->solution;
        }
        return false;
    }
}
