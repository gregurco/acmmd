<?php

/**
 * This is the model class for table "{{contest}}".
 *
 * The followings are the available columns in table '{{contest}}':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $start_time
 * @property string $end_time
 * @property integer $hide
 * @property integer $final_test
 *
 * The followings are the available model relations:
 * @property ContestProblem[] $contestProblems
 * @property ContestUser[] $contestUsers
 */
class Contest extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{contest}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title_ru, title_ro', 'required'),
			array('hide, final_test', 'numerical', 'integerOnly'=>true),
			array('title_ru, title_ro', 'length', 'max'=>255),
			array('description_ru, description_ro, start_time, end_time', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title_ru, title_ro, description_ru, description_ro, start_time, end_time, hide, final_test', 'safe', 'on'=>'search'),
		);
	}

    public function beforeSave(){
        if (empty($this->start_time)){
            $this->start_time = null;
        }

        if (empty($this->end_time)){
            $this->end_time = null;
        }

        return parent::beforeSave();
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contestProblems' => array(self::HAS_MANY, 'ContestProblem', 'contest_id'),
			'contestUsers' => array(self::HAS_MANY, 'ContestUser', 'contest_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'description' => 'Description',
			'start_time' => 'Start Time',
			'end_time' => 'End Time',
			'hide' => 'Hide',
			'final_test' => 'Final Test',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title_ru',$this->title_ru,true);
		$criteria->compare('title_ro',$this->title_ro,true);
		$criteria->compare('description_ru',$this->description_ru,true);
		$criteria->compare('description_ro',$this->description_ro,true);
		$criteria->compare('start_time',$this->start_time,true);
		$criteria->compare('end_time',$this->end_time,true);
		$criteria->compare('hide',$this->hide);
		$criteria->compare('final_test',$this->final_test);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchFrontend()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title_ru',$this->title_ru,true);
		$criteria->compare('title_ro',$this->title_ro,true);
		$criteria->compare('hide',0);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * @return string
     */
    public function getTitle(){
        return $this->{'title_' . Yii::app()->getUser()->getLanguage()};
    }

    /**
     * @return string
     */
    public function getDescription(){
        return $this->{'description_' . Yii::app()->getUser()->getLanguage()};
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
