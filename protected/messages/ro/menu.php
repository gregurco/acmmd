<?php
return array (
    'Новости' => 'Noutăți',
    'Личный кабинет' => 'Birou privat',
    'Задачи' => 'Sarcini',
    'Решения' => 'Soluțiile',
    'Олимпиады' => 'Olimpiade',
    'Статьи' => 'Articole',
    'Рейтинг' => 'Clasament',
    'Стартовая' => 'Pagina principală',
    'Пользователи' => 'Membrii',
    'Настройки' => 'Setăti',
    'Войти' => 'Logare',
    'Регистрация' => 'Registrare',
    'Статистика' =>'Statictică',
    'Выйти' => 'Ieșire',
);
?>