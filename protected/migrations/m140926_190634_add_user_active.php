<?php

class m140926_190634_add_user_active extends CDbMigration
{
    public function up()
    {
        $this->addColumn('a_user', 'active', 'tinyint NOT NULL default "1" ');
    }

    public function down()
    {
        $this->dropColumn('a_user', 'active');
    }
}