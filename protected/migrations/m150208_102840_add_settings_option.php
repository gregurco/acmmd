<?php

class m150208_102840_add_settings_option extends CDbMigration
{
    public function up()
    {
        $this->insert('a_config', array(
            'param' => 'DISABLE.BANNERS',
            'value' => '0',
            'label' => 'Отключение баннеров',
            'type' => 'boolean'
        ));
    }

    public function down()
    {
        $this->delete('a_config', 'param = "DISABLE.BANNERS"');
    }
}