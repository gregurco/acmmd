<?php

class m140820_075809_init extends CDbMigration
{
	public function safeUp()
	{
        $this->createTable('a_article', array(
            'id' => 'pk',
            'g_id' => 'int NOT NULL',
            'title_ru' => 'string',
            'title_ro' => 'string',
            'text_ru' => 'text',
            'text_ro' => 'text',
            'hide' => 'tinyint NOT NULL DEFAULT 0'
        ), "DEFAULT CHARSET=utf8");

        $this->createTable('a_config', array(
            'id' => 'pk',
            'param' => 'string NOT NULL',
            'value' => 'text',
            'label' => 'string',
            'type' => 'string'
        ), "DEFAULT CHARSET=utf8");

            $this->insert('a_config', array(
                'param' => 'REGISTR.ALLOW',
                'value' => '1',
                'label' => 'Открытая регистрация',
                'type' => 'boolean'
            ), "DEFAULT CHARSET=utf8");

            $this->insert('a_config', array(
                'param' => 'COMMENT.ALLOW',
                'value' => '1',
                'label' => 'Открытое комментирование',
                'type' => 'boolean'
            ), "DEFAULT CHARSET=utf8");

            $this->insert('a_config', array(
                'param' => 'COMMENT.VALIDATE.ALLOW',
                'value' => '1',
                'label' => 'Публикация комментариев без проверки',
                'type' => 'boolean'
            ), "DEFAULT CHARSET=utf8");

            $this->insert('a_config', array(
                'param' => 'ARTICLE.ALLOW',
                'value' => '1',
                'label' => 'Публикация статей',
                'type' => 'boolean'
            ), "DEFAULT CHARSET=utf8");

            $this->insert('a_config', array(
                'param' => 'SEND.SOLUTION',
                'value' => '1',
                'label' => 'Отправка решений',
                'type' => 'boolean'
            ), "DEFAULT CHARSET=utf8");

        $this->createTable('a_groupArticle', array(
            'id' => 'pk',
            'title_ru' => 'string',
            'title_ro' => 'text',
            'hide' => 'tinyint NOT NULL DEFAULT 0'
        ), "DEFAULT CHARSET=utf8");

        $this->createTable('a_news', array(
            'id' => 'pk',
            'title_ru' => 'string',
            'title_ro' => 'string',
            'text_ru' => 'text',
            'text_ro' => 'text',
            'create' => 'int NOT NULL',
            'hide' => 'tinyint NOT NULL DEFAULT 0'
        ), "DEFAULT CHARSET=utf8");

        $this->createTable('a_newsComment', array(
            'id' => 'pk',
            'n_id' => 'int NOT NULL',
            'name' => 'string',
            'u_id' => 'int',
            'create' => 'int',
            'text' => 'text',
            'hide' => 'tinyint NOT NULL DEFAULT 0',
        ), "DEFAULT CHARSET=utf8");

        $this->createTable('a_problem', array(
            'id' => 'pk',
            'name_ru' => 'string',
            'name_ro' => 'string',
            'description_ru' => 'text',
            'description_ro' => 'text',
            'input_ru' => 'text',
            'input_ro' => 'text',
            'output_ru' => 'text',
            'output_ro' => 'text',
            'tests' => 'int unsigned DEFAULT 10',
            'limit_time' => 'double unsigned',
            'limit_memory' => 'double unsigned',
            'examples' => 'text',
            'hide' => 'tinyint NOT NULL DEFAULT 0'
        ), "DEFAULT CHARSET=utf8");

        $this->createTable('a_solution', array(
            'id' => 'pk',
            'u_id' => 'int',
            'p_id' => 'int',
            'time_send' => 'int',
            'result' => 'int unsigned DEFAULT 0',
            'tests' => 'text',
            'log_compile' => 'text',
            'status' => 'int NOT NULL DEFAULT 1',
            'compiler' => 'string NOT NULL',
            'file_name' => 'string NOT NULL',
            'file_text' => 'text'
        ), "DEFAULT CHARSET=utf8");

        $this->createTable('a_user', array(
            'id' => 'pk',
            'login' => 'string NOT NULL',
            'password' => 'string',
            'email' => 'string',
            'name' => 'string',
            'surname' => 'string',
            'admin' => 'tinyint NOT NULL DEFAULT 0',
            'time_register' => 'int',
            'time_last_active' => 'int'
        ), "DEFAULT CHARSET=utf8");

        $this->addForeignKey('FK_article_groupArticle', 'a_article', 'g_id', 'a_groupArticle', 'id');
        $this->addForeignKey('FK_newsComment_news', 'a_newsComment', 'n_id', 'a_news', 'id');
        $this->addForeignKey('FK_newsComment_user', 'a_newsComment', 'u_id', 'a_user', 'id');
        $this->addForeignKey('FK_solution_user', 'a_solution', 'u_id', 'a_user', 'id');
        $this->addForeignKey('FK_solution_problem', 'a_solution', 'p_id', 'a_problem', 'id');
	}

	public function safeDown()
	{
        $this->dropTable('a_article');
        $this->dropTable('a_config');
        $this->dropTable('a_groupArticle');
        $this->dropTable('a_news');
        $this->dropTable('a_newsComment');
        $this->dropTable('a_problem');
        $this->dropTable('a_solution');
        $this->dropTable('a_user');
	}
}