<?php

class m150407_191649_rename_create_in_news extends CDbMigration
{
	public function up()
	{
        $this->renameColumn('{{news}}', 'create', 'created');
        $this->renameColumn('{{newsComment}}', 'create', 'created');
	}

	public function down()
	{
        $this->renameColumn('{{news}}', 'created', 'create');
        $this->renameColumn('{{newsComment}}', 'created', 'create');
	}
}