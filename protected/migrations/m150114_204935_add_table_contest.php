<?php

class m150114_204935_add_table_contest extends CDbMigration
{
	public function up()
	{
		$this->createTable('a_contest', array(
			'id' 			    => 'pk',
			'title_ru' 		    => 'string NOT NULL',
			'title_ro' 	        => 'string NOT NULL',
			'description_ru' 	=> 'text',
			'description_ro' 	=> 'text',
			'start_time' 	    => 'datetime',
			'end_time' 		    => 'datetime',
			'hide'			    => 'tinyint'
		), "DEFAULT CHARSET=utf8");

		$this->createTable('a_contest_problem', array(
			'id'		 => 'pk',
			'contest_id' => 'int NOT NULL',
			'problem_id' => 'int NOT NULL',
			'grade' => 'int NOT NULL',
		), "DEFAULT CHARSET=utf8");

		$this->createTable('a_contest_user', array(
			'id'		 => 'pk',
			'contest_id' => 'int NOT NULL',
			'user_id' 	 => 'int NOT NULL',
			'grade' 	 => 'int NOT NULL',
		), "DEFAULT CHARSET=utf8");

		$this->addForeignKey('fk_a_contest_problem_contest_id_a_contest_id', 'a_contest_problem', 'contest_id', 'a_contest', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_a_contest_problem_problem_id_a_problem_id', 'a_contest_problem', 'problem_id', 'a_problem', 'id', 'CASCADE', 'CASCADE');

		$this->addForeignKey('fk_a_contest_user_contest_id_a_contest_id', 'a_contest_user', 'contest_id', 'a_contest', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_a_contest_user_user_id_a_user_id', 'a_contest_user', 'user_id', 'a_user', 'id', 'CASCADE', 'CASCADE');
	}

	public function down()
	{
		$this->dropForeignKey('fk_a_contest_problem_contest_id_a_contest_id', 'a_contest_problem');
		$this->dropForeignKey('fk_a_contest_problem_problem_id_a_problem_id', 'a_contest_problem');

		$this->dropForeignKey('fk_a_contest_user_contest_id_a_contest_id', 'a_contest_user');
		$this->dropForeignKey('fk_a_contest_user_user_id_a_user_id', 'a_contest_user');

		$this->dropTable('a_contest');
		$this->dropTable('a_contest_problem');
		$this->dropTable('a_contest_user');
	}
}