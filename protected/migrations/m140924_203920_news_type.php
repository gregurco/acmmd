<?php

class m140924_203920_news_type extends CDbMigration
{
	public function up()
	{
        $this->addColumn('a_news', 'type', 'ENUM("default", "primary", "success", "info", "warning", "danger") NOT NULL default "default" ');
	}

	public function down()
	{
        $this->dropColumn('a_news', 'type');
	}
}