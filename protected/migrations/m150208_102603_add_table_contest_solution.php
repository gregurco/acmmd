<?php

class m150208_102603_add_table_contest_solution extends CDbMigration
{
	public function up()
	{
        $this->createTable('a_contest_solution', array(
            'id'		 => 'pk',
            'contest_problem_id' => 'int NOT NULL',
            'solution_id' => 'int NOT NULL',
        ), "DEFAULT CHARSET=utf8");

        $this->addForeignKey('fk_a_contest_solution_contest_problem_id_a_contest_id', 'a_contest_solution', 'contest_problem_id', 'a_contest_problem', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_a_contest_solution_solution_id_a_solution_id', 'a_contest_solution', 'solution_id', 'a_solution', 'id', 'CASCADE', 'CASCADE');
	}

	public function down()
	{
        $this->dropForeignKey('fk_a_contest_solution_contest_problem_id_a_contest_id', 'a_contest_solution');
        $this->dropForeignKey('fk_a_contest_solution_solution_id_a_solution_id', 'a_contest_solution');

        $this->dropTable('a_contest_solution');
	}
}