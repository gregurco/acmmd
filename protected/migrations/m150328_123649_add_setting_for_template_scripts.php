<?php

class m150328_123649_add_setting_for_template_scripts extends CDbMigration
{
	public function up()
	{
        $this->insert('a_config', array(
            'param' => 'SCRIPT.HEADER',
            'value' => '',
            'label' => 'Script (header)',
            'type' => 'textarea'
        ));

        $this->insert('a_config', array(
            'param' => 'SCRIPT.FOOTER',
            'value' => '',
            'label' => 'Script (footer)',
            'type' => 'textarea'
        ));
	}

	public function down()
	{
        $this->delete('a_config', 'param = "SCRIPT.HEADER"');
        $this->delete('a_config', 'param = "SCRIPT.FOOTER"');
	}
}