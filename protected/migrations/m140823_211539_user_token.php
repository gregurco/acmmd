<?php

class m140823_211539_user_token extends CDbMigration
{
	public function up()
	{
        $this->addColumn('a_user', 'token', 'string');
	}

	public function down()
	{
		$this->dropColumn('a_user', 'token');
	}
}