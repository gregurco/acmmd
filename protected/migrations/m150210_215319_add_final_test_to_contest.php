<?php

class m150210_215319_add_final_test_to_contest extends CDbMigration
{
	public function up()
	{
        $this->addColumn('a_contest', 'final_test', 'tinyint NOT NULL DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('a_contest', 'final_test');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}