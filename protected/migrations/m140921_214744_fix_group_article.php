<?php

class m140921_214744_fix_group_article extends CDbMigration
{
	public function safeUp()
	{
        $this->execute('ALTER TABLE a_groupArticle MODIFY title_ro varchar(255);');
	}

	public function safeDown(){
        $this->execute('ALTER TABLE a_groupArticle MODIFY title_ro text;');
	}
}