<?php

class m140927_210355_add_config_options extends CDbMigration
{
    public function up()
    {
        $this->insert('a_config', array(
            'param' => 'ADMIN.EMAIL',
            'value' => 'admin@acm-md.info',
            'label' => 'E-mail Администрации',
            'type' => 'text'
        ));

        $this->insert('a_config', array(
            'param' => 'MAIN.TITLE',
            'value' => 'Algoritmica',
            'label' => 'Title',
            'type' => 'text'
        ));

        $this->insert('a_config', array(
            'param' => 'MAIN.HEADER',
            'value' => 'Algoritmica',
            'label' => 'Hedaer',
            'type' => 'text'
        ));
    }

    public function down()
    {
        $this->delete('a_config', 'param = "ADMIN.EMAIL"');
        $this->delete('a_config', 'param = "MAIN.TITLE"');
        $this->delete('a_config', 'param = "MAIN.HEADER"');
    }
}