<?php

class m140926_204402_user_code_style extends CDbMigration
{
	public function up()
	{
        $this->addColumn('a_user', 'code_style', 'varchar(15) NOT NULL DEFAULT "Default"');
	}

	public function down()
	{
        $this->dropColumn('a_user', 'code_style');
	}
}