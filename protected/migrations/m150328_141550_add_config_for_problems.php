<?php

class m150328_141550_add_config_for_problems extends CDbMigration
{
	public function up()
	{
        $this->addColumn('a_problem', 'trim_lines', 'tinyint NOT NULL DEFAULT 0');
        $this->addColumn('a_problem', 'empty_last_line', 'tinyint NOT NULL DEFAULT 0');

        $this->setDbConnection(Yii::app()->getComponent('db2'));
        $this->addColumn('c_processing', 'trim_lines', 'tinyint NOT NULL DEFAULT 0');
        $this->addColumn('c_processing', 'empty_last_line', 'tinyint NOT NULL DEFAULT 0');
	}

	public function down()
	{
        $this->dropColumn('a_problem', 'trim_lines');
        $this->dropColumn('a_problem', 'empty_last_line');

        $this->setDbConnection(Yii::app()->getComponent('db2'));
        $this->dropColumn('c_processing', 'trim_lines');
        $this->dropColumn('c_processing', 'empty_last_line');
	}
}