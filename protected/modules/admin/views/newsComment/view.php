<?php
/* @var $this NewsCommentController */
/* @var $model NewsComment */

$this->main_header = Yii::t('interface','View NewsComment #') . ' ' . $model->id;

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')           => $this->createUrl('default/index'),
    Yii::t('interface', 'Список комментариев')  => $this->createUrl('newsComment/index'),
    Yii::t('interface', 'Просмотр комментария') => '#',
);
?>

<?php $this->widget('booster.widgets.TbDetailView', array(
    'data'       => $model,
    'attributes' => array(
        array('name' => 'id', 'label' => Yii::t('interface', 'ID')),
        array('name' => 'n_id', 'label' => Yii::t('interface', 'Новость'), 'value' => $model->news->title),
        array('name' => 'u_id', 'label' => Yii::t('interface', 'Пользователь'), 'value' => $model->user->login),
        array('name' => 'created', 'label' => Yii::t('interface', 'Создан'), 'value' => Yii::app()->dateFormatter->formatDateTime($model->created)),
        array('name' => 'text', 'label' => Yii::t('interface', 'Текст'), 'type'=>'raw'),
        array('name' => 'hide', 'label' => Yii::t('interface', 'Скрыт'), 'value' => ($model->hide)? Yii::t('interface', 'Да') : Yii::t('interface', 'Нет')),
    ),
)); ?>
