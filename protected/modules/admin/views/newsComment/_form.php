<?php
/* @var $this NewsCommentController */
/* @var $model NewsComment */
/* @var $form CActiveForm */
?>

<?php $this->widget('booster.widgets.TbDetailView', array(
    'data'       => $model,
    'attributes' => array(
        array('name' => 'id', 'label' => Yii::t('interface', 'ID')),
        array('type' => 'html', 'label' => Yii::t('interface', 'Новость'), 'value' => $model->news->title),
        array('type' => 'html', 'label' => Yii::t('interface', 'Пользователь'), 'value' => $model->user->login),
        array('type' => 'html', 'label' => Yii::t('interface', 'Пользователь (без авторизации)'), 'value' => $model->name),
        array('name' => 'created', 'label' => Yii::t('interface', 'Создан'), 'value' => Yii::app()->dateFormatter->formatDateTime($model->created)),
    ),
)); ?>


<div class="form">
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'news-comment-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

    <?php echo $form->ckEditorGroup(
        $model,
        'text',
        array(
            'widgetOptions' => array(
                'htmlOptions' => array('style'=>'height: 150px;')
            ),
            'label' => 'Текст комментария:'
        )
    ); ?>

    <?php echo $form->switchGroup($model, 'hide',
        array(
            'label' => 'Скрыта:'
        )
    ); ?>

    <div class="form-group text-center" style="margin-top: 20px;">
        <?php
        $this->widget(
            'booster.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'context'    => 'success',
                'label'      =>  Yii::t('interface','Сохранить')
            )
        );
        ?>

        <?php
        echo CHtml::link(
            Yii::t('interface','Вернуться'),
            $this->createUrl('newsComment/index'),
            array(
                'class' => 'btn btn-danger'
            )
        );
        ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->