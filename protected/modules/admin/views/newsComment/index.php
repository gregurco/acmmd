<?php
/* @var $this NewsCommentController */
/* @var $model NewsComment */

$this->main_header = Yii::t('interface','Manage News Comments');

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')            => $this->createUrl('default/index'),
    Yii::t('interface', 'Список комментариев')   => '#',
);
?>

<?php
$this->renderGridView(array(
    'id'            => 'news-comment-grid',
    'model'         => $model,
    'columns'       => array(
        'id',
        array(
            'name'  => 'newsTitle',
            'value' => '$data->news->title_'.Yii::app()->getUser()->getLanguage(),
        ),
        array(
            'name'  => 'name',
            'value' => '$data->name',
        ),
        array(
            'name'  => 'userLogin',
            'value' => '($data->user)?$data->user->login:""',
        ),
        array(
            'name'  => 'created',
            'value' => 'date("Y-m-d H:i:s",$data->created)',
        ),
        array(
            'name'          => 'hide',
            'value'         => '($data->hide)?"Да":"Нет"',
            'header'        => 'Скрыто',
            'htmlOptions'   => array(
                'style'     => 'text-align: center;',
            ),
            'filter' => array(
                0 => "Нет",
                1 => "Да",
            ),
        ),
        array(
            'htmlOptions' => array('nowrap'=>'nowrap'),
            'class'       => 'booster.widgets.TbButtonColumn',
        ),
    )
));
?>
