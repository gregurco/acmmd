<?php
/* @var $this NewsCommentController */
/* @var $model NewsComment */

$this->main_header = Yii::t('interface','Update NewsComment') . ' ' . $model->id;

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')                => $this->createUrl('default/index'),
    Yii::t('interface', 'Список комментариев')       => $this->createUrl('newsComment/index'),
    Yii::t('interface', 'Редактировать комментарий') => '#',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>