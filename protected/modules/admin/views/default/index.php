<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);

$this->main_header = Yii::t('interface','Статистика');
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><span class="glyphicon glyphicon-envelope"></span> Mandrill</h3>
    </div>
    <div class="panel-body">
        <table class="table table-hover">
            <tr>
                <td style="width: 50%;">Логин</td>
                <td><?php echo Yii::app()->yiiMandrill->getUserInfoProperty('username');?></td>
            </tr>
            <tr>
                <td>Репутация</td>
                <td><?php echo Yii::app()->yiiMandrill->getUserInfoProperty('reputation');?></td>
            </tr>
            <tr>
                <td>Лимит писем</td>
                <td>12 000</td>
            </tr>
            <tr>
                <td>Отправленно писем (месяц)</td>
                <td><?php echo Yii::app()->yiiMandrill->getUserInfoProperty('stats->last_30_days->sent');?></td>
            </tr>
            <tr>
                <td>Отправленно писем (неделя)</td>
                <td><?php echo Yii::app()->yiiMandrill->getUserInfoProperty('last_7_days->sent');?></td>
            </tr>
            <tr>
                <td>Отправленно писем (сегодня)</td>
                <td><?php echo Yii::app()->yiiMandrill->getUserInfoProperty('stats->today->sent');?></td>
            </tr>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><span class="glyphicon glyphicon-user"></span> Пользователи</h3>
    </div>
    <div class="panel-body">
        <table class="table table-hover">
            <tr>
                <td style="width: 50%;">Всего пользователей</td>
                <td><?php echo $totalUsers;?></td>
            </tr>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><span class="glyphicon glyphicon-briefcase"></span> Задачи</h3>
    </div>
    <div class="panel-body">
        <table class="table table-hover">
            <tr>
                <td style="width: 50%;">Всего задач</td>
                <td><?php echo $totalProblems;?></td>
            </tr>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><span class="glyphicon glyphicon-file"></span> Статьи</h3>
    </div>
    <div class="panel-body">
        <table class="table table-hover">
            <tr>
                <td style="width: 50%;">Всего статей</td>
                <td><?php echo $totalArticles;?></td>
            </tr>
        </table>
    </div>
</div>