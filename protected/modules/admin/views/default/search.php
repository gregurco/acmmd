<?php
/**
 * @var $this NewsController
 * @var $news_model News
 * @var $article_model Article
 * @var $problem_model Problem
 */

$this->main_header = Yii::t('interface','Поиск');

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика') => $this->createUrl('default/index'),
    Yii::t('interface', 'Поиск')      => '#'
);
?>

<h4 class="text-center"><?php echo Yii::t('interface','Новости'); ?></h4>
<?php
$this->renderGridView(array(
    'id'            => 'news-grid',
    'model'         => $news_model,
    'columns'       => array(
        'id',
        array(
            'name'   => 'title_ru',
            'value'  => '$data->title_ru',
            'header' => 'Название (рус.)',
        ),
        array(
            'name'   => 'title_ro',
            'value'  => '$data->title_ro',
            'header' => 'Название (рум.)',
        ),
        array(
            'name'          => 'created',
            'value'         => 'date("j.m.Y",$data->created)',
            'header'        => 'Создано',
            'htmlOptions'   => array(
                'style'     => 'text-align: center;',
            ),
        ),
        array(
            'name'          => 'hide',
            'value'         => '($data->hide)?"Да":"Нет"',
            'header'        => 'Скрыто',
            'htmlOptions'   => array(
                'style'     => 'text-align: center;',
            ),
            'filter' => array(
                0 => "Нет",
                1 => "Да",
            ),
        ),
        array(
            'htmlOptions' => array('nowrap'=>'nowrap'),
            'class'       => 'booster.widgets.TbButtonColumn',
        ),
    )
));
?>

<h4 class="text-center"><?php echo Yii::t('interface','Статьи'); ?></h4>
<?php
$this->renderGridView(array(
    'id'            => 'article-grid',
    'model'         => $article_model,
    'columns'       => array(
        'id',
        array(
            'name'   => 'title_ru',
            'value'  => '$data->title_ru',
            'header' => 'Название (рус.)',
        ),
        array(
            'name'   => 'title_ro',
            'value'  => '$data->title_ro',
            'header' => 'Название (рум.)',
        ),
        array(
            'name'   => 'hide',
            'value'  => '($data->hide)?"Да":"Нет"',
            'header' => 'Скрыто',
            'htmlOptions' => array(
                'style' => 'text-align: center;',
            ),
            'filter' => array(
                0 => "Нет",
                1 => "Да",
            ),
        ),
        array(
            'name' => 'groupTitle',
            'value' => '$data->groupArticle->title_'.Yii::app()->getUser()->getLanguage(),
        ),
        array(
            'htmlOptions' => array('nowrap'=>'nowrap'),
            'class'=>'booster.widgets.TbButtonColumn',
        ),
    )
));
?>

<h4 class="text-center"><?php echo Yii::t('interface','Задачи'); ?></h4>
<?php
$this->renderGridView(array(
    'id'            => 'problem-grid',
    'model'         => $problem_model,
    'dataProvider'  => $problem_model->searchAdmin(),
    'columns'       => array(
        'id',
        'name_ru',
        'name_ro',
        array(
            'name'        => 'hide',
            'value'       => '($data->hide)?"Да":"Нет"',
            'header'      => 'Скрыто',
            'htmlOptions' => array(
                'style'   => 'text-align: center;',
            ),
            'filter' => array(
                0 => "Нет",
                1 => "Да",
            ),
        ),
        array(
            'htmlOptions' => array('nowrap'=>'nowrap'),
            'class'       => 'booster.widgets.TbButtonColumn',
        ),
    )
));
?>
