<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
?>

<div class="form">
    <?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm'
    );
    ?>

		<?php echo $form->textFieldGroup($model,'title_ru',array('size'=>60,'maxlength'=>255, 'label' => 'Заглавие (рус.):')); ?>

		<?php echo $form->textFieldGroup($model,'title_ro',array('size'=>60,'maxlength'=>255, 'label' => 'Заглавие (рум.):')); ?>

        <?php echo $form->ckEditorGroup(
            $model,
            'text_ru',
            array(
                'widgetOptions' => array(
                    'htmlOptions' => array('style'=>'height: 150px;')
                ),
                'label' => 'Текст (рус.):'
            )
        ); ?>

        <?php echo $form->ckEditorGroup(
            $model,
            'text_ro',
            array(
                'widgetOptions' => array(
                    'htmlOptions' => array('style'=>'height: 150px;')
                ),
                'label' => 'Текст (рум.):'
            )
        ); ?>

        <?php echo $form->dropDownListGroup(
            $model,
            'type',
            array(
                'widgetOptions' => array(
                    'data' => MNews::getNewsTypes()
                )
            )
        ); ?>

        <?php echo $form->switchGroup($model, 'hide',
            array(
                'label' => 'Скрыта:'
            )
        ); ?>

        <div class="form-group text-center" style="margin-top: 20px;">
            <?php
            $this->widget(
                'booster.widgets.TbButton',
                array(
                    'buttonType' => 'submit',
                    'context'    => 'success',
                    'label'      => $model->isNewRecord ? Yii::t('interface','Создать') : Yii::t('interface','Сохранить')
                )
            );
            ?>

            <?php
            echo CHtml::link(
                Yii::t('interface','Вернуться'),
                $this->createUrl('news/index'),
                array(
                    'class' => 'btn btn-danger'
                )
            );
            ?>
        </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->