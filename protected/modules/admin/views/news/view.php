<?php
/* @var $this NewsController */
/* @var $model News */

$this->main_header = Yii::t('interface','View News #') . $model->id;

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')       => $this->createUrl('default/index'),
    Yii::t('interface', 'Список новостей')  => $this->createUrl('news/index'),
    Yii::t('interface', 'Просмотр новости') => '#',
);
?>

<?php $this->widget('booster.widgets.TbDetailView', array(
	'data'       => $model,
	'attributes' => array(
        array('name' => 'id', 'label' => Yii::t('interface', 'ID')),
        array('name' => 'title_ru', 'label' => Yii::t('interface', 'Название (ru)')),
        array('name' => 'title_ro', 'label' => Yii::t('interface', 'Название (ro)')),
        array('name' => 'text_ru', 'label' => Yii::t('interface', 'Текст (ru)'), 'type'=>'raw'),
        array('name' => 'text_ro', 'label' => Yii::t('interface', 'Текст (ro)'), 'type'=>'raw'),
        array('name' => 'created', 'label' => Yii::t('interface', 'Создан'), 'value' => Yii::app()->dateFormatter->formatDateTime($model->created)),
        array('name' => 'hide', 'label' => Yii::t('interface', 'Скрыт'), 'value' => ($model->hide)? Yii::t('interface', 'Да') : Yii::t('interface', 'Нет')),
	),
)); ?>
