<?php
/* @var $this NewsController */
/* @var $model News */

$this->main_header = Yii::t('interface','Manage News');

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')      => $this->createUrl('default/index'),
    Yii::t('interface', 'Список новостей') => '#'
);
?>

<?php
$this->renderGridView(array(
    'id'            => 'news-grid',
    'model'         => $model,
    'columns'       => array(
        'id',
        array(
            'name'   => 'title_ru',
            'value'  => '$data->title_ru',
            'header' => 'Название (рус.)',
        ),
        array(
            'name'   => 'title_ro',
            'value'  => '$data->title_ro',
            'header' => 'Название (рум.)',
        ),
        array(
            'name'          => 'created',
            'value'         => 'date("j.m.Y",$data->created)',
            'header'        => 'Создано',
            'htmlOptions'   => array(
                'style'     => 'text-align: center;',
            ),
        ),
        array(
            'name'          => 'hide',
            'value'         => '($data->hide)?"Да":"Нет"',
            'header'        => 'Скрыто',
            'htmlOptions'   => array(
                'style'     => 'text-align: center;',
            ),
            'filter' => array(
                0 => "Нет",
                1 => "Да",
            ),
        ),
        array(
            'htmlOptions' => array('nowrap'=>'nowrap'),
            'class'       => 'booster.widgets.TbButtonColumn',
        ),
    )
));
?>
