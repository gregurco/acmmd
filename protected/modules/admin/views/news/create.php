<?php
/* @var $this NewsController */
/* @var $model News */

$this->main_header = Yii::t('interface','Создание новости');

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')      => $this->createUrl('default/index'),
    Yii::t('interface', 'Список новостей') => $this->createUrl('news/index'),
    Yii::t('interface', 'Создать новость') => '#',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>