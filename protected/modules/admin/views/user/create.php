<?php
/* @var $this UserController */
/* @var $model User */

$this->main_header = Yii::t('interface','Создание пользователя');

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')             => $this->createUrl('default/index'),
    Yii::t('interface', 'Список пользователей')   => $this->createUrl('user/index'),
    Yii::t('interface', 'Создание пользователя')  => '#',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>