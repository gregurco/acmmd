<?php

$this->main_header = Yii::t('interface','Generate users');

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')               => $this->createUrl('default/index'),
    Yii::t('interface', 'Список пользователей')     => $this->createUrl('user/index'),
    Yii::t('interface', 'Генерация пользователей')  => '#',
);
?>

<div class="form text-center">
    <?php echo CHtml::beginForm(); ?>

    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <?php echo CHtml::label('Кол-во', 'number_to_generate'); ?>
            <?php echo CHtml::textField('number_to_generate', '', array('class' => 'form-control')); ?>
        </div>
    </div>

    <div class="row submit" style="margin-top: 25px;">
        <div class="col-md-4 col-md-offset-4">
            <?php echo CHtml::submitButton('Создать', array('class' => 'btn btn-default')); ?>
        </div>
    </div>

    <?php echo CHtml::endForm(); ?>
</div><!-- form -->