<?php
/**
 * @var $this UserController
 * @var $model User
 * @var array $problem_arr
 * @var int $score
 */

$this->main_header = Yii::t('interface','View User #') . $model->id;

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')             => $this->createUrl('default/index'),
    Yii::t('interface', 'Список пользователей')   => $this->createUrl('user/index'),
    Yii::t('interface', 'Просмотр пароля')        => '#',
);
?>

<?php $this->widget('booster.widgets.TbDetailView', array(
    'data'       => $model,
    'attributes' => array(
        array('name' => 'id', 'label' => Yii::t('interface', 'ID')),
        array('name' => 'login', 'label' => Yii::t('interface', 'Логин')),
        array('name' => 'name', 'label' => Yii::t('interface', 'Имя')),
        array('name' => 'surname', 'label' => Yii::t('interface', 'Фамилия')),
        array('name' => 'email', 'label' => Yii::t('interface', 'E-mail')),
        array('name' => 'admin', 'label' => Yii::t('interface', 'Администратор'), 'value' => ($model->admin)? Yii::t('interface', 'Да') : Yii::t('interface', 'Нет')),
        array('name' => 'active', 'label' => Yii::t('interface', 'Активен'), 'value' => ($model->active)? Yii::t('interface', 'Да') : Yii::t('interface', 'Нет')),
        array('name' => 'time_last_active', 'label' => Yii::t('interface', 'Последние действие'), 'value' => Yii::app()->dateFormatter->formatDateTime($model->time_last_active)),
        array('name' => 'time_register', 'label' => Yii::t('interface', 'Время регистрации'), 'value' => Yii::app()->dateFormatter->formatDateTime($model->time_register)),
        array('type' => 'html', 'label' => Yii::t('interface', 'Баллы'), 'value' => $score),
        array('type' => 'html', 'label' => Yii::t('interface', 'Задачи'), 'value' => implode(' ', $problem_arr)),
    ),
)); ?>
