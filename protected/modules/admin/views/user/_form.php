<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm'
    );
    ?>

        <?php echo $form->textFieldGroup($model,'login',array('size'=>60,'maxlength'=>255, 'label' => 'Логин:')); ?>

        <?php if ($model->isNewRecord): ?>
            <?php echo $form->textFieldGroup($model,'password',array('size'=>60,'maxlength'=>255, 'label' => 'Пароль:')); ?>
        <?php endif; ?>

        <?php echo $form->textFieldGroup($model,'name',array('size'=>60,'maxlength'=>255, 'label' => 'Имя:')); ?>

        <?php echo $form->textFieldGroup($model,'email',array('size'=>60,'maxlength'=>255, 'label' => 'Email:')); ?>

        <?php echo $form->textFieldGroup($model,'surname',array('size'=>60,'maxlength'=>255, 'label' => 'Фамилия:')); ?>

        <?php echo $form->switchGroup($model, 'admin',
            array(
                'label' => 'Администратор:'
            )
        ); ?>

        <?php echo $form->switchGroup($model, 'active',
            array(
                'label' => 'Активен:'
            )
        ); ?>


        <div class="form-group text-center" style="margin-top: 20px;">
            <?php
            $this->widget(
                'booster.widgets.TbButton',
                array(
                    'buttonType'=> 'submit',
                    'context'   => 'success',
                    'label'     => $model->isNewRecord ? Yii::t('interface','Создать') : Yii::t('interface','Сохранить')
                )
            );
            ?>

            <?php
            echo CHtml::link(
                Yii::t('interface','Вернуться'),
                $this->createUrl('user/index'),
                array(
                    'class' => 'btn btn-danger'
                )
            );
            ?>
        </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->