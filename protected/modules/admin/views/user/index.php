<?php
/* @var $this UserController */
/* @var $model User */

$this->main_header = Yii::t('interface','Manage Users');

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')           => $this->createUrl('default/index'),
    Yii::t('interface', 'Список пользователей') => '#',
);
?>

<?php
$this->renderGridView(array(
    'id'            => 'user-grid',
    'model'         => $model,
    'columns'       => array(
        'id',
        array(
            'name'   => 'login',
            'value'  => '$data->login',
            'header' => 'Логин',
        ),
        array(
            'name'   => 'email',
            'value'  => '$data->email',
            'header' => 'Email',
        ),
        array(
            'name'   => 'name',
            'value'  => '$data->name',
            'header' => 'Имя',
        ),
        array(
            'name'   => 'surname',
            'value'  => '$data->surname',
            'header' => 'Фамилия',
        ),
        array(
            'name'   => 'time_last_active',
            'value'  => '(date("Y-m-d H:i:s",$data->time_last_active))',
            'header' => 'Последние действие',
        ),
        array(
            'name'          => 'admin',
            'value'         => '($data->admin)?"Да":"Нет"',
            'header'        => 'Администратор',
            'htmlOptions'   => array(
                'style'     => 'text-align: center;',
            ),
            'filter' => array(
                0 => "Нет",
                1 => "Да",
            ),
        ),
        array(
            'type'   =>'html',
            'value'  => 'CHtml::link("Изменить", array("changePassword", "id" => $data->id))',
            'header' => 'Пароль',
        ),
        array(
            'htmlOptions' => array('nowrap'=>'nowrap'),
            'class'       => 'booster.widgets.TbButtonColumn',
        ),
    )
));
?>
