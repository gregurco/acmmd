<?php
/* @var $this UserController */
/* @var $model User */

$this->main_header = Yii::t('interface','Редактировние пароля') . ' ' . $model->login;

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')             => $this->createUrl('default/index'),
    Yii::t('interface', 'Список пользователей')   => $this->createUrl('user/index'),
    Yii::t('interface', 'Редактирование пароля')  => '#',
);
?>

<div class="form">

    <?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm'
    );
    ?>


    <?php echo $form->textFieldGroup($model,'password',array('size'=>60,'maxlength'=>255, 'label' => 'Пароль:')); ?>

    <div class="form-group text-center" style="margin-top: 20px;">
        <?php
        $this->widget(
            'booster.widgets.TbButton',
            array('buttonType' => 'button', 'label' => 'Сгенерировать пароль', 'htmlOptions' => array('onclick' => 'GeneratePassword();'))
        );
        ?>

        <?php
        $this->widget(
            'booster.widgets.TbButton',
            array('buttonType' => 'submit', 'label' => 'Сохранить', 'htmlOptions' => array('style' => 'margin-left: 20px;'))
        );
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->