<?php
/**
 * @var Contest $model
 */

$this->main_header = Yii::t('interface','Manage Contests');

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')     => $this->createUrl('default/index'),
    Yii::t('interface', 'Список олимпиад')  => '#',
);
?>

<?php
$this->renderGridView(array(
    'id'            => 'contest-grid',
    'model'         => $model,
    'columns'       => array(
        'id',
        array(
            'name'   => 'title_ru',
            'value'  => '$data->title_ru',
            'header' => 'Название (рус.)',
        ),
        array(
            'name'   => 'title_ro',
            'value'  => '$data->title_ro',
            'header' => 'Название (рум.)',
        ),
        array(
            'name'   => 'hide',
            'value'  => '($data->hide)?"Да":"Нет"',
            'header' => 'Скрыто',
            'htmlOptions' => array(
                'style' => 'text-align: center;',
            ),
            'filter' => array(
                0 => "Нет",
                1 => "Да",
            ),
        ),
        array(
            'name'   => 'final_test',
            'value'  => '($data->final_test)?"Да":"Нет"',
            'header' => 'Фин. тест.',
            'htmlOptions' => array(
                'style' => 'text-align: center;',
            ),
            'filter' => array(
                0 => "Нет",
                1 => "Да",
            ),
        ),
        array(
            'htmlOptions'   => array('nowrap'=>'nowrap'),
            'class'         => 'booster.widgets.TbButtonColumn',
            'template'      => '{view_results}&nbsp;&nbsp;&nbsp;{view}{update}{delete}',
            'buttons'       => array(
                'view_results' => array(
                    'label'     => '',
                    'options'   => array('class' => 'glyphicon glyphicon-signal'),
                    'url'       => '"/admin/contest/viewResults/id/$data->id"',
                )
            ),
        ),
    )
));
?>
