<?php
/**
 * @var Contest $model
 * @var array $table
 * @var array $contest_problems
 */

$this->main_header = Yii::t('interface','Просмотр результатов');

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')       => $this->createUrl('default/index'),
    Yii::t('interface', 'Список олимпиад')    => $this->createUrl('contest/index'),
    Yii::t('interface', 'Просмотр результатов')  => '#',
);
?>

<?php foreach ($table as $grade => $users): ?>
    <h2><?php echo $grade; ?></h2>
    <table class="table table-bordered">
        <tr>
            <th style="width: 15%;">Участник</th>
            <?php foreach ($contest_problems[$grade] as $contest_problem): ?>
                <th style="width: <?php echo floor(71 / count($contest_problems[$grade])) ?>%;"><?php echo $contest_problem->problem->getName(); ?></th>
            <?php endforeach; ?>
            <th style="width: 7%;">Принято задач</th>
            <th style="width: 7%;">Баллы</th>
        </tr>
        <?php foreach ($users as $user): ?>
            <tr>
                <td><?php echo CHtml::link($user['contest_user']->user->login, array('user/view', 'id' => $user['contest_user']->user->id)); ?></td>
                <?php foreach ($contest_problems[$grade] as $contest_problem): ?>
                    <td>
                        <?php if (array_key_exists($contest_problem->id, $user['problems'])): ?>
                            <?php foreach ($user['problems'][$contest_problem->id] as $solution): ?>
                                <p><?php echo CHtml::link($solution->solution->getStatusName(), array('solution/view', 'id' => $solution->solution->id)); ?></p>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                <?php endforeach; ?>
                <td class="text-center"> <?php echo $user['accepted_problems'];?> / <?php echo count($contest_problems[$grade]); ?></td>
                <td class="text-center"><?php echo $user['score'];?></td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php endforeach; ?>

<?php if (strtotime($model->end_time) < time()): ?>
<div class="row" style="margin-top: 35px;">
    <div class="col-md-4 col-md-offset-4 text-center">
        <?php if(Yii::app()->user->hasFlash('counting_started')):?>
            <div class="alert alert-success" role="alert">
                <?php echo Yii::app()->user->getFlash('counting_started'); ?>
            </div>
        <?php else: ?>
            <?php echo CHtml::link(Yii::t('interface', 'Подсчитать баллы'), array('contest/startCounting', 'id' => $model->id), array('class' => 'btn btn-success')); ?>
        <?php endif; ?>
    </div>
</div>
<?php endif; ?>