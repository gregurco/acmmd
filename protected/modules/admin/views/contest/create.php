<?php
/**
 * @var Contest $model
 */

$this->main_header = Yii::t('interface','Создание олимпиады');

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')         => $this->createUrl('default/index'),
    Yii::t('interface', 'Список олимпиад')    => $this->createUrl('contest/index'),
    Yii::t('interface', 'Создание олимпиады') => '#',
);
?>

<?php $this->renderPartial('_form', array(
    'model'                 => $model,
    'problems'              => $problems,
    'problem_options'       => $problem_options,
    'participants'          => $participants,
    'participants_options'  => $participants_options,
)); ?>