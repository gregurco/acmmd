<?php
/**
 * @var $this ContestController
 * @var $model Contest
 * @var $form CActiveForm
 * @var $problem_options array
 * @var $participants_options array
 *
 */
?>

<div class="form">
    <?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm'
    );
    ?>

        <?php echo $form->textFieldGroup($model,'title_ru',array('size'=>60,'maxlength'=>255, 'label' => 'Заглавие (рус.):')); ?>

        <?php echo $form->textFieldGroup($model,'title_ro',array('size'=>60,'maxlength'=>255, 'label' => 'Заглавие (рум.):')); ?>

        <?php echo $form->ckEditorGroup(
            $model,
            'description_ru',
            array(
                'widgetOptions' => array(
                    'htmlOptions' => array('style'=>'height: 150px;')
                ),
                'label' => 'Описание (рус.):'
            )
        ); ?>

        <?php echo $form->ckEditorGroup(
            $model,
            'description_ro',
            array(
                'widgetOptions' => array(
                    'htmlOptions' => array('style'=>'height: 150px;')
                ),
                'label' => 'Описание (рум.):'
            )
        ); ?>

        <?php echo $form->textFieldGroup($model,'start_time',array('size'=>60, 'label' => 'Начало:')); ?>

        <?php echo $form->textFieldGroup($model,'end_time',array('size'=>60, 'label' => 'Конец:')); ?>

        <?php echo $form->switchGroup($model, 'hide',
            array(
                'label' => 'Скрыта:'
            )
        ); ?>

        <?php echo $form->switchGroup($model, 'final_test',
            array(
                'label' => 'Финальное тестирование:'
            )
        ); ?>

        <?php for ($i = 9; $i <= 12; $i++): ?>
            <div class="form-group" style="margin-top: 20px;">
                <label class="control-label" for="selected_problems_<?php echo $i; ?>"><?php echo Yii::t('interface', "Задачи ({$i} класс)"); ?>:</label>
                <div>
                    <?php echo CHtml::dropDownList("selected_problems_{$i}", 0,
                        $problems,
                        array('multiple' => true, 'class' => 'chosen', 'options' => $problem_options[$i] ));
                    ?>
                </div>
            </div>
        <?php endfor; ?>

        <?php for ($i = 9; $i <= 12; $i++): ?>
            <div class="form-group" style="margin-top: 20px;">
                <label class="control-label" for="selected_users_<?php echo $i; ?>"><?php echo Yii::t('interface', "Участники ({$i} класс)"); ?>:</label><div>
                    <?php echo CHtml::dropDownList("selected_users_{$i}", 0,
                        $participants,
                        array('multiple' => true, 'class' => 'chosen', 'options' => $participants_options[$i] ));
                    ?>
                </div>
            </div>
        <?php endfor; ?>

        <div class="form-group text-center" style="margin-top: 20px;">
            <?php
            $this->widget(
                'booster.widgets.TbButton',
                array(
                    'buttonType'=> 'submit',
                    'context'   => 'success',
                    'label'     => $model->isNewRecord ? Yii::t('interface','Создать') : Yii::t('interface','Сохранить')
                )
            );
            ?>

            <?php
            echo CHtml::link(
                Yii::t('interface','Вернуться'),
                $this->createUrl('contest/index'),
                array(
                    'class' => 'btn btn-danger'
                )
            );
            ?>
        </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<script>
    $(function(){
        $('#Contest_start_time').add('#Contest_end_time').datetimepicker({format:'Y-m-d H:i:s'});

        $('.chosen').chosen({width: "100%"});
    });
</script>

