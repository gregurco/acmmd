<?php
/**
 * @var Contest $model
 */

$this->main_header = Yii::t('interface','Просмотр олимпиады');

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')         => $this->createUrl('default/index'),
    Yii::t('interface', 'Список олимпиад')    => $this->createUrl('contest/index'),
    Yii::t('interface', 'Просмотр олимпиады') => '#',
);
?>

<?php $this->widget('booster.widgets.TbDetailView', array(
    'data'       => $model,
    'attributes' => array(
        array('name' => 'id', 'label' => Yii::t('interface', 'ID')),
        array('name' => 'title_ru', 'label' => Yii::t('interface', 'Название (ru)')),
        array('name' => 'title_ro', 'label' => Yii::t('interface', 'Название (ro)')),
        array('name' => 'description_ru', 'label' => Yii::t('interface', 'Описание (ru)'), 'type'=>'raw'),
        array('name' => 'description_ro', 'label' => Yii::t('interface', 'Описание (ro)'), 'type'=>'raw'),
        array('name' => 'hide', 'label' => Yii::t('interface', 'Скрыта'), 'value' => ($model->hide)? Yii::t('interface', 'Да') : Yii::t('interface', 'Нет')),
    ),
)); ?>
