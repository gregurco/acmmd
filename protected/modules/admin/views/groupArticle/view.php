<?php
/* @var $this GroupArticleController */
/* @var $model GroupArticle */

$this->breadcrumbs=array(
	'Group Articles'=>array('index'),
	$model->id,
);

$this->main_header = 'Просмотр группы новостей #' . $model->id;
?>

<?php $this->widget('booster.widgets.TbDetailView', array(
    'data'       => $model,
    'attributes' => array(
        array('name' => 'id', 'label' => Yii::t('interface', 'ID')),
        array('name' => 'title_ru', 'label' => Yii::t('interface', 'Название (ru)')),
        array('name' => 'title_ro', 'label' => Yii::t('interface', 'Название (ro)')),
        array('name' => 'hide', 'label' => Yii::t('interface', 'Скрыта'), 'value' => ($model->hide)? Yii::t('interface', 'Да') : Yii::t('interface', 'Нет')),
    ),
)); ?>
