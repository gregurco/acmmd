<?php
/* @var $this GroupArticleController */
/* @var $model GroupArticle */

$this->main_header = Yii::t('interface','Создать группу новостей');
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>