<?php
/* @var $this GroupArticleController */
/* @var $model GroupArticle */

$this->main_header = Yii::t('interface','Manage Group Articles');
?>

<?php
$this->renderGridView(array(
    'id'            => 'group-article-grid',
    'model'         => $model,
    'columns'       => array(
        'id',
        'title_ru',
        'title_ro',
        array(
            'name'        => 'hide',
            'value'       => '($data->hide)?"Да":"Нет"',
            'header'      => 'Скрыто',
            'htmlOptions' => array(
                'style'   => 'text-align: center;',
            ),
            'filter' => array(
                0 => "Нет",
                1 => "Да",
            ),
        ),
        array(
            'htmlOptions' => array('nowrap'=>'nowrap'),
            'class'       => 'booster.widgets.TbButtonColumn',
        ),
    )
));
?>
