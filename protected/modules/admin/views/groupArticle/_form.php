<?php
/* @var $this GroupArticleController */
/* @var $model GroupArticle */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm'
    );
    ?>

        <?php echo $form->textFieldGroup($model,'title_ru',array('size'=>60,'maxlength'=>255, 'label' => 'Заглавие (рус.):')); ?>

        <?php echo $form->textFieldGroup($model,'title_ro',array('size'=>60,'maxlength'=>255, 'label' => 'Заглавие (рум.):')); ?>

        <?php echo $form->switchGroup($model, 'hide',
            array(
                'label' => 'Скрыта:'
            )
        ); ?>

        <div class="form-group text-center" style="margin-top: 20px;">
            <?php
            $this->widget(
                'booster.widgets.TbButton',
                array(
                    'buttonType'=> 'submit',
                    'context'   => 'success',
                    'label'     => $model->isNewRecord ? Yii::t('interface','Создать') : Yii::t('interface','Сохранить')
                )
            );
            ?>

            <?php
            echo CHtml::link(
                Yii::t('interface','Вернуться'),
                $this->createUrl('groupArticle/index'),
                array(
                    'class' => 'btn btn-danger'
                )
            );
            ?>
        </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->