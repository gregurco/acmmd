<?php
/* @var $this SolutionController */
/* @var $model Solution */

$this->main_header = $model->problem->name;

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')       => $this->createUrl('default/index'),
    Yii::t('interface', 'Список решений')   => $this->createUrl('solution/index'),
    Yii::t('interface', 'Просмотр решения') => '#',
);
?>

<?php $this->widget('booster.widgets.TbDetailView', array(
    'data'       => $model,
    'attributes' => array(
        array('name' => 'id', 'label' => Yii::t('interface', 'ID')),
        array('name' => 'userLogin', 'label' => Yii::t('interface', 'Пользователь'), 'value' => $model->user->login),
        array('name' => 'problemName', 'label' => Yii::t('interface', 'Задача'), 'value' => $model->problem->name),
        array('name' => 'time_send', 'label' => Yii::t('interface', 'Время отправки'), 'value' => Yii::app()->dateFormatter->formatDateTime($model->time_send)),
        array('name' => 'status', 'label' => Yii::t('interface', 'Статус'), 'value' => $model->getStatusName()),
        array('name' => 'result', 'label' => Yii::t('interface', 'Результат')),
        array('name' => 'compiler', 'label' => Yii::t('interface', 'Компилятор')),
        array('name' => 'log_compile', 'label' => Yii::t('interface', 'Лог компилятора')),
    ),
)); ?>

<br>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title text-center">
            <?php echo Yii::t('interface','Тесты');?>
        </h3>
    </div>
    <div class="panel-body">
        <table class="table table-hover">
            <tr class="active">
                <td><b><?php echo Yii::t('interface','Тест');?></b></td>
                <td><b><?php echo Yii::t('interface','Результат');?></b></td>
            </tr>
            <?php for ($i=1; $i <= $model->problem->tests; $i++): ?>
                <tr class="<?php echo $model->getTrClassByResult($i);?>">
                    <td><?php echo $i; ?></td>
                    <td><?php echo (!empty($model->tests) && array_key_exists($i, $model->tests)) ? $model->getResultStatusName($i) . ' ('.$model->tests[$i][1].' с.)' : "-"; ?></td>
                </tr>
            <?php endfor; ?>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title text-center">
            <?php echo Yii::t('interface','Код');?>
        </h3>
    </div>
    <div class="panel-body" style="padding: 0;">
        <pre class="brush : <?php echo $model->getLanguageByCompiler(); ?>"><?php echo CHtml::encode($model->file_text); ?></pre>
    </div>
    <div class="panel-footer">
        <button id="copy-button" data-clipboard-text="<?php echo $model->file_text;?>" tabindex="0" class="btn btn-default" data-toggle="popover" data-trigger="focus" data-content="<?php echo Yii::t('interface','Код был скопирован');?>"><?php echo Yii::t('interface','Скопировать код');?></button>
        <button id="download-button" class="btn btn-default"><a href="<?php echo $this->createUrl('solution/download', array('id' => $model->id));?>"><?php echo Yii::t('interface','Скачать код');?></a></button>
    </div>
</div>

<?php if ($model->getNotification()): ?>
    <div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-info-sign"></span> <?php echo $model->getNotification();?></div>
<?php endif; ?>

<script>
    var client = new ZeroClipboard( $("#copy-button") );
</script>
