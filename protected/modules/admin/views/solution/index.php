<?php
/* @var $this SolutionController */
/* @var $model Solution */

$this->main_header = Yii::t('interface','Manage Solutions');

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')     => $this->createUrl('default/index'),
    Yii::t('interface', 'Список решений') => '#',
);
?>

<?php
$this->renderGridView(array(
    'id'            => 'solution-grid',
    'model'         => $model,
    'columns'       => array(
        'id',
        array(
            'name'   => 'userLogin',
            'value'  => '$data->user ? $data->user->login : "-"',
            'header' => 'Пользователь',
        ),
        array(
            'name' => 'p_id',
            'value' => '$data->problem ? $data->problem->name : "-"',
            'header' => Yii::t('interface','Задача'),
            'filter'=> CHtml::listData(Problem::model()->findAll(),'id','name'),
        ),
        array(
            'name'   => 'result',
            'value'  => '$data->result',
            'header' => 'Баллы',
        ),
        array(
            'name'   => 'compiler',
            'value'  => '$data->compiler',
            'header' => 'Компилятор',
        ),
        array(
            'name'   => 'status',
            'value'  => '$data->getStatusName()',
            'header' => 'Статус',
        ),
        array(
            'name'   => 'time_send',
            'value'  => 'date("j.m.Y",$data->time_send)',
            'header' => 'Время отправки',
        ),
        array(
            'htmlOptions' => array('nowrap'=>'nowrap'),
            'class'       => 'booster.widgets.TbButtonColumn',
            'buttons'     => array(
                'update'    => array(
                    'visible' => ''
                )
            )
        ),
    )
));
?>