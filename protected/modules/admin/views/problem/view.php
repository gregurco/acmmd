<?php
/**
 * @var $this ProblemController
 * @var $model Problem
 * @var $statistics array
 * @var $tests array
 */

$this->main_header = Yii::t('interface','View Problem #') . ' ' . $model->id;

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')      => $this->createUrl('default/index'),
    Yii::t('interface', 'Список задач')    => $this->createUrl('problem/index'),
    Yii::t('interface', 'Просмотр задачи') => '#',
);
?>

<div role="tabpanel">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#main" aria-controls="home" role="tab" data-toggle="tab"><?php echo Yii::t('interface', 'Общее'); ?></a></li>
        <li role="presentation"><a href="#tests" aria-controls="tests" role="tab" data-toggle="tab"><?php echo Yii::t('interface', 'Тесты'); ?></a></li>
        <li role="presentation"><a href="#statistics" aria-controls="profile" role="tab" data-toggle="tab"><?php echo Yii::t('interface', 'Статистика'); ?></a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="main">
            <table class="detail-view table table-striped table-condensed" id="yw0">
                <tbody>
                <tr class="odd">
                    <th><?php echo Yii::t('interface', 'ID'); ?></th>
                    <td><?php echo $model->id; ?></td>
                </tr>
                <tr class="even">
                    <th><?php echo Yii::t('interface', 'Название');?> (ru)</th>
                    <td><?php echo $model->name_ru; ?></td>
                </tr>
                <tr class="odd">
                    <th><?php echo Yii::t('interface', 'Название'); ?> (ro)</th>
                    <td><?php echo $model->name_ro; ?></td>
                </tr>
                <tr class="even">
                    <th><?php echo Yii::t('interface', 'Описание'); ?> (ru)</th>
                    <td><?php echo $model->description_ru; ?></td>
                </tr>
                <tr class="odd">
                    <th><?php echo Yii::t('interface', 'Описание'); ?> (ro)</th>
                    <td><?php echo $model->description_ro; ?></td>
                </tr>
                <tr class="even">
                    <th><?php echo Yii::t('interface', 'Входные данные'); ?> (ru)</th>
                    <td><?php echo $model->input_ru; ?></td>
                </tr>
                <tr class="odd">
                    <th><?php echo Yii::t('interface', 'Входные данные'); ?> (ro)</th>
                    <td><?php echo $model->input_ro; ?></td>
                </tr>
                <tr class="even">
                    <th><?php echo Yii::t('interface', 'Выходные данные'); ?> (ru)</th>
                    <td><?php echo $model->output_ru; ?></td>
                </tr>
                <tr class="odd">
                    <th><?php echo Yii::t('interface', 'Выходные данные'); ?> (ro)</th>
                    <td><?php echo $model->output_ro; ?></td>
                </tr>
                <tr class="even">
                    <th><?php echo Yii::t('interface', 'Тесты'); ?></th>
                    <td><?php echo $model->tests; ?></td>
                </tr>
                <tr class="odd">
                    <th><?php echo Yii::t('interface', 'Лимит времени'); ?></th>
                    <td><?php echo $model->limit_time; ?></td>
                </tr>
                <tr class="even">
                    <th><?php echo Yii::t('interface', 'Лимит памяти'); ?></th>
                    <td><?php echo Yii::app()->format->size($model->limit_memory * 1048576); ?></td>
                </tr>
                <tr class="odd">
                    <th><?php echo Yii::t('interface', 'Скрыт'); ?></th>
                    <td><?php echo $model->hide ? Yii::t('interface', 'Да') : Yii::t('interface', 'Нет'); ?></td>
                </tr>
                <tr class="even">
                    <th><?php echo Yii::t('interface', 'Trim lines'); ?></th>
                    <td><?php echo $model->trim_lines ? Yii::t('interface', 'Да') : Yii::t('interface', 'Нет'); ?></td>
                </tr>
                <tr class="odd">
                    <th><?php echo Yii::t('interface', 'Allow empty last line'); ?></th>
                    <td><?php echo $model->empty_last_line ? Yii::t('interface', 'Да') : Yii::t('interface', 'Нет'); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="tests">
            <table class="table table-striped table-condensed">
                <thead>
                <tr>
                    <th style="width: 50px;"><?php echo Yii::t('interface', 'ID'); ?></th>
                    <th><?php echo Yii::t('interface', 'Входные файлы'); ?></th>
                    <th><?php echo Yii::t('interface', 'Выходные файлы'); ?></th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($tests as $test_id => $files): ?>
                        <tr>
                            <td><?php echo $test_id; ?></td>
                            <td><?php echo $files['input'] ? '<a href="' . $this->createUrl('problem/downloadTest', ['p_id' => $model->id, 'file' => str_replace('/', '-', '/input/' . $files['input'])]) . '">' . $files['input'] . '</a>' : '<i>none</i>'; ?></td>
                            <td>
                                <?php if (count($files['output'])): ?>
                                    <?php foreach ($files['output'] as $output_file): ?>
                                        <div class="mr-top-5">
                                            <a href="<?php echo $this->createUrl('problem/downloadTest', ['p_id' => $model->id, 'file' => str_replace('/', '-', '/output/' . $test_id . '/' . $output_file)]); ?>">
                                                <?php echo $output_file; ?>
                                            </a>
                                        </div>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <i>none</i>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="statistics">
            <div class="row" style="margin-top: 25px;">
                <div class="col-md-4 col-md-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading"><?php echo Yii::t('interface', 'Отправленные решения (суммарно)'); ?></div>
                        <div class="panel-body">
                            <canvas id="sentSolutionsChart" width="400" height="400"></canvas>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-md-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading"><?php echo Yii::t('interface', 'Пользовательская статистика'); ?></div>
                        <div class="panel-body">
                            <canvas id="userChart" width="400" height="400"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        var general_data = [
                {
                    value: <?php echo $statistics['general']['failed']; ?>,
                    color:"#F7464A",
                    highlight: "#FF5A5E",
                    label: "0 баллов"
                },
                {
                    value: <?php echo $statistics['general']['completed']; ?>,
                    color: "#46BFBD",
                    highlight: "#5AD3D1",
                    label: "Макс. кол-во баллов"
                },
                {
                    value: <?php echo $statistics['general']['partial']; ?>,
                    color: "#FDB45C",
                    highlight: "#FFC870",
                    label: "Неполные решения"
                }
            ],
            user_data = [
                {
                    value: <?php echo $statistics['users']['not_resolved']; ?>,
                    color:"#F7464A",
                    highlight: "#FF5A5E",
                    label: "Не решили"
                },
                {
                    value: <?php echo $statistics['users']['completed']; ?>,
                    color: "#46BFBD",
                    highlight: "#5AD3D1",
                    label: "Решили"
                },
                {
                    value: <?php echo $statistics['users']['partial']; ?>,
                    color: "#FDB45C",
                    highlight: "#FFC870",
                    label: "Не закончили"
                }
            ],
            options = {
                scaleShowLabelBackdrop: true,
                pointLabelDelimiter: "\n"
            },
            sentSolutionsChart,
            userChart,
            $sentSolutionsChart = $("#sentSolutionsChart"),
            $userChart = $("#userChart");

        var drawCharts = function() {
            if (!sentSolutionsChart) {
                $sentSolutionsChart.width($sentSolutionsChart.closest('.panel-body').width());
                sentSolutionsChart = new Chart($sentSolutionsChart.get(0).getContext("2d")).Pie(general_data, options);
            }

            if (!userChart) {
                $userChart.width($userChart.closest('.panel-body').width());
                userChart = new Chart($userChart.get(0).getContext("2d")).Pie(user_data, options);
            }
        };

        $('a[aria-controls="profile"]').on('shown.bs.tab', drawCharts);
        if ($('#statistics').hasClass('active')) { drawCharts(); }
    });
</script>