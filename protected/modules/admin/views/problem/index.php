<?php
/* @var $this ProblemController */
/* @var $model Problem */

$this->main_header = Yii::t('interface','Manage Problems');

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')     => $this->createUrl('default/index'),
    Yii::t('interface', 'Список задач')   => '#',
);
?>

<?php
$this->renderGridView(array(
    'id'            => 'problem-grid',
    'model'         => $model,
    'dataProvider'  => $model->searchAdmin(),
    'columns'       => array(
        'id',
        'name_ru',
        'name_ro',
        array(
            'name'        => 'hide',
            'value'       => '($data->hide)?"Да":"Нет"',
            'header'      => 'Скрыто',
            'htmlOptions' => array(
                'style'   => 'text-align: center;',
            ),
            'filter' => array(
                0 => "Нет",
                1 => "Да",
            ),
        ),
        array(
            'template'=>'{tests} {view} {update} {delete}',
            'htmlOptions' => array('nowrap'=>'nowrap'),
            'class'       => 'booster.widgets.TbButtonColumn',
            'buttons'=>array(
                'tests' => array(
                    'label'=>'Тесты',
                    'icon'=>'file',
                    'url'=>'Yii::app()->createUrl("admin/problem/tests", array("id"=>$data->id))',
                ),
            ),
        ),
    )
));
?>
