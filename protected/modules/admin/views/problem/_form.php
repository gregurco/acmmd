<?php
/* @var $this ProblemController */
/* @var $model Problem */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm'
    );
    ?>

    <?php echo $form->textFieldGroup($model,'name_ru',array('size'=>60,'maxlength'=>255, 'label' => 'Заглавие (рус.):')); ?>

    <?php echo $form->textFieldGroup($model,'name_ro',array('size'=>60,'maxlength'=>255, 'label' => 'Заглавие (рус.):')); ?>

    <?php echo $form->ckEditorGroup(
        $model,
        'description_ru',
        array(
            'widgetOptions' => array(
                'htmlOptions' => array('style'=>'height: 150px;')
            ),
            'label' => 'Описание (рус.):'
        )
    ); ?>

    <?php echo $form->ckEditorGroup(
        $model,
        'description_ro',
        array(
            'widgetOptions' => array(
                'htmlOptions' => array('style'=>'height: 150px;')
            ),
            'label' => 'Описание (рум.):'
        )
    ); ?>

    <?php echo $form->ckEditorGroup(
        $model,
        'input_ru',
        array(
            'widgetOptions' => array(
                'htmlOptions' => array('style'=>'height: 150px;')
            ),
            'label' => 'Входные данные (рус.):'
        )
    ); ?>

    <?php echo $form->ckEditorGroup(
        $model,
        'input_ro',
        array(
            'widgetOptions' => array(
                'htmlOptions' => array('style'=>'height: 150px;')
            ),
            'label' => 'Входные данные (рум.):'
        )
    ); ?>

    <?php echo $form->ckEditorGroup(
        $model,
        'output_ru',
        array(
            'widgetOptions' => array(
                'htmlOptions' => array('style'=>'height: 150px;')
            ),
            'label' => 'Выходные данные (рус.):'
        )
    ); ?>

    <?php echo $form->ckEditorGroup(
        $model,
        'output_ro',
        array(
            'widgetOptions' => array(
                'htmlOptions' => array('style'=>'height: 150px;')
            ),
            'label' => 'Выходные данные (рум.):'
        )
    ); ?>

    <div class="form-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse2">Дополнительные параметры</a>
                </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">
                    <?php echo $form->textFieldGroup($model,'tests',array('size'=>3, 'maxlength'=>3, 'label' => 'Кол-во тестов:')); ?>

                    <?php echo $form->textFieldGroup($model,'limit_time', array('label' => 'Лимит времени:')); ?>

                    <?php echo $form->textFieldGroup($model,'limit_memory', array('label' => 'Лимит памяти:')); ?>

                    <?php echo $form->switchGroup($model, 'hide',
                        array(
                            'label' => 'Скрыта:'
                        )
                    ); ?>

                    <?php echo $form->switchGroup($model, 'trim_lines',
                        array(
                            'label' => 'Trim lines:'
                        )
                    ); ?>

                    <?php echo $form->switchGroup($model, 'empty_last_line',
                        array(
                            'label' => 'Allow empty last line:'
                        )
                    ); ?>

                </div>
            </div>
        </div>
    </div>

    <div class="form-group" id="examples">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse1">Примеры входных/выходных данных</a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="panel-body">
                    <button type="button" class="btn btn-primary" style="margin-bottom: 15px;" onclick="AddTableRow($('#examples'));"><i class="fa fa-plus"></i> Добавить ряд</button>

                    <table id="example" class="table table-bordered">
                        <?php for ($i=0; $i<10; $i++):?>
                            <tr class="row-title" <?php if (!isset($model->examples[$i]['input']) && !isset($model->examples[$i]['output']) && !isset($model->examples[$i]['explanation'])){ echo "style='display: none;'";} ?> >
                                <td class="text-center">
                                    <strong>Тест <?php echo $i+1;?></strong>
                                </td>
                            </tr>
                            <tr class="row-data" <?php if (!isset($model->examples[$i]['input']) && !isset($model->examples[$i]['output']) && !isset($model->examples[$i]['explanation'])){ echo "style='display: none;'";} ?> >
                                <td>
                                    <?php echo $form->html5EditorGroup(
                                        $model,
                                        'examples['.$i.'][input]',
                                        array(
                                            'widgetOptions' => array(
                                                'htmlOptions' => array('style'=>'height: 150px;')
                                            ),
                                            'label' => 'Входные данные:'
                                        )
                                    ); ?>

                                    <?php echo $form->html5EditorGroup(
                                        $model,
                                        'examples['.$i.'][output]',
                                        array(
                                            'widgetOptions' => array(
                                                'htmlOptions' => array('style'=>'height: 150px;')
                                            ),
                                            'label' => 'Выходные данные:'
                                        )
                                    ); ?>

                                    <?php echo $form->html5EditorGroup(
                                        $model,
                                        'examples['.$i.'][explanation]',
                                        array(
                                            'widgetOptions' => array(
                                                'htmlOptions' => array('style'=>'height: 150px;')
                                            ),
                                            'label' => 'Объяснение:'
                                        )
                                    ); ?>
                                </td>
                            </tr>
                        <?php endfor; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group" id="tests">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse3">Входные/выходные данные</a>
                </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
                <div class="panel-body">

                </div>
            </div>
        </div>
    </div>

    <div class="form-group text-center" style="margin-top: 20px;">
        <?php
        $this->widget(
            'booster.widgets.TbButton',
            array(
                'buttonType'=> 'submit',
                'context'   => 'success',
                'label'     => $model->isNewRecord ? Yii::t('interface','Создать') : Yii::t('interface','Сохранить')
            )
        );
        ?>

        <?php
        echo CHtml::link(
            Yii::t('interface','Вернуться'),
            $this->createUrl('problem/index'),
            array(
                'class' => 'btn btn-danger'
            )
        );
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->