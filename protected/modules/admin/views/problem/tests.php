<?php
/**
 * @var $this ProblemController
 * @var $model Problem
 * @var $uploadTestForm UploadTestForm
 * @var $tests array
 */

$this->main_header = Yii::t('interface','Tests for Problem #') . ' ' . $model->id;

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')      => $this->createUrl('default/index'),
    Yii::t('interface', 'Список задач')    => $this->createUrl('problem/index'),
    Yii::t('interface', 'Тесты') => '#',
);
?>

<table class="table table-striped table-condensed">
    <thead>
    <tr>
        <th style="width: 50px;"><?php echo Yii::t('interface', 'ID'); ?></th>
        <th><?php echo Yii::t('interface', 'Входные файлы'); ?></th>
        <th><?php echo Yii::t('interface', 'Выходные файлы'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($tests as $test_id => $files): ?>
        <tr class="<?php if (!$files['input'] || !count($files['output'])): ?>warning<?php endif; ?>">
            <td><?php echo $test_id; ?></td>
            <td>
                <?php if ($files['input']): ?>
                    <a href="<?php echo $this->createUrl('problem/downloadTest', ['p_id' => $model->id, 'file' => str_replace('/', '-', '/input/' . $files['input'])]); ?>">
                        <?php echo $files['input']; ?>
                    </a>
                    <div onclick="deleteTestConfirm('<?php echo $this->createUrl('problem/deleteTest', ['p_id' => $model->id, 'file' => str_replace('/', '-', '/input/' . $files['input'])]); ?>')" class="btn btn-danger btn-xs">
                        <i class="fa fa-trash-o"></i>
                    </div>
                <?php else: ?>
                    <i>none</i>
                <?php endif; ?>
            </td>
            <td>
                <?php if (count($files['output'])): ?>
                    <?php foreach ($files['output'] as $output_file): ?>
                        <div class="mr-top-5">
                            <a href="<?php echo $this->createUrl('problem/downloadTest', ['p_id' => $model->id, 'file' => str_replace('/', '-', '/output/' . $test_id . '/' . $output_file)]); ?>">
                                <?php echo $output_file; ?>
                            </a>
                            <div onclick="deleteTestConfirm('<?php echo $this->createUrl('problem/deleteTest', ['p_id' => $model->id, 'file' => str_replace('/', '-', '/output/' . $test_id . '/' . $output_file)]); ?>')" class="btn btn-danger btn-xs">
                                <i class="fa fa-trash-o"></i>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <i>none</i>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<div class="form">
    <div class="row mr-top-25">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <?php $form=$this->beginWidget('CActiveForm',array(
                'htmlOptions'=>array('class' => 'form-horizontal well', 'enctype'=>'multipart/form-data'),
            )); ?>

            <div class="row">
                <h3 class="col-sm-12 text-center"><?php echo Yii::t('interface','Загрузить тест');?></h3>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($uploadTestForm, 'type', array('class' => 'col-sm-4 control-label'));?>
                <div class="col-sm-8">
                    <?php echo $form->dropDownList($uploadTestForm, 'type', array('input' => Yii::t('interface','Input'), 'output' => Yii::t('interface','Output')), array('class' => 'form-control')); ?>
                    <?php echo $form->error($uploadTestForm, 'type'); ?>
                </div>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($uploadTestForm, 'ordinalNumber', array('class' => 'col-sm-4 control-label'));?>
                <div class="col-sm-8">
                    <?php echo $form->dropDownList($uploadTestForm, 'ordinalNumber', $uploadTestForm->getOrdinalNumberValues(), array('class' => 'form-control')); ?>
                    <?php echo $form->error($uploadTestForm, 'ordinalNumber'); ?>
                </div>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($uploadTestForm, 'file', array('class' => 'col-sm-4 control-label'));?>
                <div class="col-sm-8">
                    <?php echo $form->fileField($uploadTestForm, 'file', array('class' => 'form-control')); ?>
                    <?php echo $form->error($uploadTestForm, 'file'); ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12 text-center">
                    <?php echo CHtml::submitButton(Yii::t('interface','Отправить'), array('class' => 'btn btn-default')); ?>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>

<div class="modal fade" id="deleteTestModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="z-index: 10000;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo Yii::t('interface','Удаление теста'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo Yii::t('interface','Вы действительно хотите <b>удалить</b> тест? Данный тест будет удален безвозвратно.'); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('interface','Закрыть'); ?></button>
                <a href="" id="deleteTestModalButton" class="btn btn-danger"><?php echo Yii::t('interface','Удалить'); ?></a>
            </div>
        </div>
    </div>
</div>

<script>
    function deleteTestConfirm(url) {
        $('#deleteTestModalButton').attr('href', url);
        $('#deleteTestModal').modal('show');
    }
</script>