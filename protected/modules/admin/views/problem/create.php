<?php
/* @var $this ProblemController */
/* @var $model Problem */

$this->main_header = Yii::t('interface','Create Problem');

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')     => $this->createUrl('default/index'),
    Yii::t('interface', 'Список задач')   => $this->createUrl('problem/index'),
    Yii::t('interface', 'Создать задачу') => '#',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>