<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->main_header = 'Редактирование настройки "' . $model->param . '"';

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')               => $this->createUrl('default/index'),
    Yii::t('interface', 'Настройки')                => $this->createUrl('config/index'),
    Yii::t('interface', 'Редактировать настройку')  => '#',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>