<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->main_header = Yii::t('interface','Manage Configs');

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика') => $this->createUrl('default/index'),
    Yii::t('interface', 'Настройки')  => '#',
);
?>

<?php
$this->renderGridView(array(
    'id'            => 'config-grid',
    'model'         => $model,
    'columns'       => array(
        array(
            'name' => 'label',
            'value' => '$data->label',
            'header' => 'Параметр',
            'htmlOptions' => array('style' => 'width: 30%;'),
        ),
        array(
            'name' => 'value',
            'value' => '($data->type=="boolean") ? ($data->value ? "Да" : "Нет") : ($data->type=="textarea" ? (substr($data->value, 0, 150) . (strlen($data->value) > 150 ? "..." : "")) : $data->value)',
            'header' => 'Значение',
            'filter' => false,
        ),
        array(
            'htmlOptions' => array('nowrap'=>'nowrap'),
            'class'       => 'booster.widgets.TbButtonColumn',
            'buttons'     => array(
                'delete'    => array(
                    'visible' => ''
                ),
                'view'    => array(
                    'visible' => ''
                )
            )
        ),
    )
));
?>