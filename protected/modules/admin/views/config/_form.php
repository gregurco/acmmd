<?php
/* @var $this ConfigController */
/* @var $model Config */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm'
    );
    ?>

        <?php if ($model->type == 'boolean'): ?>
            <?php echo $form->switchGroup($model, 'value',
                array(
                    'label' => Yii::t('interface','Значение:')
                )
            ); ?>
        <?php elseif ($model->type == 'textarea'): ?>
            <?php
            echo $form->textAreaGroup(
                $model,
                'value',
                array(
                    'widgetOptions' => array(
					    'htmlOptions' => array('rows' => 10),
				    ),
                    'label'     => Yii::t('interface','Значение:')
                )
            );
            ?>
        <?php else: ?>
            <?php
            echo $form->textFieldGroup(
                $model,
                'value',
                array(
                    'size'      => 60,
                    'maxlength' => 255,
                    'label'     => Yii::t('interface','Значение:')
                )
            );
            ?>
        <?php endif; ?>

        <div class="form-group text-center" style="margin-top: 20px;">
            <?php
            $this->widget(
                'booster.widgets.TbButton',
                array(
                    'buttonType' => 'submit',
                    'context'    => 'success',
                    'label'      => Yii::t('interface','Сохранить')
                )
            );
            ?>

            <?php
            echo CHtml::link(
                Yii::t('interface','Вернуться'),
                $this->createUrl('config/index'),
                array(
                    'class' => 'btn btn-danger'
                )
            );
            ?>
        </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->