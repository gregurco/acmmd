<?php
/**
 * @var Article $model
 */

$this->main_header = Yii::t('interface','View Article #') . $model->id;

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')       => $this->createUrl('default/index'),
    Yii::t('interface', 'Список статей')    => $this->createUrl('article/index'),
    Yii::t('interface', 'Просмотр статьи')  => '#',
);
?>

<?php $this->widget('booster.widgets.TbDetailView', array(
    'data'       => $model,
    'attributes' => array(
        array('name' => 'id', 'label' => Yii::t('interface', 'ID')),
        array('name' => 'title_ru', 'label' => Yii::t('interface', 'Название (ru)')),
        array('name' => 'title_ro', 'label' => Yii::t('interface', 'Название (ro)')),
        array('name' => 'text_ru', 'label' => Yii::t('interface', 'Текст (ru)'), 'type'=>'raw'),
        array('name' => 'text_ro', 'label' => Yii::t('interface', 'Текст (ro)'), 'type'=>'raw'),
        array('name' => 'groupTitle', 'label' => Yii::t('interface', 'Группа'), 'value' => $model->groupArticle->title),
        array('name' => 'hide', 'label' => Yii::t('interface', 'Скрыта'), 'value' => ($model->hide)? Yii::t('interface', 'Да') : Yii::t('interface', 'Нет')),
    ),
)); ?>
