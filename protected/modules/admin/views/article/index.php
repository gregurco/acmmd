<?php
/**
 * @var Article $model
 */

$this->main_header = Yii::t('interface','Manage Article');

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')     => $this->createUrl('default/index'),
    Yii::t('interface', 'Список статей')  => '#',
);
?>

<?php
$this->renderGridView(array(
    'id'            => 'article-grid',
    'model'         => $model,
    'columns'       => array(
        'id',
        array(
            'name'   => 'title_ru',
            'value'  => '$data->title_ru',
            'header' => 'Название (рус.)',
        ),
        array(
            'name'   => 'title_ro',
            'value'  => '$data->title_ro',
            'header' => 'Название (рум.)',
        ),
        array(
            'name'   => 'hide',
            'value'  => '($data->hide)?"Да":"Нет"',
            'header' => 'Скрыто',
            'htmlOptions' => array(
                'style' => 'text-align: center;',
            ),
            'filter' => array(
                0 => "Нет",
                1 => "Да",
            ),
        ),
        array(
            'name' => 'groupTitle',
            'value' => '$data->groupArticle->title_'.Yii::app()->getUser()->getLanguage(),
        ),
        array(
            'htmlOptions' => array('nowrap'=>'nowrap'),
            'class'=>'booster.widgets.TbButtonColumn',
        ),
    )
));
?>
