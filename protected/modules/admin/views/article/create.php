<?php
/**
 * @var Article $model
 */

$this->main_header = Yii::t('interface','Создание статьи');

$this->breadcrumbs = array(
    Yii::t('interface', 'Статистика')       => $this->createUrl('default/index'),
    Yii::t('interface', 'Список статей')    => $this->createUrl('article/index'),
    Yii::t('interface', 'Создание статьи')  => '#',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>