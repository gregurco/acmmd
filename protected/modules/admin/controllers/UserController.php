<?php

class UserController extends MainAdminController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','delete','create','update','ChangePassword','password', 'generate'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'       => $this->loadModel($id),
            'problem_arr' => MProfile::getLinkedProblems($id),
            'score'       => Solution::userScore($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new User;

		if (Yii::app()->request->hasPost('User')) {
			$model->attributes = Yii::app()->request->post('User');
			if ($model->save()) {
                $this->redirect(array('user/index'));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $model->validate('update');

		if (Yii::app()->request->hasPost('User')) {
			$model->attributes = Yii::app()->request->post('User');
			if ($model->save()) {
                $this->redirect(array('user/index'));
            }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        NewsComment::model()->deleteAllByAttributes(array('u_id' => $id));
        Solution::model()->deleteAllByAttributes(array('u_id' => $id));
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!Yii::app()->request->hasGet('ajax')) {
            $this->redirect(Yii::app()->request->hasPost('returnUrl') ? Yii::app()->request->post('returnUrl') : array('/admin/user'));
        }
    }

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model = new User('search');
		$model->unsetAttributes();  // clear any default values
		if (Yii::app()->request->hasGet('User')) {
            $model->attributes = Yii::app()->request->get('User');
        }

		$this->render('index',array(
			'model'=>$model,
		));
	}

    public function actionChangePassword($id){
        $model = $this->loadModel($id);
        $model->setScenario('changePasswordAdmin');

        if(Yii::app()->request->hasPost('User')) {
            $user_password = Yii::app()->request->getPost('User');
            $model->password = $user_password['password'];
            if ($model->save()) {
                $this->redirect(array('index'));
            }
        }

        $model->password = '';

        $this->render('changePassword',array(
            'model'=>$model,
        ));
    }

    public function actionGenerate()
    {
        if (Yii::app()->request->getPost('number_to_generate')) {
            $this->layout = '';
            foreach (Yii::app()->log->routes as $route) {
                if ($route instanceof CWebLogRoute || $route instanceof CFileLogRoute || $route instanceof YiiDebugToolbarRoute) {
                    $route->enabled = false;
	            }
            }

            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=generated-users.csv');

            $output = fopen('php://output', 'w');

            fputcsv($output, array('Login', 'Password'), ';');

            $i = 1;
            $g = 0;
            $default_name = 'participant-';
            while ($g < Yii::app()->request->getPost('number_to_generate')){
                $find_user = User::model()->countByAttributes(array('login' => $default_name . $i));
                if (!$find_user) {
                    $new_password = User::generatePassword(6);

                    $new_user = new User();
                    $new_user->login = $default_name . $i;
                    $new_user->password = $new_password;
                    //$new_user->password_repeat = $new_password;

                    if ($new_user->save()) {
                        fputcsv($output, array( $new_user->login, $new_password), ';');
                        $g++;
                    }
                }
                $i++;
            }

            exit();
        }

        $this->render('generate',array(

        ));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}
