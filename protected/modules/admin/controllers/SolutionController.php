<?php

class SolutionController extends MainAdminController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view','admin','delete','create','update','download'),
                'users'=>array('*'),
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        // Connect HighLighter
        Yii::app()->syntaxhighlighter->addHighlighter();
        //Connect ZeroClipboard
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/libs/ZeroClipboard/ZeroClipboard.min.js');

        $model = $this->loadModel($id);

        $this->render('view', array(
            'model'=>$model,
        ));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if (Yii::app()->request->hasPost('Solution')) {
			$model->attributes=Yii::app()->request->post('Solution');
			if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!Yii::app()->request->hasGet('ajax')) {
            $this->redirect(Yii::app()->request->hasPost('returnUrl') ? Yii::app()->request->post('returnUrl') : array('admin'));
        }
    }

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model = new Solution('search');
		$model->unsetAttributes();  // clear any default values
		if (Yii::app()->request->hasGet('Solution')) {
            $model->attributes = Yii::app()->request->get('Solution');
        }

		$this->render('index',array(
			'model'=>$model,
		));
	}

    /**
     * @param $id
     * @return mixed
     * @throws CHttpException
     */
    public function actionDownload($id){
        $model = $this->loadModel($id);
        return Yii::app()->getRequest()->sendFile($model->problem->name . $model->getExtension(), $model->file_text);
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Solution the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Solution::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}
