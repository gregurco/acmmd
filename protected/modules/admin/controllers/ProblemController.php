<?php

class ProblemController extends MainAdminController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view','admin','delete','create','update','downloadTest', 'tests', 'deleteTest'),
                'users'=>array('*'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);

        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/libs/chatjs/Chart.min.js', CClientScript::POS_END);

        $testFilesManager = new TestFilesManager($id);
        $testFilesManager->search();

        $this->render('view',array(
            'model'     => $model,
            'statistics'=> MProblem::getGeneralStatisticsOfSolutions($model),
            'tests'     => $testFilesManager->getFiles(),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Problem;
        $examples = array();

        if (Yii::app()->request->hasPost('Problem')) {
            ### Erase empty rows from array
            for ($i=0; $i<10; $i++) {
                if (empty($_POST['Problem']['examples'][$i]['input']) && empty($_POST['Problem']['examples'][$i]['output'])){
                    unset($_POST['Problem']['examples'][$i]);
                }
            }

            $_POST['Problem']['examples'] = json_encode($_POST['Problem']['examples']);
            $model->attributes=Yii::app()->request->post('Problem');

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create',array(
            'model'=>$model,
            'examples'=>$examples,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->request->hasPost('Problem')) {
            ### Erase empty rows from array
            for ($i=0; $i<10; $i++){
                if (empty($_POST['Problem']['examples'][$i]['input']) && empty($_POST['Problem']['examples'][$i]['output'])){
                    unset($_POST['Problem']['examples'][$i]);
                }
            }

            $_POST['Problem']['examples'] = json_encode($_POST['Problem']['examples']);
            $model->attributes=Yii::app()->request->post('Problem');
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        Solution::model()->deleteAllByAttributes(array('p_id' => $id));

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!Yii::app()->request->hasGet('ajax')) {
            $this->redirect(Yii::app()->request->hasPost('returnUrl') ? Yii::app()->request->post('returnUrl') : array('admin'));
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $model = new Problem('search');
        $model->unsetAttributes();  // clear any default values
        if (Yii::app()->request->hasGet('Problem')) {
            $model->attributes = Yii::app()->request->get('Problem');
        }

        $this->render('index',array(
            'model'=>$model,
        ));
    }

    /**
     * @param $p_id
     * @param $file
     * @return mixed
     * @throws CFileException
     */
    public function actionDownloadTest($p_id, $file)
    {
        $file = str_replace('-', '/', $file);
        $tests_dir = Yii::app()->basePath . '/../storage/tests/' . $p_id;
        if (file_exists($tests_dir . $file)) {
            return Yii::app()->getRequest()->sendFile(substr($file, strrpos($file, '/') + 1), file_get_contents($tests_dir . $file));
        } else {
            throw new CFileException('File not exists');
        }
    }

    /**
     * @param $p_id
     * @param $file
     * @throws CFileException
     */
    public function actionDeleteTest($p_id, $file)
    {
        $file = str_replace('-', '/', $file);
        $tests_dir = Yii::app()->basePath . '/../storage/tests/' . $p_id;
        if (file_exists($tests_dir . $file)) {
            if (!unlink($tests_dir . $file)) {
                throw new CFileException('Can\'t delete file');
            }
        } else {
            throw new CFileException('File not exists');
        }

        $this->redirect(array('tests', 'id' => $p_id));
    }


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionTests($id)
    {
        $model = $this->loadModel($id);
        $uploadTestForm = new UploadTestForm();

        $testFilesManager = new TestFilesManager($id);
        $testFilesManager->search();
        $tests = $testFilesManager->getFiles();

        if (isset($_POST['UploadTestForm'])) {
            $uploadTestForm->attributes = $_POST['UploadTestForm'];
            $uploadTestForm->file = CUploadedFile::getInstance($uploadTestForm, 'file');

            if ($uploadTestForm->validate()) {
                if ($uploadTestForm->type == 'input') {
                    if (file_exists(Yii::app()->basePath . '/../storage/tests/' . $id . '/input/' . $uploadTestForm->ordinalNumber . '.txt')) {
                        $uploadTestForm->addError('file', 'Входной файл с ' . $uploadTestForm->ordinalNumber . '-ым порядковым номер уже существует. Сначал удалите его и повторите загрузку файла.');
                    } else {
                        @mkdir(Yii::app()->basePath . '/../storage/tests/' . $id . '/input');

                        $uploadTestForm->file->saveAs(Yii::app()->basePath . '/../storage/tests/' . $id . '/input/' . $uploadTestForm->ordinalNumber . '.txt');
                    }
                } else {
                    $outputNumber = 1;
                    if (array_key_exists($uploadTestForm->ordinalNumber, $tests)) {
                        $outputNumber = max($tests[$uploadTestForm->ordinalNumber]['output']) + 1;
                    }

                    @mkdir(Yii::app()->basePath . '/../storage/tests/' . $id . '/output');
                    @mkdir(Yii::app()->basePath . '/../storage/tests/' . $id . '/output/' . $uploadTestForm->ordinalNumber);

                    $uploadTestForm->file->saveAs(Yii::app()->basePath . '/../storage/tests/' . $id . '/output/' . $uploadTestForm->ordinalNumber . '/' . $outputNumber . '.txt');
                }

                $this->redirect(array('tests', 'id' => $id));
            }
        }

        $this->render('tests',array(
            'model'         => $model,
            'tests'         => $tests,
            'uploadTestForm'=> $uploadTestForm,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Problem the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Problem::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}