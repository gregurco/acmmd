<?php

class ContestController extends MainAdminController
{
	public function actionIndex()
	{
        $model = new Contest('search');
        $model->unsetAttributes();  // clear any default values
        if (Yii::app()->request->hasGet('Contest')) {
            $model->attributes = Yii::app()->request->get('Contest');
        }

		$this->render('index',array(
            'model' => $model,
        ));
	}

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/libs/chosen/chosen.jquery.min.js?v=1.3.0',CClientScript::POS_END);
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/libs/chosen/chosen.min.css?v=1.3.0');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/libs/datetimepicker/jquery.datetimepicker.js?v=2.4.1',CClientScript::POS_END);
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/libs/datetimepicker/jquery.datetimepicker.css?v=2.4.1');

        $model = new Contest;

        if (Yii::app()->request->hasPost('Contest')) {
            $model->attributes=Yii::app()->request->post('Contest');
            if ($model->save()) {
                for ($i = 9; $i <=12; $i++) {
                    $selected_problems = Yii::app()->request->post("selected_problems_{$i}", array());

                    foreach ($selected_problems as $new_problem_id) {
                        $contest_problem = new ContestProblem();
                        $contest_problem->contest_id = $model->id;
                        $contest_problem->problem_id = $new_problem_id;
                        $contest_problem->grade      = $i;
                        $contest_problem->save();
                    }
                }

                for ($i = 9; $i <=12; $i++) {
                    $selected_users = Yii::app()->getRequest()->post("selected_users_{$i}", array());

                    foreach ($selected_users as $new_user_id) {
                        $contest_user = new ContestUser();
                        $contest_user->contest_id = $model->id;
                        $contest_user->user_id    = $new_user_id;
                        $contest_user->grade      = $i;
                        $contest_user->save();
                    }
                }

                $this->redirect(array('contest/'));
            }
        }

        $problems = CHtml::listData(Problem::model()->findAll(), 'id', 'name');

        $participants = array();
        $participants_obj = User::model()->findAll();
        foreach ($participants_obj as $participant_obj){
            $participants[$participant_obj->id] = $participant_obj->login . ' (' . $participant_obj->name . ' ' . $participant_obj->surname . ')';
        }

        $this->render('create',array(
            'model'                 => $model,
            'problems'              => $problems,
            'problem_options'       => array(9 => array(), 10 => array(), 11 => array(), 12 => array()),
            'participants'          => $participants,
            'participants_options'  => array(9 => array(), 10 => array(), 11 => array(), 12 => array()),
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!Yii::app()->request->hasGet('ajax')) {
            $this->redirect(Yii::app()->request->hasPost('returnUrl') ? Yii::app()->request->post('returnUrl') : array('contest/'));
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/libs/chosen/chosen.jquery.min.js?v=1.3.0',CClientScript::POS_END);
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/libs/chosen/chosen.min.css?v=1.3.0');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/libs/datetimepicker/jquery.datetimepicker.js?v=2.4.1',CClientScript::POS_END);
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/libs/datetimepicker/jquery.datetimepicker.css?v=2.4.1');

        $model = $this->loadModel($id);

        if (Yii::app()->request->hasPost('Contest')) {
            $model->attributes = Yii::app()->request->post('Contest');
            if ($model->save()) {
                for ($i = 9; $i <=12; $i++) {
                    $selected_problems = Yii::app()->request->post("selected_problems_{$i}", array());

                    $contest_problems = CHtml::listData(ContestProblem::model()->findAllByAttributes(array('contest_id' => $model->id, 'grade' => $i)), 'id', 'problem_id');

                    foreach (array_diff($selected_problems, $contest_problems) as $new_problem_id) {
                        $contest_problem = new ContestProblem();
                        $contest_problem->contest_id = $model->id;
                        $contest_problem->problem_id = $new_problem_id;
                        $contest_problem->grade      = $i;
                        $contest_problem->save();
                    }

                    foreach (array_diff($contest_problems, $selected_problems) as $delete_problem_id) {
                        ContestProblem::model()->deleteAllByAttributes(array('contest_id' => $model->id, 'problem_id' => $delete_problem_id, 'grade' => $i));
                    }
                }

                for ($i = 9; $i <=12; $i++) {
                    $selected_users = Yii::app()->getRequest()->post("selected_users_{$i}", array());

                    $contest_users = CHtml::listData(ContestUser::model()->findAllByAttributes(array('contest_id' => $model->id, 'grade' => $i)), 'id', 'user_id');

                    foreach (array_diff($selected_users, $contest_users) as $new_user_id) {
                        $contest_user = new ContestUser();
                        $contest_user->contest_id = $model->id;
                        $contest_user->user_id    = $new_user_id;
                        $contest_user->grade      = $i;
                        $contest_user->save();
                    }

                    foreach (array_diff($contest_users, $selected_users) as $delete_problem_id) {
                        ContestUser::model()->deleteAllByAttributes(array('contest_id' => $model->id, 'user_id' => $delete_problem_id, 'grade' => $i));
                    }
                }

                $this->redirect(array('contest/'));
            }
        }

        $problems = CHtml::listData(Problem::model()->findAll(), 'id', 'name');

        $problem_options = array(9 => array(), 10 => array(), 11 => array(), 12 => array());
        for ($i = 9; $i <=12; $i++) {
            $contest_problems = ContestProblem::model()->findAllByAttributes(array('contest_id' => $model->id, 'grade' => $i));
            foreach ($contest_problems as $contest_problem) {
                $problem_options[$i][$contest_problem->problem_id] = array('selected' => 'selected');
            }
        }

        $participants = array();
        $participants_obj = User::model()->findAll();
        foreach ($participants_obj as $participant_obj){
            $participants[$participant_obj->id] = $participant_obj->login . ' (' . $participant_obj->name . ' ' . $participant_obj->surname . ')';
        }

        $participants_options = array(9 => array(), 10 => array(), 11 => array(), 12 => array());
        for ($i = 9; $i <=12; $i++) {
            $contest_users = ContestUser::model()->findAllByAttributes(array('contest_id' => $model->id, 'grade' => $i));
            foreach ($contest_users as $contest_user) {
                $participants_options[$i][$contest_user->user_id] = array('selected' => 'selected');
            }
        }

        $this->render('update',array(
            'model'                 => $model,
            'problems'              => $problems,
            'problem_options'       => $problem_options,
            'participants'          => $participants,
            'participants_options'  => $participants_options,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
     * @param $id
     * @throws CHttpException
     */
    public function actionViewResults($id)
    {
        $model = $this->loadModel($id);

        $temp_table = $table = $contest_problems = array();

        $final_counting = false;

        foreach ($model->contestUsers as $contest_user) {
            $temp_table[$contest_user->user->id] = array(
                'contest_user'  => $contest_user,
                'problems'       => array()
            );
        }

        $contest_problem_ids = array();
        foreach ($model->contestProblems as $contest_problem) {
            $contest_problem_ids[] = $contest_problem->id;
            $contest_problems[$contest_problem->grade][] = $contest_problem;
        }
        $criteria = new CDbCriteria();
        $criteria->addInCondition('contest_problem_id', $contest_problem_ids);
        $contest_solutions = ContestSolution::model()->findAll($criteria);
        foreach ($contest_solutions as $contest_solution) {
            $temp_table[$contest_solution->solution->u_id]['problems'][$contest_solution->contest_problem_id][] = $contest_solution;
            if (in_array($contest_solution->solution->status, array(Solution::STATUS_CONTEST_WAITING_TESTING, Solution::STATUS_CONTEST_TESTING, Solution::STATUS_CONTEST_WAITING_COMPILING, Solution::STATUS_CONTEST_COMPILING))){
                $final_counting = true;
            }
        }

        foreach ($temp_table as $user_id => $user) {
            $score = $accepted_problems = 0;

            foreach ($user['problems'] as $problem) {
                if (count($problem)){
                    if (in_array($problem[count($problem) - 1]->solution->status, array(Solution::STATUS_CONTEST_ACCEPT_PRE_TESTING, Solution::STATUS_CONTEST_WAITING_TESTING, Solution::STATUS_CONTEST_TESTING, Solution::STATUS_CONTEST_WAITING_COMPILING, Solution::STATUS_CONTEST_COMPILING, Solution::STATUS_CONTEST_FINISH))){
                        $accepted_problems++;
                        $score += $problem[count($problem) - 1]->solution->result;
                    }
                }
            }

            $temp_table[$user_id]['accepted_problems'] = $accepted_problems;
            $temp_table[$user_id]['score'] = $score;
        }

        foreach ($temp_table as $key => $val) {
            $table[$val['contest_user']->grade][$key] = $val;
        }

        if ($final_counting) {
            Yii::app()->user->setFlash('counting_started', Yii::t('interface', "Подсчет баллов идет!"));
        }

        $this->render('view_results',array(
            'model'             => $model,
            'table'             => $table,
            'contest_problems'  => $contest_problems,
        ));
    }

    /**
     * @param $id
     * @throws CHttpException
     */
    public function actionStartCounting($id)
    {
        $model = $this->loadModel($id);

        foreach ($model->contestUsers as $contest_user) {
            foreach ($model->contestProblems as $contest_problem) {
                if ($contest_problem->grade == $contest_user->grade) {
                    $criteria = new CDbCriteria();
                    $criteria->with = array('solution');
                    $criteria->compare('contest_problem_id', $contest_problem->id);
                    $criteria->compare('solution.u_id', $contest_user->user_id);
                    $criteria->order = 'solution.id DESC';
                    $criteria->limit = 1;

                    $contest_solution = ContestSolution::model()->findAll($criteria);
                    if (count($contest_solution) && $contest_solution[0]->solution->status == Solution::STATUS_CONTEST_ACCEPT_PRE_TESTING) {
                        $contest_solution[0]->solution->status = Solution::STATUS_CONTEST_WAITING_COMPILING;
                        $contest_solution[0]->solution->save();

                        Solution::model()->updateByPk($contest_solution[0]->solution->id, array('status' => Solution::STATUS_CONTEST_WAITING_COMPILING));
                    }
                }
            }
        }

        Yii::app()->user->setFlash('counting_started', Yii::t('interface', "Подсчет баллов идет!"));

        $this->redirect(array('contest/viewResults', 'id' => $id));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Contest the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Contest::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}