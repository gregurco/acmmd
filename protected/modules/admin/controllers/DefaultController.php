<?php

class DefaultController extends MainAdminController
{
	public function actionIndex()
	{
        $totalUsers = User::model()->count();
        $totalProblems = Problem::model()->count();
        $totalArticles = Article::model()->count();

		$this->render('index', array(
            'totalUsers'    => $totalUsers,
            'totalProblems' => $totalProblems,
            'totalArticles' => $totalArticles,
        ));
	}

    public function actionSearch($text)
    {
        $news_model = new News('search');
        $news_model->title_ru = $text;

        $article_model = new Article('search');
        $article_model->title_ru = $text;

        $problem_model = new Problem('search');
        $problem_model->name_ru = $text;

        $this->render('search', array(
            'news_model'    => $news_model,
            'article_model' => $article_model,
            'problem_model' => $problem_model,
        ));
    }
}