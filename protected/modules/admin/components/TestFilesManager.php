<?php

class TestFilesManager
{
    private $problem_id;

    private $files = [];

    private $ignored_files = ['.', '..', '.gitkeep', '.htaccess'];

    /**
     * @param $problem_id
     */
    public function __construct($problem_id)
    {
        $this->problem_id = $problem_id;
    }

    public function search()
    {
        $tests_dir = Yii::app()->basePath . '/../storage/tests/' . $this->problem_id;

        if (is_dir($tests_dir)) {
            if (is_dir($tests_dir . '/input')) {
                foreach (scandir($tests_dir . '/input') as $test_number) {
                    if (!in_array($test_number, $this->ignored_files)) {
                        $this->files[str_replace('.txt', '', $test_number)] = [
                            'input'  => $test_number,
                            'output' => [],
                        ];

                    }
                }
            }

            if ($tests_dir . '/output') {
                foreach (scandir($tests_dir . '/output') as $test_number_dir) {
                    if (!in_array($test_number_dir, $this->ignored_files)) {
                        foreach (scandir($tests_dir . '/output/' . $test_number_dir) as $test_number) {
                            if (!in_array($test_number, $this->ignored_files)) {
                                if (!array_key_exists($test_number_dir, $this->files)) {
                                    $this->files[$test_number_dir]['input'] = '';
                                }
                                $this->files[$test_number_dir]['output'][] = $test_number;
                            }
                        }
                    }
                }
            }
        }

        ksort($this->files);
    }

    /**
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }
}