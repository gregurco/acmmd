<?php

class MainAdminController extends Controller
{
    /** @var string  */
    public $layout='/layouts/main';

    /** @var array  */
    public $menu = array();

    /** @var string  */
    public $main_header = '';

    /** @var string  */
    public $small_header = '';

    public function init()
    {
        $this->createLeftMenu();

        parent::init();
    }

    private function createLeftMenu()
    {
        $this->menu = array(
            array(
                'label' => Yii::t('menu', 'Статистика'),
                'url'   => array('default/index'),
                'icon'  => 'glyphicon-th',
            ),
            array(
                'label' => Yii::t('menu', 'Новости'),
                'url'   => array('news/'),
                'icon'  => 'glyphicon-info-sign',
                'items' => array(
                    array('label'=>Yii::t('interface','Список новость'), 'url'=>array('news/index')),
                    array('label'=>Yii::t('interface','Создать новость'), 'url'=>array('news/create')),
                    array('label'=>Yii::t('interface','Список комментариев'), 'url'=>array('newsComment/index')),
                ),
            ),
            array(
                'label' => Yii::t('menu', 'Статьи'),
                'url'   => array('article/'),
                'icon'  => 'glyphicon-list-alt',
                'items' => array(
                    array('label'=>Yii::t('interface','Список статей'), 'url'=>array('article/index')),
                    array('label'=>Yii::t('interface','Создать статью'), 'url'=>array('article/create')),
                    array('label'=>Yii::t('interface','Список групп статей'), 'url'=>array('groupArticle/index')),
                    array('label'=>Yii::t('interface','Создать группу статей'), 'url'=>array('groupArticle/create')),
                ),
            ),
            array(
                'label' => Yii::t('menu', 'Пользователи'),
                'url'   => array('user/'),
                'icon'  => 'glyphicon-user',
                'items' => array(
                    array('label'=>Yii::t('interface','Список пользователей'), 'url'=>array('user/index')),
                    array('label'=>Yii::t('interface','Создать пользователя'), 'url'=>array('user/create')),
                    array('label'=>Yii::t('interface','Генерация пользователей'), 'url'=>array('user/generate')),
                ),
            ),
            array(
                'label'=>Yii::t('menu', 'Задачи'),
                'url'=>array('problem/'),
                'items' => array(
                    array('label'=>Yii::t('interface','Список задач'), 'url'=>array('problem/index')),
                    array('label'=>Yii::t('interface','Создать задачу'), 'url'=>array('problem/create')),
                ),
            ),
            array('label'=>Yii::t('menu', 'Решения'), 'url'=>array('solution/')),
            array(
                'label'=>Yii::t('menu', 'Олимпиады'),
                'url'=>array('contest/'),
                'icon'  => '',
                'items' => array(
                    array('label'=>Yii::t('interface','Список олимпиад'), 'url'=>array('contest/index')),
                    array('label'=>Yii::t('interface','Создать олимпиаду'), 'url'=>array('contest/create'))
                ),
            ),
            array(
                'label' => Yii::t('menu', 'Настройки'),
                'url'   => array('config/'),
                'icon'  => 'glyphicon-wrench'
            )
        );
    }

    /**
     * @param $data
     * @throws Exception
     */
    public function renderGridView($data){
        $this->widget('booster.widgets.TbGridView', array(
            'id'            => $data['id'],
            'type'          => 'striped bordered condensed',
            'summaryText'   => '',
            'dataProvider'  => isset($data['dataProvider'])? $data['dataProvider'] : $data['model']->search(),
            'filter'        => $data['model'],
            'pager'         => array(
                'class'         => 'booster.widgets.TbPager',
                'alignment'     => 'centered'
            ),
            'columns'       => $data['columns']
        ));
    }
}