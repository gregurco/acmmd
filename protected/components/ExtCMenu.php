<?php

Yii::import('zii.widgets.CMenu');
class ExtCMenu extends CMenu
{
    public $itemClassWithSubMenu;

    /**
     * Recursively renders the menu items.
     * @param array $items the menu items to be rendered recursively
     */
    protected function renderMenuRecursive($items)
    {
        $count=0;
        $n=count($items);
        foreach($items as $item)
        {
            $count++;
            $options=isset($item['itemOptions']) ? $item['itemOptions'] : array();
            $class=array();
            if($item['active'] && $this->activeCssClass!='') {
                $class[] = $this->activeCssClass;
            }
            if($count===1 && $this->firstItemCssClass!==null)
                $class[]=$this->firstItemCssClass;
            if($count===$n && $this->lastItemCssClass!==null)
                $class[]=$this->lastItemCssClass;
            if($this->itemCssClass!==null)
                $class[]=$this->itemCssClass;
            if(isset($item['items']) && count($item['items']) && $this->itemClassWithSubMenu){
                $class[] = $this->itemClassWithSubMenu;
            }
            if($class!==array())
            {
                if(empty($options['class']))
                    $options['class']=implode(' ',$class);
                else
                    $options['class'].=' '.implode(' ',$class);
            }

            echo CHtml::openTag('li', $options);

            $menu=$this->renderMenuItem($item);
            if(isset($this->itemTemplate) || isset($item['template']))
            {
                $template=isset($item['template']) ? $item['template'] : $this->itemTemplate;
                echo strtr($template,array('{menu}'=>$menu));
            }
            else
                echo $menu;

            if(isset($item['items']) && count($item['items']))
            {
                echo "\n".CHtml::openTag('ul',isset($item['submenuOptions']) ? $item['submenuOptions'] : $this->submenuHtmlOptions)."\n";
                $this->renderMenuRecursive($item['items']);
                echo CHtml::closeTag('ul')."\n";
            }

            echo CHtml::closeTag('li')."\n";
        }
    }

    /**
     * Renders the content of a menu item.
     * Note that the container and the sub-menus are not rendered here.
     * @param array $item the menu item to be rendered. Please see {@link items} on what data might be in the item.
     * @return string
     * @since 1.1.6
     */
    protected function renderMenuItem($item)
    {
        if(isset($item['url']))
        {
            $label=$this->linkLabelWrapper===null ? $item['label'] : CHtml::tag($this->linkLabelWrapper, $this->linkLabelWrapperHtmlOptions, $item['label']);
            return CHtml::link($this->drawLeftGlyphs($item) . $label . $this->drawRightPointer($item),$item['url'],isset($item['linkOptions']) ? $item['linkOptions'] : array());
        }
        else
            return CHtml::tag('span',isset($item['linkOptions']) ? $item['linkOptions'] : array(), $item['label']);
    }

    protected function drawRightPointer($item){
        if (isset($item['items']) and count($item['items'])) {
            return CHtml::tag('i', array('class' => 'fa fa-angle-left pull-right'), '');
        }
        return '';
    }

    protected function drawLeftGlyphs($item){
        if (isset($item['icon']) and !empty($item['icon'])) {
            return CHtml::tag('i', array('class' => 'glyphicon ' . $item['icon']), '');
        }
        return '';
    }

    protected function isItemActive($item,$route)
    {
        if(isset($item['url']) && is_array($item['url']) && !strcasecmp(trim(CHtml::normalizeUrl($item['url']), '/'), Yii::app()->request->pathInfo))
            //if(isset($item['url']) && is_array($item['url']) && !strcasecmp(trim($item['url'][0],'/'),$route)) // дефолтный вариант условия
        {
            if(count($item['url'])>1)
            {
                foreach(array_splice($item['url'],1) as $name=>$value)
                {
                    if(!isset($_GET[$name]) || $_GET[$name]!=$value)
                        return false;
                }
            }
            return true;
        }
        return false;
    }
}