<?php

/**
 * Manager for Top
 *
 */
class MTop {

    const USERS_PER_PAGE = 100;

    /**
     * @return CSqlDataProvider
     */
    static function getGeneralScore() {
        $sql     = 'SELECT b.id, a.score, b.login, b.name, b.surname FROM (SELECT c.u_id id,SUM(c.rslt) score FROM (SELECT s.u_id, s.p_id, MAX(s.result) rslt FROM a_solution s WHERE s.`status` BETWEEN ' . Solution::STATUS_WAITING_COMPILING . ' AND ' . Solution::STATUS_COMPILE_ERROR . ' GROUP BY s.u_id, s.p_id) c GROUP BY c.u_id) a RIGHT JOIN (SELECT * from a_user) b ON a.id = b.id';
        $rawData = Yii::app()->db->createCommand($sql);
        $count   = Yii::app()->db->createCommand('SELECT COUNT(*) FROM (' . $sql . ') as count_alias')->queryScalar(); //the count

        return new CSqlDataProvider($rawData, array(
            'keyField'      => 'id',
            'totalItemCount'=> $count,
            'sort'          => array(
                'attributes'    => array(
                    'score'
                ),
                'defaultOrder'  => array(
                    'score' => CSort::SORT_DESC, //default sort value
                ),
            ),
            'pagination'    => array(
                'pageSize' => self::USERS_PER_PAGE,
            ),
        ));
    }
}