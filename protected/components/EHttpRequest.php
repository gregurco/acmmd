<?php
class EHttpRequest extends CHttpRequest
{
    private $_requestUri;

    /**
     * @return string
     */
    public function getRequestUri()
    {
        if ($this->_requestUri === null) {
            $this->_requestUri = DMultilangHelper::processLangInUrl(parent::getRequestUri());
        }

        return $this->_requestUri;
    }

    /**
     * @param $name
     * @return bool
     */
    public function hasGet($name){
        return isset($_GET[$name]);
    }

    /**
     * @param $name
     * @return bool
     */
    public function hasPost($name){
        return isset($_POST[$name]);
    }

    /**
     * @param $name
     * @param null $defaultValue
     * @return null
     */
    public function post($name, $defaultValue=null)
    {
        return isset($_POST[$name]) ? $_POST[$name] : $defaultValue;
    }

    /**
     * @param $name
     * @param null $defaultValue
     * @return null
     */
    public function get($name, $defaultValue=null)
    {
        return isset($_GET[$name]) ? $_GET[$name] : $defaultValue;
    }
}