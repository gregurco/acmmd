<?php

class MProblem extends CComponent {

    /**
     * @param $id
     * @return array|mixed|null|static
     * @throws CHttpException
     */
    static function loadPublicModel($id)
    {
        $model = Problem::model()->findByAttributes(array('id' => $id, 'hide' => 0));
        if ($model===null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * @return string
     */
    static function getSendForbiddenMessage() {
        if (!Yii::app()->config->get('SEND.SOLUTION')) {
            return Yii::t('interface', 'Отправка решений временно недоступна.');
        } elseif (Yii::app()->user->isGuest) {
            return Yii::t('interface', 'Для отправки решения, вы должны авторизироваться.');
        }
        return '';
    }

    /**
     * @param $done
     * @param $needed
     * @return mixed
     */
    static function getStatus($done, $needed){
        if ($needed && $done >= $needed){
            return Yii::t('interface',"Решена");
        }
        return Yii::t('interface',"Начата");
    }

    /**
     * Get problems available for simple user
     *
     * @return array|mixed|null|static
     */
    public static function getAvailable(){
        $criteria = new CDbCriteria;
        $criteria->condition = 'hide = :hide';
        $criteria->params = array(':hide' => 0);
        $criteria->order = 'id ASC';

        return Problem::model()->findAll($criteria);
    }

    /**
     * @param Problem $problem
     * @return array
     */
    static function getGeneralStatisticsOfSolutions(Problem $problem) {
        $result = array(
            'general' => array(
                'completed' => 0,
                'partial'   => 0,
                'failed'    => 0,
            ),
            'users' => array(
                'completed'    => array(),
                'partial'      => array(),
                'not_resolved' => 0,
            ),
        );

        foreach ($problem->solution as $solution) {
            /** @var $solution Solution */
            if ($solution->result == $problem->tests * 10) {
                $result['general']['completed']++;
                $result['users']['completed'][$solution->u_id] = 1;
            } elseif ($solution->result == 0) {
                $result['general']['failed']++;
            } else {
                $result['general']['partial']++;
                $result['users']['partial'][$solution->u_id] = 1;
            }
        }

        foreach ($result['users']['completed'] as $user_id => $val) {
            unset($result['users']['partial'][$user_id]);
        }

        $result['users']['partial']      = count($result['users']['partial']);
        $result['users']['completed']    = count($result['users']['completed']);
        $result['users']['not_resolved'] = User::model()->count() - $result['users']['partial'] - $result['users']['completed'];

        return $result;
    }
}