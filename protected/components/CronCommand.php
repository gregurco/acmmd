<?php
set_time_limit(0);

class CronCommand extends CConsoleCommand
{
    const STATUS_WAITING_COMPILING = 1;
    const STATUS_COMPILING = 2;
    const STATUS_WAITING_TESTING = 3;
    const STATUS_TESTING = 4;
    const STATUS_FINISH = 5;
    const STATUS_COMPILE_ERROR = 10;

    const STATUS_CONTEST_WAITING_PRE_COMPILING = 21;
    const STATUS_CONTEST_PRE_COMPILING = 22;
    const STATUS_CONTEST_COMPILE_ERROR = 23;
    const STATUS_CONTEST_WAITING_PRE_TESTING = 24;
    const STATUS_CONTEST_PRE_TESTING = 25;
    const STATUS_CONTEST_FAIL_PRE_TESTING = 26;
    const STATUS_CONTEST_ACCEPT_PRE_TESTING = 27;
    const STATUS_CONTEST_WAITING_COMPILING = 28;
    const STATUS_CONTEST_COMPILING = 29;
    const STATUS_CONTEST_WAITING_TESTING = 30;
    const STATUS_CONTEST_TESTING = 31;
    const STATUS_CONTEST_FINISH = 32;

    public $dbConnection = '';

    /** @var CConsole */
	private $console;

    public function run() {
		$this->console = new CConsole();

		while ($this->compile()) {
            sleep(5);
        }
    }

    /**
     * @return bool
     */
    private function compile(){
        $criteria = new CDbCriteria();
        $criteria->addInCondition('status', array(self::STATUS_WAITING_COMPILING, self::STATUS_CONTEST_WAITING_PRE_COMPILING, self::STATUS_CONTEST_WAITING_COMPILING));

        $solutions = Solution::model()->findAll($criteria);
        foreach ($solutions as $solution) {
            if ($solution->status == self::STATUS_WAITING_COMPILING) {
                Solution::model()->updateByPk($solution->id, array('status' => self::STATUS_COMPILING));
            } elseif ($solution->status == self::STATUS_CONTEST_WAITING_PRE_COMPILING) {
                Solution::model()->updateByPk($solution->id, array('status' => self::STATUS_CONTEST_PRE_COMPILING));
            }
			
            $dir = __DIR__ . '/../../testing';
			file_put_contents($dir.'/'.$this->fileName($solution->compiler), $solution->file_text);

            if ($solution->compiler == 'FPC'){
                $this->console->exec('cd '.$dir.'; fpc '.$dir.'/1 2> '.$dir.'/log.txt');
            }elseif($solution->compiler == 'GCC'){
                $this->console->exec('cd '.$dir.'; gcc '.$dir.'/'.$this->fileName($solution->compiler).' -o '.$dir.'/1 2> '.$dir.'/log.txt');
            }elseif($solution->compiler == 'G++'){
                $this->console->exec('cd '.$dir.'; g++ '.$dir.'/'.$this->fileName($solution->compiler).' -o '.$dir.'/1 2> '.$dir.'/log.txt');
            }elseif($solution->compiler == 'Prolog'){
                $this->console->exec('cd '.$dir.'; swipl --goal=goal --stand_alone=true -o 1 -c 1.pl 2> '.$dir.'/log.txt');
            }elseif($solution->compiler == 'Java'){
                $this->console->exec('cd '.$dir.'; javac '.$dir.'/'.$this->fileName($solution->compiler).' 2> '.$dir.'/log.txt');
            }
			
			$compile_result = file_get_contents($dir.'/log.txt');
            $compile_result = nl2br($compile_result);

            if (
                ($solution->compiler == 'Java' && file_exists($dir.'/Main.class')) ||
                ($solution->compiler != 'Java' && file_exists($dir.'/1'))
            ){
                if ($solution->status == self::STATUS_WAITING_COMPILING) {
                    Solution::model()->updateByPk($solution->id, array('status' => self::STATUS_WAITING_TESTING, 'log_compile' => $compile_result));
                    $this->test($solution);
                } elseif ($solution->status == self::STATUS_CONTEST_WAITING_PRE_COMPILING) {
                    Solution::model()->updateByPk($solution->id, array('status' => self::STATUS_CONTEST_WAITING_PRE_TESTING, 'log_compile' => $compile_result));
                    $this->contest_pre_test($solution);
                } elseif ($solution->status == self::STATUS_CONTEST_WAITING_COMPILING) {
                    Solution::model()->updateByPk($solution->id, array('status' => self::STATUS_CONTEST_WAITING_TESTING, 'log_compile' => $compile_result));
                    $this->contest_test($solution);
                }
            }else{
                if ($solution->status == self::STATUS_WAITING_COMPILING) {
                    Solution::model()->updateByPk($solution->id, array('status' => self::STATUS_COMPILE_ERROR, 'log_compile' => $compile_result, 'result' => 0));
                } elseif ($solution->status == self::STATUS_CONTEST_WAITING_PRE_COMPILING || $solution->status == self::STATUS_CONTEST_WAITING_COMPILING) {
                    Solution::model()->updateByPk($solution->id, array('status' => self::STATUS_CONTEST_COMPILE_ERROR, 'log_compile' => $compile_result, 'result' => 0));
                }
            }
        }

        $criteria = new CDbCriteria;
        $criteria->addInCondition('status', array(self::STATUS_COMPILING, self::STATUS_WAITING_TESTING, self::STATUS_TESTING));
        Solution::model()->updateAll(array('status' => self::STATUS_WAITING_COMPILING, 'log_compile' => NULL, 'tests' => NULL, 'result' => 0), $criteria);

        $criteria = new CDbCriteria;
        $criteria->addInCondition('status', array(self::STATUS_CONTEST_PRE_COMPILING, self::STATUS_CONTEST_WAITING_PRE_TESTING, self::STATUS_CONTEST_PRE_TESTING));
        Solution::model()->updateAll(array('status' => self::STATUS_CONTEST_WAITING_PRE_COMPILING, 'log_compile' => NULL, 'tests' => NULL, 'result' => 0), $criteria);

		gc_collect_cycles();
		return true;
    }

    /**
     * @param Solution $solution
     * @throws Exception
     */
    private function test($solution){
        Solution::model()->updateByPk($solution->id, array('status' => self::STATUS_TESTING));
        $tests      = $tests_text = array();
		$result     = 0;
        $dir_input  = __DIR__ . '/../../storage/tests/'.$solution->p_id.'/input';
        $dir_output = __DIR__ . '/../../storage/tests/'.$solution->p_id.'/output';
        $dir_to     = __DIR__ . '/../../testing';

        for ($i=1; $i<=10; $i++) {
            $tests[$i] = false;

            copy($dir_input."/".$i.'.txt', $dir_to."/input.txt");
			
			if ($solution->compiler == 'Java') {
				$execute_result = $this->execute_shell(
//					"cd ".$dir_to."; ulimit -v ".(1024*$solution->problem->limit_memory*100)."; time sudo time -v -o './log_time.txt' timeout ".($solution->problem->limit_time*10)." java Main;",
					"cd ".$dir_to."; time sudo time -v -o './log_time.txt' timeout ".($solution->problem->limit_time*10)." java Main;",
					$dir_to
				);
			} else {
                $execute_result = $this->execute_shell(
//					"cd ".$dir_to."; ulimit -v ".(1024*$solution->problem->limit_memory)."; time sudo time -v -o './log_time.txt' timeout ".$solution->problem->limit_time." ./1;",
					"cd ".$dir_to."; time sudo time -v -o './log_time.txt' timeout ".$solution->problem->limit_time." ./1;",
					$dir_to
				);
			}

            if ($execute_result[0] == 'time') {
                $tests_text[$i] = array('time', $execute_result[1]);
            } elseif ($execute_result[0] == 'memory') {
				$tests_text[$i] = array('memory', $execute_result[1]);
            } elseif (!file_exists($dir_to.'/output.txt')) {
                $tests_text[$i] = array('outputDoesntExist', $execute_result[1]);
            } else {
                $file1 = file($dir_to . '/output.txt');
                $handle = opendir($dir_output . DIRECTORY_SEPARATOR . $i);
                while ($file = readdir($handle)) {
                    if (($file!=".") && ($file!="..") && !$tests[$i]) {
                        $file2 = file($dir_output . DIRECTORY_SEPARATOR . $i . DIRECTORY_SEPARATOR . $file);

                        if ($solution->problem->trim_lines) {
                            $file1 = array_map('trim', $file1);
                            $file2 = array_map('trim', $file2);
                        }

                        if ($solution->problem->empty_last_line) {
                            if (count($file1) && ($file1[count($file1) - 1] == '' || $file1[count($file1) - 1] == PHP_EOL)) {
                                $file1 = array_pop($file1);
                            }

                            if ($file2[count($file2) - 1] == '' || $file2[count($file2) - 1] == PHP_EOL) {
                                $file2 = array_pop($file2);
                            }
                        }

                        $diff = array_diff($file1, $file2);
                        if (empty($diff)) {
                            $tests[$i] = true;
							$result += 10;
                            $tests_text[$i] = array('ok', $execute_result[1]);
                        }
                    }
                }

				if (!$tests[$i]) {
                    $tests_text[$i] = array('wrongAnswer', $execute_result[1]);
                }
            }

            @unlink($dir_to."/input.txt");
            @unlink($dir_to."/output.txt");
            @unlink($dir_to."/log_time.txt");
        }
        Solution::model()->updateByPk($solution->id, array('status' => self::STATUS_FINISH, 'tests' => json_encode($tests_text), 'result' => $result));
		$this->clearDir();
    }

    /**
     * @param $solution
     * @throws Exception
     */
    private function contest_pre_test($solution){
        Solution::model()->updateByPk($solution->id, array('status' => self::STATUS_CONTEST_PRE_TESTING));
        $tests      = $tests_text = array();
		$result     = 0;
        $dir_input  = __DIR__ . '/../../storage/tests/'.$solution->p_id.'/input';
        $dir_output = __DIR__ . '/../../storage/tests/'.$solution->p_id.'/output';
        $dir_to     = __DIR__ . '/../../testing';

        for ($i=1; $i<=1; $i++){
            $tests[$i] = false;

            copy($dir_input."/pre_".$i.'.txt', $dir_to."/input.txt");

			if ($solution->compiler == 'Java'){
				$execute_result = $this->execute_shell(
//					"cd ".$dir_to."; ulimit -v ".(1024*$solution->problem->limit_memory*100)."; time sudo time -v -o './log_time.txt' timeout ".($solution->problem->limit_time*10)." java Main;",
					"cd ".$dir_to."; time sudo time -v -o './log_time.txt' timeout ".($solution->problem->limit_time*10)." java Main;",
					$dir_to
				);
			}else{
                $execute_result = $this->execute_shell(
//					"cd ".$dir_to."; ulimit -v ".(1024*$solution->problem->limit_memory)."; time sudo time -v -o './log_time.txt' timeout ".$solution->problem->limit_time." ./1;",
					"cd ".$dir_to."; time sudo time -v -o './log_time.txt' timeout ".$solution->problem->limit_time." ./1;",
					$dir_to
				);
			}

            if ($execute_result[0] == 'time'){
                $tests_text[$i] = array('time', $execute_result[1]);
            }elseif($execute_result[0] == 'memory'){
				$tests_text[$i] = array('memory', $execute_result[1]);
            }elseif(!file_exists($dir_to.'/output.txt')){
                $tests_text[$i] = array('outputDoesntExist', $execute_result[1]);
            }else{
                $file1 = file($dir_to.'/output.txt');
                $handle = opendir($dir_output.'/pre_'.$i);
                while ($file = readdir($handle)) {
                    if (($file!=".") && ($file!="..") && !$tests[$i]) {
                        $file2 = file($dir_output.'/pre_'.$i.'/'.$file);

                        if ($solution->problem->trim_lines) {
                            $file1 = array_map('trim', $file1);
                            $file2 = array_map('trim', $file2);
                        }

                        if ($solution->problem->empty_last_line) {
                            if (count($file1) && ($file1[count($file1) - 1] == '' || $file1[count($file1) - 1] == PHP_EOL)) {
                                $file1 = array_pop($file1);
                            }

                            if ($file2[count($file2) - 1] == '' || $file2[count($file2) - 1] == PHP_EOL) {
                                $file2 = array_pop($file2);
                            }
                        }

                        $diff = array_diff($file1, $file2);
                        if (empty($diff)) {
                            $tests[$i] = true;
							$result += 10;
                            $tests_text[$i] = array('ok', $execute_result[1]);
                        }
                    }
                }

				if (!$tests[$i]){
                    $tests_text[$i] = array('wrongAnswer', $execute_result[1]);
                }
            }

            @unlink($dir_to."/input.txt");
            @unlink($dir_to."/output.txt");
            @unlink($dir_to."/log_time.txt");
        }

        if (array_search(false, $tests)){
            Solution::model()->updateByPk($solution->id, array('status' => self::STATUS_CONTEST_FAIL_PRE_TESTING, 'tests' => json_encode($tests_text)));
        }else{
            Solution::model()->updateByPk($solution->id, array('status' => self::STATUS_CONTEST_ACCEPT_PRE_TESTING, 'tests' => json_encode($tests_text)));
        }

		$this->clearDir();
    }

    /**
     * @param $solution
     * @throws Exception
     */
    private function contest_test($solution){
        Solution::model()->updateByPk($solution->id, array('status' => self::STATUS_CONTEST_TESTING));
        $tests      = $tests_text = array();
        $result     = 0;
        $dir_input  = __DIR__ . '/../../storage/tests/'.$solution->p_id.'/input';
        $dir_output = __DIR__ . '/../../storage/tests/'.$solution->p_id.'/output';
        $dir_to     = __DIR__ . '/../../testing';

        for ($i=1; $i<=10; $i++){
            $tests[$i] = false;

            copy($dir_input."/".$i.'.txt', $dir_to."/input.txt");

            if ($solution->compiler == 'Java'){
                $execute_result = $this->execute_shell(
//					"cd ".$dir_to."; ulimit -v ".(1024*$solution->problem->limit_memory*100)."; time sudo time -v -o './log_time.txt' timeout ".($solution->problem->limit_time*10)." java Main;",
                    "cd ".$dir_to."; time sudo time -v -o './log_time.txt' timeout ".($solution->problem->limit_time*10)." java Main;",
                    $dir_to
                );
            }else{
                $execute_result = $this->execute_shell(
//					"cd ".$dir_to."; ulimit -v ".(1024*$solution->problem->limit_memory)."; time sudo time -v -o './log_time.txt' timeout ".$solution->problem->limit_time." ./1;",
                    "cd ".$dir_to."; time sudo time -v -o './log_time.txt' timeout ".$solution->problem->limit_time." ./1;",
                    $dir_to
                );
            }

            if ($execute_result[0] == 'time'){
                $tests_text[$i] = array('time', $execute_result[1]);
            }elseif($execute_result[0] == 'memory'){
                $tests_text[$i] = array('memory', $execute_result[1]);
            }elseif(!file_exists($dir_to.'/output.txt')){
                $tests_text[$i] = array('outputDoesntExist', $execute_result[1]);
            }else{
                $file1 = file($dir_to.'/output.txt');
                $handle = opendir($dir_output . DIRECTORY_SEPARATOR . $i);
                while ($file = readdir($handle)) {
                    if (($file!=".") && ($file!="..") && !$tests[$i]) {
                        $file2 = file($dir_output . DIRECTORY_SEPARATOR . $i . DIRECTORY_SEPARATOR . $file);

                        if ($solution->problem->trim_lines) {
                            $file1 = array_map('trim', $file1);
                            $file2 = array_map('trim', $file2);
                        }

                        if ($solution->problem->empty_last_line) {
                            if (count($file1) && ($file1[count($file1) - 1] == '' || $file1[count($file1) - 1] == PHP_EOL)) {
                                $file1 = array_pop($file1);
                            }

                            if ($file2[count($file2) - 1] == '' || $file2[count($file2) - 1] == PHP_EOL) {
                                $file2 = array_pop($file2);
                            }
                        }

                        $diff = array_diff($file1, $file2);
                        if (empty($diff)) {
                            $tests[$i] = true;
                            $result += 10;
                            $tests_text[$i] = array('ok', $execute_result[1]);
                        }
                    }
                }

                if (!$tests[$i]){
                    $tests_text[$i] = array('wrongAnswer', $execute_result[1]);
                }
            }

            @unlink($dir_to."/input.txt");
            @unlink($dir_to."/output.txt");
            @unlink($dir_to."/log_time.txt");
        }
        Solution::model()->updateByPk($solution->id, array('status' => self::STATUS_CONTEST_FINISH, 'tests' => json_encode($tests_text), 'result' => $result));
        $this->clearDir();
    }

    /**
     * @param $cmd
     * @param $adr_to
     * @return array
     * @throws Exception
     */
    private function execute_shell($cmd, $adr_to){
        ## Connect
        $connection = ssh2_connect('127.0.0.1', 22);
        if (!ssh2_auth_password($connection, Yii::app()->params['testing_ssh_login'], Yii::app()->params['testing_ssh_password'])) {
            throw new Exception('SSH authorization fault');
        }

        $stream = ssh2_exec($connection, $cmd);

        $errorStream = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
        stream_set_blocking($errorStream, true);
        stream_set_blocking($stream, true);

        $output = stream_get_contents($stream);
        $error = stream_get_contents($errorStream);

        preg_match_all('/0m(.*)s/i', $error, $arr);

        fclose($errorStream);
        fclose($stream);

        if (!file_exists($adr_to.'/log_time.txt')) {
            throw new Exception($cmd);
        }

        $log_time = file_get_contents($adr_to.'/log_time.txt');

        if (substr_count($log_time, 'status 124')){
            return array('time', $arr[1][0]);
        }elseif (substr_count($log_time, 'status 137')){
            return array('memory', $arr[1][0]);
        }else{
            return array('ok', $arr[1][0]);
        }
    }

    /**
     * @param $compiler
     * @return string
     */
	private function fileName($compiler){
		switch($compiler){
			case "FPC":
				return "1.pas";
				break;
			case "GCC":
				return "1.c";
				break;
			case "G++":
				return "1.cpp";
				break;				
			case "Prolog":
				return "1.pl";
				break;	
			case "Java":
				return "Main.java";
				break;		
		}
	}

	private function clearDir(){
		if($handle = opendir(__DIR__ . '/../../testing/')) {
			while (false !== ($file = readdir($handle))) {
				if($file != "." && $file != "..") {
                    unlink(__DIR__ . '/../../testing/'.$file);
                }
            }
            closedir($handle);
        }
    }
}