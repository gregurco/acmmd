<?php
/**
 * @author ElisDN <mail@elisdn.ru>
 * @link http://www.elisdn.ru
 */
class DMultilangHelper
{
    /**
     * @return bool
     */
    public static function enabled()
    {
        return count(Yii::app()->params['translatedLanguages']) > 1;
    }

    /**
     * @return array
     */
    public static function suffixList()
    {
        $list = array();

        foreach (Yii::app()->params['translatedLanguages'] as $lang => $name)
        {
            $suffix = '_' . $lang;
            $list[$suffix] = $name;
        }

        return $list;
    }

    /**
     * @param string $url
     * @return string
     */
    public static function processLangInUrl($url)
    {
        if (self::enabled())
        {

            self::selectLanguage(Yii::app()->getUser()->getLanguage());

            $domains = explode('/', ltrim($url, '/'));

            $isLangExists = in_array($domains[0], array_keys(Yii::app()->params['translatedLanguages']));

            if ($isLangExists)
            {
                $lang = array_shift($domains);
                Yii::app()->setLanguage($lang);

                self::selectLanguage($lang);

                $url = '/' . implode('/', $domains);
                CController::redirect($url);
            }

            Yii::app()->sourceLanguage = Yii::app()->getUser()->getLanguage();


            $url = '/' . implode('/', $domains);
        }

        return $url;
    }

    /**
     * @param string $lang
     */
    public static function selectLanguage($lang){
        Yii::app()->language = Yii::app()->session['language'] = $lang;
    }
}