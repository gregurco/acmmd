<?php

/**
 * Manager for Article
 *
 */
class MArticle extends CComponent {

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Article the loaded model
     * @throws CHttpException
     */
    static function loadPublicModel($id)
    {
        $article = Article::model()->findByAttributes(array('id' => $id, 'hide' => 0));
        if ($article === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $article;
    }

    static function getPublicCollection() {
        return GroupArticle::model()->findAllByAttributes(array('hide' => 0));
    }
}