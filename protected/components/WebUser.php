<?php

/**
 * Class WebUser
 *
 * @property User $_model
 */
class WebUser extends CWebUser{
    private $_model;

    public function loadUser()
    {
        if($this->_model === NULL) {
            if(!Yii::app()->user->isGuest) {
                $this->_model = User::model()->findByPk(Yii::app()->user->getId());
                User::model()->updateBypk($this->_model->id, array('time_last_active' => time()));
            }
        }

        return $this->_model;
    }

    /**
     * @return bool
     */
    public function userIsAdmin(){
        return !Yii::app()->user->isGuest && $this->loadUser()->admin;
    }

    /**
     * @return mixed|null
     */
    public function getLanguage(){
        if(isset(Yii::app()->session['language']) and in_array(Yii::app()->session['language'],array('ru','ro'))){
            return Yii::app()->session['language'];
        }else{
            return Yii::app()->params['defaultLanguage'];
        }
    }
}