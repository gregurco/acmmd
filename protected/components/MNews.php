<?php

/**
 * Manager for News
 *
 */
class MNews extends CComponent {

    const NEWS_PET_PAGE = 10;

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return News the loaded model
     * @throws CHttpException
     */
    static function loadPublicModel($id)
    {
        $news = News::model()->findByAttributes(array('id' => $id, 'hide' => 0));
        if ($news===null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $news;
    }

    /**
     * @return CActiveDataProvider
     */
    static function getPublicCollection() {
        return new CActiveDataProvider('News', array(
            'criteria' => array(
                'condition' => '`hide` = 0',
                'order'     => '`created` DESC',
            ),
            'pagination' => array(
                'pageSize' => self::NEWS_PET_PAGE,
            )
        ));
    }

    static function getPublicComments($news) {
        return NewsComment::model()->findAllByAttributes(array('n_id' => $news->id, 'hide' => 0));
    }

    /**
     * @param $newsComment NewsComment
     * @param $form array
     * @param $news News
     *
     * @return NewsComment
     */
    static function addComment($newsComment, $form, $news) {
        if (Yii::app()->config->get('COMMENT.ALLOW')) {
            if (Yii::app()->user->isGuest) {
                $newsComment->name = $form['name'];
            }
            $newsComment->text = $form['text'];
            $newsComment->verifyCode = $form['verifyCode'];
            $newsComment->n_id = $news->id;

            if ($newsComment->validate() && $newsComment->save()) {
                $newsComment->unsetAttributes();
                if (!Yii::app()->config->get('COMMENT.VALIDATE.ALLOW')) {
                    Yii::app()->user->setFlash('addComment', Yii::t('interface', 'Комментарий будет опубликован после проверки модератором.'));
                } else {
                    Yii::app()->user->setFlash('addComment', Yii::t('interface', 'Комментарий был успешно добавлен.'));
                }
            }

            $newsComment->verifyCode = '';
        }

        return $newsComment;
    }

    /**
     * @return array
     */
    public static function getNewsTypes(){
        return array(
            'default' => 'Default',
            'primary' => 'Primary (dark blue)',
            'success' => 'Success (green)',
            'info'    => 'Info (blue)',
            'warning' => 'Warning (yellow)',
            'danger'  => 'Danger (red)'
        );
    }
}