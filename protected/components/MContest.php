<?php

class MContest extends CComponent {

    /**
     * @param $id
     * @return array|mixed|null|static
     * @throws CHttpException
     */
    static function loadPublicContest($id)
    {
        $contest = Contest::model()->findByPk($id);
        if ($contest === null || !self::currentUserHasAccess($contest)) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $contest;
    }

    /**
     * @param $id
     * @return array|mixed|null|static
     * @throws CHttpException
     */
    static function loadPublicContestProblem($id)
    {
        $contest_problem = ContestProblem::model()->findByPk($id);
        if ($contest_problem === null || !self::currentUserHasAccess($contest_problem->contest)) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $contest_problem;
    }

    /**
     * @param $contest_problem
     * @return array|mixed|null|static
     * @throws CHttpException
     */
    static function loadPublicContestUserByContestProblem($contest_problem)
    {
        $contest_user = ContestUser::model()->findByAttributes(array(
            'contest_id' => $contest_problem->contest_id,
            'user_id'    => Yii::app()->getUser()->getId(),
            'grade'      => $contest_problem->grade
        ));

        if ($contest_user === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        return $contest_user;
    }

    /**
     * @param ContestProblem[] $contest_problems
     * @return int
     */
    static function countAcceptedProblems(array $contest_problems) {
        $accepted_problems = 0;

        foreach ($contest_problems as $contest_problem) {
            if (
                $contest_problem->getLastSolution() &&
                $contest_problem->getLastSolution()->status == Solution::STATUS_CONTEST_ACCEPT_PRE_TESTING
            ){
                $accepted_problems++;
            }
        }

        return $accepted_problems;
    }

    /**
     * @return string
     */
    static function getSendForbiddenMessage() {
        if (!Yii::app()->config->get('SEND.SOLUTION')) {
            return Yii::t('interface', 'Отправка решений временно недоступна.');
        } elseif (Yii::app()->user->isGuest) {
            return Yii::t('interface', 'Для отправки решения, вы должны авторизироваться.');
        }
        return '';
    }

    /**
     * @param Contest $contest
     * @return bool
     */
    static function currentUserHasAccess(Contest $contest){
        $now = new DateTime('now');
        $start_time = new DateTime($contest->start_time);
        $end_time = new DateTime($contest->end_time);

        return
            $start_time <= $now &&
            $end_time > $now &&
            $contest->hide == 0 &&
            (bool)ContestUser::model()->countByAttributes(array('contest_id' => $contest->id, 'user_id' => Yii::app()->getUser()->getId()));
    }

    /**
     * @param $end_time
     * @param string $format
     * @return string
     */
    static function getRestDuration($end_time, $format = '%H:%I:%S'){
        $now = new DateTime('now');
        $end_time = new DateTime($end_time);

        return $now->diff($end_time)->format($format);
    }
}