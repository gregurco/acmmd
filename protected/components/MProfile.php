<?php

class MProfile extends CComponent {

    static function getLinkedProblems($user_id = null) {
        if (!$user_id) {
            $user_id = Yii::app()->user->id;
        }

        $finishedProblem    = Solution::finishedProblem($user_id, false);
        $unfinishedProblem  = Solution::unFinishedProblem($user_id, false);

        $problem_arr = array();
        foreach (MProblem::getAvailable() as $problem) {
            if (in_array($problem->id, $finishedProblem)) {
                $problem_arr[] = CHtml::link(CHtml::tag('span', array('class' => 'label label-success'), $problem->id), array('problem/view', 'id' => $problem->id), array('title' => $problem->getName()));
            } elseif (in_array($problem->id, $unfinishedProblem)) {
                $problem_arr[] = CHtml::link(CHtml::tag('span', array('class' => 'label label-warning'), $problem->id), array('problem/view', 'id' => $problem->id), array('title' => $problem->getName()));
            } else {
                $problem_arr[] = CHtml::link(CHtml::tag('span', array('class' => 'label label-default'), $problem->id), array('problem/view', 'id' => $problem->id), array('title' => $problem->getName()));
            }
        }

        return $problem_arr;
    }
}