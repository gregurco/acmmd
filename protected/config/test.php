<?php

return CMap::mergeArray(
    array(
        'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
        'name'=>'algoRITMica',
        'timeZone' => 'Europe/Chisinau',
        // preloading 'log' component
        'preload'=>array('log', 'config', 'booster'),

        // autoloading model and component classes
        'import'=>array(
            'application.models.*',
            'application.components.*',
        ),

        'modules'=>array(
            'admin',
        ),

        // application components
        'components'=>array(
            'db'=>array(
                'class'=>'system.db.CDbConnection',
                'emulatePrepare' => true,
                'charset' => 'utf8',
                'tablePrefix' => 'a_'
            ),
            'request'=>array(
                'class'=>'EHttpRequest',
            ),
            'user'=>array(
                // enable cookie-based authentication
                'allowAutoLogin'=>true,
                'class' => 'WebUser',
                'loginUrl' => '/profile/login',
            ),
            // uncomment the following to enable URLs in path-format
            'urlManager'=>array(
                'urlFormat'=>'path',
                'rules'=>array(
                    '<language:(ru|ro)>/' => 'site/index',
                    '<language:(ru|ro)>/<action:(contact|login|logout)>' => 'site/<action>',
                    '<language:(ru|ro)>/<controller:\w+>/<id:\d+>'=>'<controller>/view',
                    '<language:(ru|ro)>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                    '<language:(ru|ro)>/<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                )
            ),
            'errorHandler'=>array(
                // use 'site/error' action to display errors
                'errorAction'=>'site/error',
            ),
            'log'=>array(
                'class'=>'CLogRouter',
                'routes'=>array(
                    array(
                        'class'=>'CFileLogRoute',
                        'levels'=>'error, warning',
                    ),
                ),
            ),
            'clientScript' => array(
                'packages'=>array(
                    'main_js' => array(
                        'baseUrl'=>'js/',
                        'js'=>array('functions.js'),
                    ),
                    'main_css' => array(
                        'baseUrl'=>'css/',
                        'css'=>array('menu.css', 'main.css', 'form.css', 'templatemo_style.css'),
                    ),
                ),
            ),
            'file'=>array(
                'class'=>'application.vendor.idlesign.ist-yii-cfile.CFile',
            ),
            'config'=>array(
                'class' => 'DConfig',
                'cache'=>3600,
            ),
            'messages' => array(
                'forceTranslation' => true
            ),
            'booster' => array(
                'class' => 'application.vendor.clevertech.yii-booster.src.components.Booster',
            ),
            'yiiMandrill' => array(
                'class'     => 'application.vendor.gregurco.yii-mandrill.YiiMandrill',
                'apiKey'    => '_b_17Uy2pMf5To-RK8ppbg',
                'fromEmail' => 'admin@acm-md.info',
                'fromName'  => 'Admin AcmMd',
            ),
            'syntaxhighlighter' => array(
                'class' => 'ext.JMSyntaxHighlighter.JMSyntaxHighlighter'
            ),
            'fixture'=>array(
                'class'=>'system.test.CDbFixtureManager',
            ),
        ),

        // application-level parameters that can be accessed
        // using Yii::app()->params['paramName']
        'params'=>array(
            // this is used in contact page
            'adminEmail'=>'',
            'translatedLanguages'=>array(
                'ru'=>'Russian',
                'ro'=>'Romanian',
            ),
            'defaultLanguage'=>'ru',
        ),

        'aliases' => array(
            'xupload' => 'application.vendor.asgaroth.xupload'
        ),
    ),

    require_once(dirname(__FILE__).'/config-test.php')
);