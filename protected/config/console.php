<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return CMap::mergeArray(
    array(
        'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
        'name'=>'My Console Application',

        // preloading 'log' component
        'preload'=>array('log'),
        'import'=>array(
            'application.models.*',
            'application.components.*',
        ),

        // application components
        'components'=>array(
            'db'=>array(
                'class'=>'system.db.CDbConnection',
                'emulatePrepare' => true,
                'charset' => 'utf8',
                'tablePrefix' => 'a_'
            ),
            'log'=>array(
                'class'=>'CLogRouter',
                'routes'=>array(
                    array(
                        'class'=>'CFileLogRoute',
                        'levels'=>'error, warning',
                    ),
                ),
            ),
        ),
        'commandMap' => array(
            'cron' => array(
                'class' => 'application.components.CronCommand',
                'dbConnection' => array('db', 'db2'),
            ),
            'migrate'=>array(
                'class'=>'system.cli.commands.MigrateCommand',
                'migrationTable'=>'yii_migration'
            ),
        ),
    ),

    require_once(dirname(__FILE__).'/config.php')
);