<?php
/**
 * @var $this ContestController
 * @var $model Contest
 */
?>

<h1 class="text-center"><?php echo Yii::t('interface', "Список олимпиад");?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'problem-grid',
    'dataProvider'=> $model->searchFrontend(),
    'filter'=>$model,
    'enableSorting'=>false,
    'columns'=>array(
        array(
            'header' => '№',
            'value'  => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + $row + 1',
            'htmlOptions'=>array('style'=>'width: 20px;'),
        ),
        array(
            'name' => 'title',
            'value' => '$data->title',
            'header' => Yii::t('interface','Название')
        ),
        array(
            'name' => 'start_time',
            'value' => '$data->start_time',
            'header' => Yii::t('interface','Начало')
        ),
        array(
            'name' => 'end_time',
            'value' => '$data->end_time',
            'header' => Yii::t('interface','Конец')
        ),
        array(
            'type'=>'html',
            'value' => 'MContest::currentUserHasAccess($data) ? CHtml::link("'.Yii::t('interface','Просмотреть').'", array("view", "id" => $data->id)) : ""',
            'header' => Yii::t('interface','Действие')
        ),
    ),
    'itemsCssClass' => 'table table-hover table-bordered',
    'pagerCssClass' => 'text-center',
    'summaryText' => '',
    'pager'=>array(
        'htmlOptions' => array(
            'class' => 'pagination'
        ),
        'header'=>'',
        'maxButtonCount'=>25,
        'selectedPageCssClass'=>'active',
        'hiddenPageCssClass'=>'disabled',
        'firstPageLabel'=>'&lt;&lt;',
        'lastPageLabel'=>'&gt;&gt;',
        'prevPageLabel'=>false,
        'nextPageLabel'=>false,
    ),
)); ?>
