<?php
/**
 * @var $this ContestController
 * @var $contest_problem ContestProblem
 */

$this->menu=array(
    array('label'=>Yii::t('interface', 'Начальная страница'), 'url'=>array('contest/view', 'id' => $contest_problem->contest_id)),
);
?>

<h1 class="text-center">"<?php echo $contest_problem->problem->name; ?>"</h1>

<h3><?php echo Yii::t('interface', 'Описание');?>:</h3>
<?php echo $contest_problem->problem->description; ?>

<br/><h3 style="display: inline;"><?php echo Yii::t('interface', 'Лимит времени');?>:</h3>
<?php echo $contest_problem->problem->limit_time . Yii::t('interface',' c.'); ?>

<br/><br/><h3 style="display: inline;"><?php echo Yii::t('interface', 'Лимит памяти');?>:</h3>
<?php echo $contest_problem->problem->limit_memory . Yii::t('interface',' мб.'); ?>

<br/><br/><h3><?php echo Yii::t('interface', 'Входные данные');?>:</h3>
<?php echo $contest_problem->problem->input; ?>

<h3><?php echo Yii::t('interface', 'Выходные данные');?>:</h3>
<?php echo $contest_problem->problem->output; ?>

<?php if (!empty($contest_problem->problem->examples)): ?>
    <table class="problem_example">
        <tr>
            <th style="width: 1%;">№</th>
            <th>input.txt</th>
            <th>output.txt</th>
        </tr>
        <?php foreach($contest_problem->problem->examples as $key => $value): ?>
            <tr>
                <td><?php echo $key+1 ;?></td>
                <td><?php echo $value['input']; ?></td>
                <td><?php echo $value['output']; ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>

<?php if(!empty($sendForbidden)): ?>
    <div class="flash-error">
        <?php echo $sendForbidden; ?>
    </div>
<?php else: ?>
    <div class="row mr-top-25">
        <div class="col-md-2"></div>
        <div class="col-md-8" id="solution_div">
            <?php $form=$this->beginWidget('CActiveForm',array(
                'htmlOptions'=>array('class' => 'form-horizontal well', 'enctype'=>'multipart/form-data'),
            )); ?>
            <div class="row">
                <h3 class="col-sm-12 text-center"><?php echo Yii::t('interface','Отправка решения');?></h3>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($modelForm, 'upload_mode', array('class' => 'col-sm-3 control-label'));?>
                <div class="col-sm-9">
                    <?php echo $form->dropDownList($modelForm, 'upload_mode', array('file' => Yii::t('interface','Файл'), 'text' => Yii::t('interface','Текст')), array('class' => 'form-control')); ?>
                    <?php echo $form->error($modelForm, 'upload_mode'); ?>
                </div>
            </div>
            <div class="form-group" id="file_solution_div">
                <?php echo $form->labelEx($modelForm, 'file_solution', array('class' => 'col-sm-3 control-label'));?>
                <div class="col-sm-9">
                    <?php echo $form->fileField($modelForm, 'file_solution', array('class' => 'form-control')); ?>
                    <?php echo $form->error($modelForm, 'file_solution'); ?>
                </div>
            </div>
            <div class="form-group" style="display: none;" id="text_solution_div">
                <?php echo $form->labelEx($modelForm, 'text_solution', array('class' => 'col-sm-3 control-label'));?>
                <div class="col-sm-9">
                    <?php echo $form->textArea($modelForm, 'text_solution', array('class' => 'form-control', 'style' => 'height: 350px')); ?>
                    <?php echo $form->error($modelForm, 'text_solution'); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($modelForm, 'compiler', array('class' => 'col-sm-3 control-label'));?>
                <div class="col-sm-9">
                    <?php echo $form->dropDownList($modelForm, 'compiler', Solution::listOfCompilers(), array('class' => 'form-control')); ?>
                    <?php echo $form->error($modelForm, 'compiler'); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <?php echo CHtml::submitButton(Yii::t('interface','Отправить'), array('class' => 'btn btn-default')); ?>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
        <div class="col-md-2"></div>
    </div>
<?php endif; ?>