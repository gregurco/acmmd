<?php
/**
 * @var $this ContestController
 * @var $contest Contest
 * @var $contest_user ContestUser
 * @var $contest_problems ContestProblem[]
 * @var $accepted_problems int
 */
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="text-center"><?php echo $contest->getTitle();?></h1>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><?php echo Yii::t('interface', "Описание");?></h3></div>
            <div class="panel-body">
                <?php echo $contest->getDescription();?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><?php echo Yii::t('interface', "Статистика");?></h3></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <p><?php echo Yii::t('interface', "Класс");?>: <?php echo $contest_user->grade;?></p>
                        <p class="mr-top-10"><?php echo Yii::t('interface', "Начало");?>: <?php echo $contest->start_time;?></p>
                        <p><?php echo Yii::t('interface', "Конец");?>: <?php echo $contest->end_time;?></p>
                        <p><?php echo Yii::t('interface', "Осталось");?>: <?php echo MContest::getRestDuration($contest->end_time); ?></p>
                    </div>
                    <div class="col-md-6">
                        <p><?php echo Yii::t('interface', "Всего задач");?>: <?php echo count($contest_problems);?></p>
                        <p><?php echo Yii::t('interface', "Принято задач");?>: <?php echo $accepted_problems;?></p>
                        <p><?php echo Yii::t('interface', "Осталось решить");?>: <?php echo count($contest_problems) - 0;?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title"><?php echo Yii::t('interface', "Задачи");?></h3></div>

            <ul class="list-group">
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-md-4"><strong><?php echo Yii::t('interface', "Название");?></strong></div>
                        <div class="col-md-4"><strong><?php echo Yii::t('interface', "Статус");?></strong></div>
                        <div class="col-md-4"><strong><?php echo Yii::t('interface', "Действие");?></strong></div>
                    </div>
                </li>
                <?php foreach ($contest_problems as $contest_problem): ?>
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-4"><?php echo $contest_problem->problem->getName(); ?></div>
                            <div class="col-md-4">
                                <?php echo is_object($contest_problem->getLastSolution())? '<a href="'.$this->createUrl('solution/download', array('id' => $contest_problem->getLastSolution()->id)).'">' . $contest_problem->getLastSolution()->getStatusName() .'</a>' : '-' ?>
                            </div>
                            <div class="col-md-4"><?php echo CHtml::link(Yii::t('interface','Просмотреть'), array("viewProblem", "id" => $contest_problem->id));?></div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
