<?php
/* @var $this ProblemController */
/* @var $model Problem */

$this->menu=array(
	array('label'=>Yii::t('interface', "Список задач"), 'url'=>array('problem/index')),
    array('label'=>Yii::t('interface', "Отправленные решения"), 'url'=>array('solution/index'),'visible'=>!Yii::app()->user->isGuest),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#problem-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1 class="text-center"><?php echo Yii::t('interface', "Список задач");?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'problem-grid',
	'dataProvider'=>!Yii::app()->user->isGuest?$model->searchForUser():$model->search(),
	'filter'=>$model,
    'enableSorting'=>false,
	'columns'=>array(
        array(
            'header' => '№',
            'value'  => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + $row + 1',
            'htmlOptions'=>array('style'=>'width: 20px;'),
        ),
        array(
            'name' => 'name_'.Yii::app()->getUser()->getLanguage(),
            'value' => '$data->name_'.Yii::app()->getUser()->getLanguage(),
            'header' => Yii::t('interface','Название')
        ),
        array(
            'value'=>'!empty($data->solution)?MProblem::getStatus($data->solution[0]["max_result"],$data->tests*10):""',
            'header' => Yii::t('interface','Статус'),
            'visible'=>!Yii::app()->user->isGuest,
        ),
        array(
            'type'=>'html',
            'value' => 'CHtml::link("'.Yii::t('interface','Просмотреть').'", array("view", "id" => $data->id))',
            'header' => Yii::t('interface','Действие')
        ),
	),
    'itemsCssClass' => 'table table-hover table-bordered',
    'pagerCssClass' => 'text-center',
    'summaryText' => '',
    'pager'=>array(
        'htmlOptions' => array(
            'class' => 'pagination'
        ),
        'header'=>'',
        'maxButtonCount'=>25,
        'selectedPageCssClass'=>'active',
        'hiddenPageCssClass'=>'disabled',
        'firstPageLabel'=>'&lt;&lt;',
        'lastPageLabel'=>'&gt;&gt;',
        'prevPageLabel'=>false,
        'nextPageLabel'=>false,
    ),
)); ?>
