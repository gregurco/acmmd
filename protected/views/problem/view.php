<?php
/* @var $this ProblemController */
/* @var $model Problem */
?>

<div class="row">
    <div class="col-md-5">
        <div style="padding-top: 12px;">
            <a href="<?php echo $this->createUrl('problem/index'); ?>"><button class="btn btn-default">
                <i class="fa fa-chevron-left"></i> <?php echo Yii::t('interface', 'Назад'); ?>
            </button></a>

            <?php if (!Yii::app()->user->isGuest): ?>
                <a href="<?php echo $this->createUrl('solution/index', array('pid' => $model->id)); ?>"><button class="btn btn-default">
                    <i class="fa fa-file-text-o"></i> <?php echo Yii::t('interface', 'Отправленные решения'); ?>
                </button></a>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-md-2 text-center">
        <h1>"<?php echo $model->name; ?>"</h1>
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo Yii::t('interface', 'Описание');?></h3>
    </div>
    <div class="panel-body">
        <?php echo $model->description; ?>
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo Yii::t('interface', 'Ограничения');?></h3>
    </div>
    <div class="panel-body">
        <p><?php echo Yii::t('interface', 'Лимит времени');?>: <?php echo $model->limit_time . Yii::t('interface',' c.'); ?></p>
        <p><?php echo Yii::t('interface', 'Лимит памяти');?>: <?php echo $model->limit_memory . Yii::t('interface',' мб.'); ?></p>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo Yii::t('interface', 'Входные данные');?></h3>
    </div>
    <div class="panel-body">
        <?php echo $model->input; ?>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo Yii::t('interface', 'Выходные данные');?></h3>
    </div>
    <div class="panel-body">
        <?php echo $model->output; ?>
    </div>
</div>

<?php if (!empty($model->examples)): ?>
    <table class="table table-hover table-bordered">
        <tr>
            <th style="width: 1%;">№</th>
            <th>input.txt</th>
            <th>output.txt</th>
        </tr>
        <?php foreach($model->examples as $key => $value): ?>
            <tr>
                <td><?php echo $key+1 ;?></td>
                <td><?php echo $value['input']; ?></td>
                <td><?php echo $value['output']; ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>

<?php if (!empty($sendForbidden)): ?>
    <div class="flash-error mr-top-25">
        <?php echo $sendForbidden; ?>
    </div>
<?php else: ?>
    <div class="row mr-top-25">
        <div class="col-md-2"></div>
        <div class="col-md-8" id="solution_div">
            <?php $form=$this->beginWidget('CActiveForm',array(
                'htmlOptions'=>array('class' => 'form-horizontal well', 'enctype'=>'multipart/form-data'),
            )); ?>
            <div class="row">
                <h3 class="col-sm-12 text-center"><?php echo Yii::t('interface','Отправка решения');?></h3>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($modelForm, 'upload_mode', array('class' => 'col-sm-3 control-label'));?>
                <div class="col-sm-9">
                    <?php echo $form->dropDownList($modelForm, 'upload_mode', array('file' => Yii::t('interface','Файл'), 'text' => Yii::t('interface','Текст')), array('class' => 'form-control')); ?>
                    <?php echo $form->error($modelForm, 'upload_mode'); ?>
                </div>
            </div>
            <div class="form-group" id="file_solution_div">
                <?php echo $form->labelEx($modelForm, 'file_solution', array('class' => 'col-sm-3 control-label'));?>
                <div class="col-sm-9">
                    <?php echo $form->fileField($modelForm, 'file_solution', array('class' => 'form-control')); ?>
                    <?php echo $form->error($modelForm, 'file_solution'); ?>
                </div>
            </div>
            <div class="form-group" style="display: none;" id="text_solution_div">
                <?php echo $form->labelEx($modelForm, 'text_solution', array('class' => 'col-sm-3 control-label'));?>
                <div class="col-sm-9">
                    <?php echo $form->textArea($modelForm, 'text_solution', array('class' => 'form-control', 'style' => 'height: 350px')); ?>
                    <?php echo $form->error($modelForm, 'text_solution'); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($modelForm, 'compiler', array('class' => 'col-sm-3 control-label'));?>
                <div class="col-sm-9">
                    <?php echo $form->dropDownList($modelForm, 'compiler', Solution::listOfCompilers(), array('class' => 'form-control')); ?>
                    <?php echo $form->error($modelForm, 'compiler'); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <?php echo CHtml::submitButton(Yii::t('interface','Отправить'), array('class' => 'btn btn-default')); ?>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
        <div class="col-md-2"></div>
    </div>
<?php endif; ?>