<?php
/**
 * @var $this ProfileController
 * @var $model User
 */
?>
<h1 class="text-center"><?php echo Yii::t('interface', 'Восстановление пароля');?></h1>

<div class="col-md-3"></div>
<div class="col-md-6">
    <?php if(Yii::app()->user->hasFlash('reminder')): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo Yii::t('interface', 'Закрыть');?></span></button>
            <?php echo Yii::app()->user->getFlash('reminder'); ?>
        </div>
    <?php endif; ?>

    <?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'htmlOptions' => array('class' => 'well'), // for inset effect
        )
    );
    ?>

    <?php echo $form->textFieldGroup($model,'email'); ?>


    <div class="form-group">
        <?php
        $this->widget(
            'booster.widgets.TbButton',
            array('buttonType' => 'submit', 'label' => Yii::t('interface', 'Восстановить'))
        );
        ?>
    </div>
<?php $this->endWidget(); ?>
</div>
<div class="col-md-3"></div>