<?php
/**
 * @var $this ProfileController
 * @var $model User
 */
?>
<h1 class="text-center"><?php echo Yii::t('interface', 'Регистрация');?></h1>

<div class="col-md-3"></div>
<div class="col-md-6 text-center">
    <?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id'=>'login-form',
            'htmlOptions' => array('class' => 'well'), // for inset effect
        )
    );
    ?>

        <?php echo $form->textFieldGroup($model,'login'); ?>

        <?php echo $form->passwordFieldGroup($model,'password'); ?>

        <?php echo $form->passwordFieldGroup($model,'password_repeat'); ?>

        <?php echo $form->textFieldGroup($model,'email'); ?>

        <?php echo $form->textFieldGroup($model,'name'); ?>

        <?php echo $form->textFieldGroup($model,'surname'); ?>

        <?php if(CCaptcha::checkRequirements()): ?>
            <?php echo $form->textFieldGroup($model,'verifyCode'); ?>
            <div class="form-group">
                <?php $this->widget('CCaptcha'); ?>
            </div>
        <?php endif; ?>

        <div class="form-group" style="margin-top: 20px;">
            <?php
            $this->widget(
                'booster.widgets.TbButton',
                array('buttonType' => 'submit', 'label' => Yii::t('interface', 'Зарегистрироваться'))
            );
            ?>
        </div>

    <?php $this->endWidget(); ?>
</div>
<div class="col-md-3"></div>
