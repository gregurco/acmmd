<?php
/* @var $this SiteController */
$this->menu=array(
    array('label'=>Yii::t('interface','Личный кабинет'), 'url'=>array('profile/index')),
    array('label'=>Yii::t('interface','Изменить пароль'), 'url'=>array('profile/changePassword')),
    array('label'=>Yii::t('interface','Редактирование профиля'), 'url'=>array('profile/changeProfile')),
);
?>
<h1 class="text-center"><?php echo Yii::t('interface','Изменить пароль');?></h1>

<?php if(Yii::app()->user->hasFlash('password')): ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <?php echo Yii::app()->user->getFlash('password'); ?>
    </div>
<?php endif; ?>

<div class="col-md-2"></div>
<div class="col-md-8 text-center">
    <?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id'=>'login-form',
            'htmlOptions' => array('class' => 'well'), // for inset effect
        )
    );
    ?>

        <?php echo $form->passwordFieldGroup($model,'password'); ?>

        <?php echo $form->passwordFieldGroup($model,'password_repeat'); ?>

        <div class="form-group">
            <?php
            $this->widget(
                'booster.widgets.TbButton',
                array('buttonType' => 'submit', 'label' => Yii::t('interface','Сохранить'))
            );
            ?>
        </div>
    <?php $this->endWidget(); ?>
</div>
<div class="col-md-2"></div>
