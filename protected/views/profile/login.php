<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

?>
<h1 class="text-center"><?php echo Yii::t('interface', 'Авторизация');?></h1>

<div class="col-md-3"></div>
<div class="col-md-6 text-center">
    <?php if(Yii::app()->user->hasFlash('login')): ?>
        <div class="alert alert-success" role="alert">
            <?php echo Yii::app()->user->getFlash('login'); ?>
        </div>
    <?php endif; ?>

    <?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id'=>'login-form',
            'htmlOptions' => array('class' => 'well'), // for inset effect
        )
    );
    ?>

    <?php echo $form->textFieldGroup($model,'username', array('label' => Yii::t('interface', 'Логин'))); ?>

    <?php echo $form->passwordFieldGroup($model,'password', array('label' => Yii::t('interface', 'Пароль'))); ?>

    <?php echo $form->checkboxGroup($model,'rememberMe', array('label' => Yii::t('interface', 'Запомнить'))); ?>

    <?php echo CHtml::link(Yii::t('interface', 'Зарегистрироваться'),'register')?> <?php echo Yii::t('interface', 'или');?> <?php echo CHtml::link(Yii::t('interface', 'Восстановить пароль'),'reminder')?>

    <div class="form-group" style="margin-top: 20px;">
        <?php
        $this->widget(
            'booster.widgets.TbButton',
            array('buttonType' => 'submit', 'label' => Yii::t('interface', 'Войти'))
        );
        ?>
    </div>

    <?php $this->endWidget(); ?>
</div>
<div class="col-md-3"></div>