<?php
/**
 * @var $this ProfileController
 * @var $model User
 */
?>
<h1 class="text-center"><?php echo Yii::t('interface','Информация о пользователе');?>: <?php echo $model->login;?></h1>
<?php $this->widget('zii.widgets.CDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'name',
        'surname',
        array(
            'label' => Yii::t('interface','Время регистрации'),
            'value' => CHtml::encode(date("Y-m-d H:i:s",$model->time_register)),
        )
    ),
)); ?>