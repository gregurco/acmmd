<?php
/**
 * @var $this SiteController
 * @var $model User
 * @var $problem_arr array
 * @var $score int
 */
$this->menu=array(
    array('label'=>Yii::t('interface','Личный кабинет'), 'url'=>array('profile/index')),
    array('label'=>Yii::t('interface','Изменить пароль'), 'url'=>array('profile/changePassword')),
    array('label'=>Yii::t('interface','Редактирование профиля'), 'url'=>array('profile/changeProfile')),
);
?>

<h1 class="text-center"><?php echo Yii::t('interface', "Информация о пользователе");?> : <?php echo Yii::app()->user->name;?></h1>

<?php $this->widget('booster.widgets.TbDetailView', array(
    'data'       => $model,
    'attributes' => array(
        array('name' => 'name', 'label' => Yii::t('interface', 'Имя')),
        array('name' => 'surname', 'label' => Yii::t('interface', 'Фамилия')),
        array('name' => 'email', 'label' => Yii::t('interface', 'E-mail')),
        array('name' => 'time_register', 'label' => Yii::t('interface', 'Время регистрации'), 'value' => Yii::app()->dateFormatter->formatDateTime($model->time_register)),
        array('type' => 'html', 'label' => Yii::t('interface', 'Баллы'), 'value' => $score),
        array('type' => 'html', 'label' => Yii::t('interface', 'Задачи'), 'value' => implode(' ', $problem_arr), 'cssClass' => 'user-problems-list'),
    ),
)); ?>
