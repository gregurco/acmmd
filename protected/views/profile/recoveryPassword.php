<?php
/**
 * @var $this ProfileController
 * @var $model User
 */
?>
<h1 class="text-center"><?php echo Yii::t('interface', 'Восстановление пароля');?></h1>

<div class="col-md-3"></div>
<div class="col-md-6 text-center">
    <?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id'=>'login-form',
            'htmlOptions' => array('class' => 'well'), // for inset effect
        )
    );
    ?>

    <?php echo $form->passwordFieldGroup($model,'password'); ?>

    <?php echo $form->passwordFieldGroup($model,'password_repeat'); ?>

    <div class="form-group">
        <?php
        $this->widget(
            'booster.widgets.TbButton',
            array('buttonType' => 'submit', 'label' => 'Сохранить')
        );
        ?>
    </div>
    <?php $this->endWidget(); ?>
</div>
<div class="col-md-3"></div>