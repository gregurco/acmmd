Для пользователя <?php echo $modal->login;?> с сайта Acm-Md.info было запрошено восстановление пароля.

Ссылка для восстановления пароля: http://<?php echo $_SERVER['HTTP_HOST']?>/profile/recoveryPassword/token/<?php echo $modal->token;?>

Внимание! Если вы не запрашивали восстановление пароля, то не переходите по данной ссылке.