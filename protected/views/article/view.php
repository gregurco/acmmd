<?php
/**
 * @var $this ArticleController
 * @var $model Article
 */
?>

<div class="row">
    <div class="col-md-12">
        <div class="pull-left" style="padding-top: 12px;">
            <a href="<?php echo $this->createUrl('index'); ?>">
                <button class="btn btn-default">
                    <i class="fa fa-chevron-left"></i> <?php echo Yii::t('interface', 'Назад'); ?>
                </button>
            </a>
        </div>

        <h1 class="text-center"><?php echo Yii::t('interface',$model->title); ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php  echo Yii::t('interface',$model->text); ?>
    </div>
</div>