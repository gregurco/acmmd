<?php
/* @var GroupArticle $groupArticles */

$this->layout='//layouts/column1';
?>
<h1 class="text-center"><?php echo Yii::t('interface', 'Статьи');?></h1>

<?php foreach ($groupArticles as $groupArticle):?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $groupArticle->title;?></h3>
        </div>
        <div class="panel-body">
            <ul class="list-group">
                <?php foreach ($groupArticle->article as $article): ?>
                    <li class="list-group-item">
                        <span class="badge">0</span>
                        <?php echo CHtml::link($article->title, array("view", "id" => $article->id)); ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php endforeach; ?>