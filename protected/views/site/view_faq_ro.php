<h1 class="text-center">FAQ</h1>

<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                    Ce putem gasi pe acest site?
                </a>
            </h4>
        </div>
        <div id="collapse1" class="panel-collapse collapse in">
            <div class="panel-body">
                Acest site contine o arhiva de probleme de olimpiada cu sistemul de testare incorporat.
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                    Ce trebuie sa faci pentru a participa la acest sistem?
                </a>
            </h4>
        </div>
        <div id="collapse2" class="panel-collapse collapse">
            <div class="panel-body">
                Pentru participare in asect sistem este deajuns da te inregistra si a trece in sectiunea ’’Sarcini’’, unde veti putea gasi problem de diferita complexitate si pe diferite tematici. Dupa incarcarea  rezolvarii problemei, programul va trece anumit numar de teste (deobicei 10), pentru fiecare test veti putea primi 10 puncte, suma punctelor pentru toate problemele rezolvate vor forma pozitia dumnevoastra in reiting.
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                    Care este specificul problemelor si cum se le expediezi pe server?
                </a>
            </h4>
        </div>
        <div id="collapse3" class="panel-collapse collapse">
            <div class="panel-body">
                Toate problemele necesita lucrul cu fisierele <b>input.txt</b> si <b>output.txt</b>, destinate pentru citirea datelor initiale si extragerea rezultatului necesar. Expedierea rezolvarilor este posibila doar utilizatorilor inregistrati sub forma codului in fisier.
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                    In ce limbaje poti expedia rezolvarea?
                </a>
            </h4>
        </div>
        <div id="collapse4" class="panel-collapse collapse">
            <div class="panel-body">
                Sistema verificarii prelucreaza programele scriese in libajele urmatoare: <b>Pascal</b>, <b>C</b>, <b>C++</b>, <b>Prolog</b> и <b>Java</b>.
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                    Unde pot descarca compilator/mediu de programare?
                </a>
            </h4>
        </div>
        <div id="collapse5" class="panel-collapse collapse">
            <div class="panel-body">
                <ul>
                    <li>Pascal:
                        <ol>
                            <li><a href="http://sourceforge.net/projects/freepascal/files/Win32/2.6.4/fpc-2.6.4.x86_64-win64.exe/download" target="_blank">Free Pascal (Windows x64)</a></li>
                            <li><a href="http://sourceforge.net/projects/freepascal/files/Win32/2.6.4/fpc-2.6.4.i386-win32.exe/download" target="_blank">Free Pascal (Windows x32)</a></li>
                            <li><a href="http://www.bloodshed.net/dev/devpas192.exe" target="_blank">Dev-Pas 1.9</a></li>
                            <li><a href="https://code-live.ru/media/2010/08/22/turbo_pascal_7_1_tpx.exe" target="_blank">Turbo Pascal 7.1</a></li>
                            <li><a href="http://pascalabc.net/ssyilki-dlya-skachivaniya" target="_blank">ABC Pascal</a></li>
                        </ol>
                    </li>
                    <li>C/C++:
                        <ol>
                            <li><a href="http://prdownloads.sourceforge.net/dev-cpp/devcpp-4.9.9.2_setup.exe" target="_blank">Dev-C++ 5</a></li>
                            <li><a href="http://www.bloodshed.net/dev/devcpp401.zip" target="_blank">Dev-C++ 4</a></li>
                        </ol>
                    </li>
                    <li>Prolog:
                        <ol>
                            <li>-</li>
                        </ol>
                    </li>
                    <li>Java:
                        <ol>
                            <li>-</li>
                        </ol>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                    Ce se intampla dupa expedierea programului pe server?
                </a>
            </h4>
        </div>
        <div id="collapse6" class="panel-collapse collapse">
            <div class="panel-body">
                Fiecare program expediat se compileaza pe server, si in cazul compilarii reusite, el trece 10 teste. Ve-ti primi 10 puncte pentru test, daca: raspunsul dumnevoastra coincide cu raspunsul nostrum si se incadreaza in restrictiile de timp si memorie.
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
                    Cum de inteles ca problema e rezolvata corect?
                </a>
            </h4>
        </div>
        <div id="collapse7" class="panel-collapse collapse">
            <div class="panel-body">
                Daca programul s-a compilat reusit si ati acumulat 100 de puncte, atunci rezolvarea dumnevoastra este corecta.
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">
                    Cum se formeaza reitingul?
                </a>
            </h4>
        </div>
        <div id="collapse8" class="panel-collapse collapse">
            <div class="panel-body">
                Reitingul se formeaza din suma punctelor celor mai reusite rezolvari. Daca dumnevoastra ati expediat la o problema 2 rezolvari, si una acumuleaza 50 de puncte , iar alta 70 de puncte , atunci se ia in consideratie numai 70 de puncte.
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">
                    Ce trebuie de facut, daca nu gasiti aici raspuns la intrebarea dumnevoastra?
                </a>
            </h4>
        </div>
        <div id="collapse9" class="panel-collapse collapse">
            <div class="panel-body">
                Expediati intrebari pe adresa <?php echo Yii::app()->params->adminEmail;?>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">
                    Cum de inteles raspunsurile compilatorului?
                </a>
            </h4>
        </div>
        <div id="collapse10" class="panel-collapse collapse">
            <div class="panel-body">
                In arhiva rezolvarilor, vizavi de fiecare test, puteti observa urmatoarele mesaje:

                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th style="width: 10%;">Mesajul</th> <th style="width: 45%;">Evenimentul</th> <th style="width: 45%;">Motivul</th>
                        </tr>

                        <tr>
                            <td style="color: green">Accepted</td>
                            <td>Programul a trecut testele, raspunsul este corect si se incadreaza in limitele de timp si memorie.</td>
                            <td style="vertical-align:middle" class="text-center">-</td>
                        </tr>


                        <tr>
                            <td style="color: red">Compilation error</td>
                            <td>Programul a trecut compilare. Textul greselii il puteti urmari in arhiva problemei.</td>
                            <td>In cod a fost comisa o greseala sintactica.</td>
                        </tr>

                        <tr>
                            <td style="color: red">Wrong answer</td>
                            <td>Raspunsul dvs nu coincide cu raspunsurile administratiei.</td>
                            <td>In cod a fost comisa o greseala logica.</td>
                        </tr>

                        <tr>
                            <td style="color: red">Time limit</td>
                            <td>Timpul de executie a programului a depasit limita.</td>
                            <td>Trebuie sa modificati algoritmul sau sa-l optimizati.</td>
                        </tr>

                        <tr>
                            <td style="color: red">File doesn't exist</td>
                            <td style="vertical-align:top">Nu a fost gasit fisierul de iesire.</td>
                            <td>Verificati daca corect ati numit fisierul de iesire si daca el se creeaza in toate situatiile.</td>
                        </tr>

                        <tr>
                            <td>Waiting</td>
                            <td>Asteptare</td>
                            <td>Programul se afla in asteptare de compilare</td>
                        </tr>

                        <tr>
                            <td>Compiling</td>
                            <td>Compilarea programului</td>
                            <td>Este nevoie de timp pentru creearea fisierului de executie</td>
                        </tr>

                        <tr>
                            <td>Running</td>
                            <td>Testarea programului</td>
                            <td>Are loc testarea programei executind codul pentru fiecare test existent</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
