<h1 class="text-center">FAQ</h1>

<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                    Что мы можем найти на данном сайте?
                </a>
            </h4>
        </div>
        <div id="collapse1" class="panel-collapse collapse in">
            <div class="panel-body">
                Данный сайт содержит архив задач по олимпиадному программированию со встроенной проверяющей системой.
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                    Что нужно сделать, для участия в данной системе?
                </a>
            </h4>
        </div>
        <div id="collapse2" class="panel-collapse collapse">
            <div class="panel-body">
                Для участия в системе достаточно зарегистрироваться и перейти в раздел <b>"Задачи"</b>, где вы сможете найти задачи разной сложности и тематики. Сдав задачу, она пройдет заданное количество тестов (обычно 10), за каждый тест вы сможете получить по 10 баллов, сумма баллов за все задачи и будут формировать вашу позицию в рейтинге.
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                    Какова специфика задач и как их отправлять на сервер?
                </a>
            </h4>
        </div>
        <div id="collapse3" class="panel-collapse collapse">
            <div class="panel-body">
                Все задачи требуют работы с файлами <b>input.txt</b> и <b>output.txt</b>, предназначенными для чтения входных данных и вывода результата соответственно. Отправлять решения можно только зарегистрированным пользователям в виде исходного кода.
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                    На каких языках можно сдавать написанные задачи?
                </a>
            </h4>
        </div>
        <div id="collapse4" class="panel-collapse collapse">
            <div class="panel-body">
                Проверяющая система обрабатывает программы, реализованные на языках: <b>Pascal</b>, <b>C</b>, <b>C++</b>, <b>Prolog</b> и <b>Java</b>.
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                    Где я могу скачать компиляторы/среды разработки?
                </a>
            </h4>
        </div>
        <div id="collapse5" class="panel-collapse collapse">
            <div class="panel-body">
                <ul>
                    <li>Pascal:
                        <ol>
                            <li><a href="http://sourceforge.net/projects/freepascal/files/Win32/2.6.4/fpc-2.6.4.x86_64-win64.exe/download" target="_blank">Free Pascal (Windows x64)</a></li>
                            <li><a href="http://sourceforge.net/projects/freepascal/files/Win32/2.6.4/fpc-2.6.4.i386-win32.exe/download" target="_blank">Free Pascal (Windows x32)</a></li>
                            <li><a href="http://www.bloodshed.net/dev/devpas192.exe" target="_blank">Dev-Pas 1.9</a></li>
                            <li><a href="https://code-live.ru/media/2010/08/22/turbo_pascal_7_1_tpx.exe" target="_blank">Turbo Pascal 7.1</a></li>
                            <li><a href="http://pascalabc.net/ssyilki-dlya-skachivaniya" target="_blank">ABC Pascal</a></li>
                        </ol>
                    </li>
                    <li>C/C++:
                        <ol>
                            <li><a href="http://prdownloads.sourceforge.net/dev-cpp/devcpp-4.9.9.2_setup.exe" target="_blank">Dev-C++ 5</a></li>
                            <li><a href="http://www.bloodshed.net/dev/devcpp401.zip" target="_blank">Dev-C++ 4</a></li>
                        </ol>
                    </li>
                    <li>Prolog:
                        <ol>
                            <li>-</li>
                        </ol>
                    </li>
                    <li>Java:
                        <ol>
                            <li>-</li>
                        </ol>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                    Что происходит после отправки задачи на сервер?
                </a>
            </h4>
        </div>
        <div id="collapse6" class="panel-collapse collapse">
            <div class="panel-body">
                Каждая отправленная задача компилируется на сервере, и в случае успешной компиляции, она проходит 10 тестов. Вы получаете 10 баллов за тест, если: ваш ответ совпадает с нашим ответом, тест уложился во временные рамки и рамки занятой памяти.
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
                    Как понять, что задача решена правильно?
                </a>
            </h4>
        </div>
        <div id="collapse7" class="panel-collapse collapse">
            <div class="panel-body">
                Если задача скомпилировалась удачно и вы набрали 100 баллов, то ваше решение правильное.
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">
                    Как формируется рейтинг?
                </a>
            </h4>
        </div>
        <div id="collapse8" class="panel-collapse collapse">
            <div class="panel-body">
                Рейтинг формируется из суммы баллов наилучших решений задач. Если вы сдали на 1 задаче 2 решения, и одно набрало 50 баллов, а второе 70, то учтется только 70.
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">
                    Что делать, если ответ на вопрос тут не найден?
                </a>
            </h4>
        </div>
        <div id="collapse9" class="panel-collapse collapse">
            <div class="panel-body">
                Высылайте свои вопросы по адресу <?php echo Yii::app()->params->adminEmail;?>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">
                    Как понять то, что выдает нам тестировщик?
                </a>
            </h4>
        </div>
        <div id="collapse10" class="panel-collapse collapse">
            <div class="panel-body">
                В логе задач, напротив каждого теста, Вы можете увидеть следующие сообщения:

                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th style="width: 10%;">Сообщение</th> <th style="width: 45%;">Событие</th> <th style="width: 45%;">Причина</th>
                        </tr>

                        <tr>
                            <td style="color: green">Accepted</td>
                            <td>Задача прошла тест, ваш ответ верен и выуложились в ограничения времени и памяти.</td>
                            <td style="vertical-align:middle" class="text-center">-</td>
                        </tr>


                        <tr>
                            <td style="color: red">Compilation error</td>
                            <td>Задача не прошла компиляцию. Текст ошибки можно увидеть в логе задачи.</td>
                            <td>В коде была допущена синтаксическая ошибка.</td>
                        </tr>

                        <tr>
                            <td style="color: red">Wrong answer</td>
                            <td>Ваш ответ не сходится с ответом Администрации сайта.</td>
                            <td>В коде была допущена логическая ошибка.</td>
                        </tr>

                        <tr>
                            <td style="color: red">Time limit</td>
                            <td>Время выполнения задачи превысил допустимый лимит.</td>
                            <td>Вам нужно изменить алгоритм решения или оптимизировать его.</td>
                        </tr>

                        <tr>
                            <td style="color: red">File doesn't exist</td>
                            <td style="vertical-align:top">Не найден выходной файл.</td>
                            <td>Проверьте, правильно ли вы назвали выходной файл и создается ли он у вас при всех возможных условиях.</td>
                        </tr>

                        <tr>
                            <td>Waiting</td>
                            <td>Ожидание</td>
                            <td>Задача стоит в очереди на компиляцию</td>
                        </tr>

                        <tr>
                            <td>Compiling</td>
                            <td>Компиляция программы</td>
                            <td>Необходимо время для создания исполняемого файла</td>
                        </tr>

                        <tr>
                            <td>Running</td>
                            <td>Тестирование программы</td>
                            <td>Идет тестирование программы путем ее запуска для каждого имеющегося теста</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
