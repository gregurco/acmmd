<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>

    <?php if (isset($this->menu) && !empty($this->menu)): ?>
        <div class="row" id="am_row_content">
            <div class="col-md-2" id="am_left_menu">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items'         => $this->menu,
                    'htmlOptions'   => array('class'=>'nav nav-pills nav-stacked'),
                ));
                ?>
            </div>

            <div class="col-md-10">
                <?php echo $content; ?>
            </div>
        </div>
    <?php else: ?>
        <div class="row" id="am_row_content">
            <div class="col-md-12" id="am_content_1">
                <?php echo $content; ?>
            </div>
        </div>
    <?php endif; ?>
<?php $this->endContent(); ?>