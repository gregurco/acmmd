<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
    <div class="row" id="am_row_content">
        <div class="col-md-12" id="am_content_1">
            <?php echo $content; ?>
        </div>
    </div>
<?php $this->endContent(); ?>