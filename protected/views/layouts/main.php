<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="description" content="Олимпиады по информатике. Новости, задачи, решения. Автоматическая система проверки." />
    <meta name="keywords" content="программирование, задачи по программированию, разбор задач, проверяющая система, acm, асм" />
    <meta name='yandex-verification' content='5588bb78e7752088' />
    <meta name="google-site-verification" content="xih-VBQbOWHyRsJQHEWFn1PpWaYMASeuVBphIpY96xU" />
    <meta name="keywords" content="" />
    <meta name="description" content="Pascal Guide" />

    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="text/javascript">
        var now = new Date(<?php echo date("Y, n-1, j, H ,i, s");?>);

        function clearText(field){
            if (field.defaultValue == field.value) field.value = '';
            else if (field.value == '') field.value = field.defaultValue;
        }

        function ShowTime(){
            now.setTime(now.getTime());
            timer.innerHTML =now.toLocaleString()+"&nbsp;";
            window.setTimeout('ShowTime()',1000);
        }
    </script>

    <!-- Рейтинг майл -->
        <script type="text/javascript">//<![CDATA[
            var _tmr = _tmr || [];
            _tmr.push({id: "2428636",  type: "pageView", start: (new Date()).getTime()});
            (function (d, w) {
                var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true;
                ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
                var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
                if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
            })(document, window);

            //]]>
        </script>
    <!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->

    <?php echo Yii::app()->config->get('SCRIPT.HEADER'); ?>
</head>

<body onload="ShowTime();">
    <div id="templatemo_body_wrapper">
        <div id="templatemo_wrapper">
            <div id="templatemo_header">
                <div id="language_widget">
                    <?php $this->widget('LanguageSwitcherWidget');?>
                </div>

                <div id="site_title">
                    <?php echo CHtml::link(Yii::app()->config->get('MAIN.HEADER'), array(Yii::app()->defaultController.'/index')); ?>
                </div>

                <div id="search_box">
                    <div id="timer" style="color: white;"></div>
                </div>
                <!--
                <div id="search_box">
                    <form action="#" method="get">
                        <input type="text" value="Введите искомое..." name="q" size="10" id="searchfield" title="searchfield" onfocus="clearText(this)" onblur="clearText(this)" />
                      <input type="submit" name="Search" value="Поиск" id="searchbutton" title="Search" />
                    </form>
                </div>
                -->
                <div class="cleaner"></div>
            </div>

            <div class="template_top_menu">
                <?php $this->widget('zii.widgets.CMenu',array(
                    'items'=>array(
                        array('label'=>Yii::t('menu', 'Новости'), 'url'=>array('/news')),
                        array('label'=>Yii::t('menu', 'Личный кабинет'), 'url'=>array('profile/index'),'visible'=>!Yii::app()->user->isGuest),
                        array('label'=>Yii::t('menu', 'Задачи'), 'url'=>array('/problem')),
                        array('label'=>Yii::t('menu', 'Олимпиады'), 'url'=>array('/contest')),
                        array('label'=>Yii::t('menu', 'Статьи'), 'url'=>array('/article')),
                        array('label'=>Yii::t('menu', 'Рейтинг'), 'url'=>array('/top')),
						array('label'=>Yii::t('menu', 'FAQ'), 'url'=>array('/site/FAQ')),
						array('label'=>Yii::t('menu', 'Admin Center'), 'url'=>array('/admin'), 'visible'=>Yii::app()->user->userIsAdmin()),
                        array('label'=>Yii::t('menu', 'Выйти').' ('.Yii::app()->user->name.')', 'url'=>array('/profile/logout'), 'visible'=>!Yii::app()->user->isGuest),
                        array('label'=>Yii::t('menu', 'Войти'), 'url'=>array('/profile/login'), 'visible'=>Yii::app()->user->isGuest),
                    ),
                    'activateParents'=>true,
                    'id' => 'menu',
                )); ?>
            </div>

            <div id="templatemo_main" class="container">
                <?php echo $content; ?>
            </div>

        </div>
        <div class="cleaner"></div>
    </div>


    <div id="templatemo_footer_wrapper">
        <div id="templatemo_footer">
            <div class="col-md-3" style="border-right: 1px solid #131313; height: inherit;">
                <h4><?php echo Yii::t('interface','Читаемые статьи');?>:</h4>
<!--                <ul class="footer_menu"></ul>-->
            </div>

            <div class="col-md-3"  style="border-right: 1px solid #131313;">
                <h4><?php echo Yii::t('interface','Ссылки');?>:</h4>
<!--                <ul class="footer_menu"></ul>-->
            </div>

            <div class="col-md-3" style="border-right: 1px solid #131313;">
                <h4><?php echo Yii::t('interface','Активные сегодня');?>:</h4>
                <?php echo User::getActiveTodayUser();?>
            </div>

            <div class="col-md-3">
                <h4><?php echo Yii::t('interface','Рейтинг');?>:</h4>
                <?php if (!Yii::app()->config->get('DISABLE.BANNERS')):?>
                    <a target="_blank" href="http://top.mail.ru/jump?from=2428636">
                        <img src="//top-fwz1.mail.ru/counter?id=2428636;t=341;l=1" style="border: 0; height:18px; width:88px;" alt="Рейтинг@Mail.ru" />
                    </a>

                    <br/><br/>

                    <script id="oa-script" type="text/javascript">
                        var _oaq = _oaq || [];
                        _oaq.push(['setSiteId', 'MD-3090.74561-1']);
                        _oaq.push(['trackPageView']);
                        _oaq.push(['enableLinkTracking']);
                        (function () {
                            var d=document;var oa=d.createElement('script'); oa.type='text/javascript';
                            oa.async=true; oa.src=d.location.protocol+'//www.ournet-analytics.com/top20md.js';
                            var s=d.getElementsByTagName('script')[0]; s.parentNode.insertBefore(oa, s);})();
                    </script>
                <?php endif; ?>
            </div>
            <div class="cleaner"></div>
        </div>
    </div>

    <div class="clear"></div>

    <div id="templatemo_copyright" style="height: 75px; text-align: center;">
        Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
        All Rights Reserved.<br/>
        <?php echo Yii::powered(); ?>
    </div>

    <?php echo Yii::app()->config->get('SCRIPT.FOOTER'); ?>
</body>
</html>
