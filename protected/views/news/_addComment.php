<?php if(Yii::app()->user->hasFlash('addComment')): ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <?php echo Yii::app()->user->getFlash('addComment'); ?>
    </div>
<?php endif; ?>

<div class="form text-center">
    <?php
    $form = $this->beginWidget(
        'booster.widgets.TbActiveForm',
        array(
            'id' => 'add-comment',
            'htmlOptions' => array('class' => 'well'), // for inset effect
        )
    );
    ?>
        <h2><?php echo Yii::t('interface','Добавить комментарий');?></h2>

        <?php echo $form->html5EditorGroup(
            $newsComment,
            'text',
            array(
                'widgetOptions' => array(
                    'htmlOptions' => array('style'=>'height: 150px;')
                ),
                'label' => Yii::t('interface','Ваш комментарий:')
            )
        ); ?>

        <?php
        if(Yii::app()->user->isGuest)
        {
            echo $form->textFieldGroup(
                $newsComment,
                'name',
                array(
                    'wrapperHtmlOptions' => array('class' => 'text-center'),
                    'label' => Yii::t('interface','Ваше имя:')
                )
            );
        }
        ?>

        <?php if(CCaptcha::checkRequirements()): ?>
            <?php
            echo $form->textFieldGroup(
                $newsComment,
                'verifyCode',
                array(
                    'wrapperHtmlOptions' => array('class' => 'text-center'),
                    'label' => Yii::t('interface','Проверочый код:')
                )
            );
            ?>
            <div class="form-group">
                <?php $this->widget('CCaptcha'); ?>
            </div>
        <?php endif; ?>


        <div class="form-group" style="margin-top: 20px;">
            <?php
            $this->widget(
                'booster.widgets.TbButton',
                array('buttonType' => 'submit', 'label' => Yii::t('interface','Добавить комментарий'))
            );
            ?>
        </div>

    <?php $this->endWidget(); ?>
</div>