<?php
/**
 * @var $this NewsController
 * @var $model News
 * @var $newsComment NewsComment
 * @var $comments NewsComment
 */

?>

<div class="row">
    <div class="col-md-2">
        <div style="padding-top: 12px;">
            <a href="<?php echo $this->createUrl('index'); ?>">
                <button class="btn btn-default">
                    <i class="fa fa-chevron-left"></i> <?php echo Yii::t('interface', 'Назад'); ?>
                </button>
            </a>
        </div>
    </div>
    <div class="col-md-8 text-center">
        <h1><?php echo $model->title; ?></h1>
    </div>
</div>

<div class="text-left well well-lg">
    <?php echo $model->text; ?>
</div>

<h3><?php echo Yii::t('interface','Комментарии');?>(<?php echo $newsComment->model()->countByAttributes(array('n_id'=>$model->id))?>)</h3>

<?php foreach($comments as $comment): ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <?php echo isset($comment->user->login) ? CHtml::link($comment->user->login, array('profile/view', 'id' => $comment->user->id)) : $comment->name; ?>
            <div class="pull-right"><?php echo date("Y-m-d H:i:s", $comment['created']); ?></div>
        </div>
        <div class="panel-body">
            <?php echo $comment->text; ?>
        </div>
    </div>
<?php endforeach; ?>

<?php
if (Yii::app()->config->get('COMMENT.ALLOW')){
    $this->renderPartial('_addComment', array(
        'newsComment'=>$newsComment
    ));
}else{
    $this->widget(
        'booster.widgets.TbPanel',
        array(
            'title' => Yii::t('interface','Предупреждение'),
            'context' => 'warning',
            'headerIcon' => 'glyphicon glyphicon-pencil',
            'content' => Yii::t('interface','Написание комментариев временно запрещено!')
        )
    );
}
?>


