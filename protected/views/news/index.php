<?php
/**
 * @var $dataProvider CActiveDataProvider
 * @var $this NewsController
 */

$this->layout='//layouts/column1';
?>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'itemView'=>'_view',
    'pagerCssClass' => 'text-center',
    'summaryText' => '',
    'pager'=>array(
        'htmlOptions' => array(
            'class' => 'pagination'
        ),
        'header'=>'',
        'maxButtonCount'=>25,
        'selectedPageCssClass'=>'active',
        'hiddenPageCssClass'=>'disabled',
        'firstPageLabel'=>'&lt;&lt;',
        'lastPageLabel'=>'&gt;&gt;',
        'prevPageLabel'=>false,
        'nextPageLabel'=>false,
    ),
));
