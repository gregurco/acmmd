<?php
/* @var $this NewsController */
/* @var $data News */
?>

<div class="panel panel-<?php echo $data->type; ?>">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo CHtml::link(CHtml::encode($data->title), array("view", "id" => $data->id)); ?></h3>
    </div>
    <div class="panel-body">
        <?php echo $data->text; ?>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="pull-left" style="margin-left: 10px;">
                <?php echo CHtml::encode(Yii::t('interface', 'Создан')); ?>: <?php echo CHtml::encode(date("j.m.Y",$data->created)); ?>
            </div>
            <div class="pull-right" style="margin-right: 10px;">
                <?php echo Yii::t('interface','Комментариев');?>: <?php echo NewsComment::model()->countByAttributes(array('n_id'=>$data->id))?>
            </div>
        </div>
    </div>
</div>