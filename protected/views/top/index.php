<?php
/* @var $this SiteController */
/* @var $model CSqlDataProvider */

?>

<h1 class="text-center"><?php echo Yii::t('interface', 'Общий рейтинг');?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'top-grid',
    'dataProvider'=>$model,
    'ajaxUpdate' => true,
    'enableSorting'=>true,
    'filter' => null,
    'columns'=>array(
        array(
            'header' => '№',
            'value'  => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + $row + 1',
            'htmlOptions'=>array('style'=>'width: 20px;'),
        ),
        array(
            'name' => 'login',
            'value' => '$data["login"]',
            'header' => Yii::t('interface','Логин')
        ),
        array(
            'name' => 'score',
            'value' => '($data["score"])?$data["score"]:0',
            'header' => Yii::t('interface','Баллы')
        ),
        array(
            'type'=>'html',
            'value' => 'CHtml::link("'.Yii::t('interface','Просмотреть').'", array("profile/view", "id" => $data["id"]))',
            'header' => Yii::t('interface','Действие')
        ),
    ),
    'itemsCssClass' => 'table table-hover table-bordered',
    'pagerCssClass' => 'text-center',
    'summaryText' => '',
    'pager'=>array(
        'htmlOptions' => array(
            'class' => 'pagination'
        ),
        'header'=>'',
        'maxButtonCount'=>25,
        'selectedPageCssClass'=>'active',
        'hiddenPageCssClass'=>'disabled',
        'firstPageLabel'=>'&lt;&lt;',
        'lastPageLabel'=>'&gt;&gt;',
        'prevPageLabel'=>false,
        'nextPageLabel'=>false,
    ),
)); ?>
