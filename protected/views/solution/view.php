<?php
/* @var $this SolutionController */
/* @var $dataProvider CActiveDataProvider */
/* @var $model Solution */
?>
<div class="row">
    <div class="col-md-5">
        <div style="padding-top: 12px;">
            <a href="<?php echo $this->createUrl('solution/index'); ?>"><button class="btn btn-default">
                <i class="fa fa-chevron-left"></i> <?php echo Yii::t('interface', 'Назад'); ?>
            </button></a>

            <a href="<?php echo $this->createUrl('problem/view', array('id' => $model->p_id)); ?>"><button class="btn btn-default">
                <i class="fa fa-file-text-o"></i> <?php echo Yii::t('interface', 'Описание задачи'); ?>
            </button></a>
        </div>
    </div>
    <div class="col-md-2 text-center">
        <h1>"<?php echo $model->problem ? $model->problem->name : '-'; ?>"</h1>
    </div>
</div>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        array(
            'name' => 'time_send',
            'value' => date("j.m.Y", $model->time_send),
            'label' => Yii::t('interface','Время отправки'),
        ),
        array(
            'name' => 'status',
            'value' => $model->getStatusName(),
            'label' => Yii::t('interface','Статус'),
        ),
        array(
            'name' => 'result',
            'value' => $model->result,
            'label' => Yii::t('interface','Результат'),
        ),
        array(
            'name' => 'compiler',
            'value' => $model->compiler,
            'label' => Yii::t('interface','Компилятор'),
        ),
        array(
            'name' => 'log_compile',
            'value' => $model->log_compile,
            'label' => Yii::t('interface','Лог компилятора'),
            'type' => 'html',
        ),
    ),
));?>
<br>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title text-center">
            <?php echo Yii::t('interface','Тесты');?>
        </h3>
    </div>
    <div class="panel-body">
        <table class="table table-hover">
            <tr class="active">
                <td><b><?php echo Yii::t('interface','Тест');?></b></td>
                <td><b><?php echo Yii::t('interface','Результат');?></b></td>
            </tr>
            <?php for ($i=1; $i <= ($model->problem ? $model->problem->tests : 10); $i++): ?>
                <tr class="<?php echo $model->getTrClassByResult($i);?>">
                    <td><?php echo $i; ?></td>
                    <td><?php echo (!empty($model->tests) && array_key_exists($i, $model->tests)) ? $model->getResultStatusName($i) . ' ('.$model->tests[$i][1].' с.)' : "-"; ?></td>
                </tr>
            <?php endfor; ?>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title text-center">
            <?php echo Yii::t('interface','Код');?>
        </h3>
    </div>
    <div class="panel-body" style="padding: 0;">
        <pre class="brush : <?php echo $model->getLanguageByCompiler(); ?>"><?php echo CHtml::encode($model->file_text); ?></pre>
    </div>
    <div class="panel-footer">
        <button id="copy-button" data-clipboard-text="<?php echo CHtml::encode($model->file_text);?>" tabindex="0" class="btn btn-default" data-toggle="popover" data-trigger="focus" data-content="<?php echo Yii::t('interface','Код был скопирован');?>"><?php echo Yii::t('interface','Скопировать код');?></button>
        <button id="download-button" class="btn btn-default"><a href="<?php echo $this->createUrl('solution/download', array('id' => $model->id));?>"><?php echo Yii::t('interface','Скачать код');?></a></button>
    </div>
</div>

<?php if ($model->getNotification()): ?>
    <div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-info-sign"></span> <?php echo $model->getNotification();?></div>
<?php endif; ?>

<script>
    var client = new ZeroClipboard( $("#copy-button") );
</script>
