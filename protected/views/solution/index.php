<?php
/* @var $this SolutionController */
/* @var $dataProvider CActiveDataProvider */
/* @var $model Solution */

$this->menu = array(
    array('label'=>Yii::t('interface', 'Список задач'), 'url'=>array('problem/index')),
    array('label'=>Yii::t('interface', 'Отправленные решения'), 'url'=>array('solution/index')),
);
?>

<h1 class="text-center"><?php echo Yii::t('interface','Отправленные решения');?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'            => 'solution-grid',
    'dataProvider'  => $model->searchPlainForUser(),
    'filter'        => $model,
    'columns'       => array(
        array(
            'name'  => 'id',
            'value' => '$data->id',
            'header'=> 'ID',
        ),
        array(
            'name'  => 'p_id',
            'value' => '$data->problem ? $data->problem->name : \'-\'',
            'header'=> Yii::t('interface','Задача'),
            'filter'=> CHtml::listData(Problem::model()->findAll(),'id','name'),
        ),
        array(
            'name'  => 'time_send',
            'value' => 'date("j.m.Y",$data->time_send)',
            'header'=> Yii::t('interface','Время отправки'),
        ),
        array(
            'name'  => 'status',
            'value' => '$data->getStatusName()',
            'header'=> Yii::t('interface','Статус'),
            'filter'=> array(
                Solution::STATUS_WAITING_COMPILING  => Yii::t('interface','Ожидание'),
                Solution::STATUS_COMPILING          => Yii::t('interface','Компилирование'),
                Solution::STATUS_WAITING_TESTING    => Yii::t('interface','Ожидание тестирования'),
                Solution::STATUS_TESTING            => Yii::t('interface','Тестирование'),
                Solution::STATUS_FINISH             => Yii::t('interface','Завершено'),
                Solution::STATUS_COMPILE_ERROR      => Yii::t('interface','Ошибка компиляции'),
            )
        ),
        array(
            'name'  => 'compiler',
            'value' => '$data->compiler',
            'header'=> Yii::t('interface','Компилятор'),
            'filter'=> CHtml::listData($model->findAll(), 'compiler', 'compiler'),
        ),
        array(
            'name'  => 'result',
            'value' => '$data->result',
            'header'=> Yii::t('interface','Результат'),
        ),
        array(
            'class'     => 'CButtonColumn',
            'template'  => '{view}',
        ),
    ),
    'itemsCssClass' => 'table table-hover table-bordered',
    'pagerCssClass' => 'text-center',
    'summaryText'   => '',
    'pager'         =>array(
        'htmlOptions' => array(
            'class' => 'pagination'
        ),
        'header'                => '',
        'maxButtonCount'        => 25,
        'selectedPageCssClass'  => 'active',
        'hiddenPageCssClass'    => 'disabled',
        'firstPageLabel'        => '&lt;&lt;',
        'lastPageLabel'         => '&gt;&gt;',
        'prevPageLabel'         => false,
        'nextPageLabel'         => false,
    ),
    'afterAjaxUpdate' => 'function(){$(\'input[name="Solution[time_send]"]\').datepicker({format: \'dd.mm.yyyy\', autoclose: true});}',
)); ?>

<script>
    $(function(){
        $('input[name="Solution[time_send]"]').datepicker({format: 'dd.mm.yyyy', autoclose: true});
    });

    $(document).ajaxStart(function () {
        $("#solution-grid")
            .find("thead *[name^='Solution']").attr("readonly", true).end()
            .find("thead select[name^='Solution']").attr("disabled", true);
    });
    $(document).ajaxComplete(function () {
        $("#solution-grid")
            .find("thead *[name^='Solution']").attr("readonly", false).end()
            .find("thead select[name^='Solution']").attr("disabled", false);
    });
</script>