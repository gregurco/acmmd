<?php
/**
 * User: Gregurco Vlad
 * Date: 06.11.14
 * Time: 23:04
 */

return array(
    'user' => array(
        'login'     => 'user',
        'password'  => md5('password'),
        'email'     => 'user@mail.local',
        'name'      => 'John',
        'surname'   => 'Smith',
        'admin'     => 0,
        'active'    => 1
    ),
    'user_not_active'   => array(
        'login'         => 'user_not_active',
        'password'      => md5('password_not_active'),
        'email'         => 'user_not_active@mail.local',
        'name'          => 'Tim',
        'surname'       => 'Cook',
        'admin'         => 0,
        'active'        => 0
    ),
    'admin' => array(
        'login'     => 'admin',
        'password'  => md5('password_admin'),
        'email'     => 'admin@mail.local',
        'name'      => 'Kinny',
        'surname'   => 'West',
        'admin'     => 1,
        'active'    => 1
    ),
);