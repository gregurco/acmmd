<?php

/**
 * Change the following URL based on your server configuration
 * Make sure the URL ends with a slash so that we can use relative URLs in test cases
 */
define('TEST_BASE_URL',         'http://acmmd.local');
define('TEST_BASE_INDEX',       'index-test.php');
define('TEST_BASE_SCREENSHOTS', TEST_BASE_URL.'/protected/tests/report/screenshots');

/**
 * The base class for functional test cases.
 * In this class, we set the base URL for the test application.
 * We also provide some common methods to be used by concrete test classes.
 */
class WebTestCase extends CWebTestCase
{
    protected $captureScreenshotOnFailure = true;
    protected $screenshotPath = '/Users/home/PhpstormProjects/AcmMd/protected/tests/report/screenshots/';
    protected $screenshotUrl = TEST_BASE_SCREENSHOTS;

    /**
	 * Sets up before each test method runs.
	 * This mainly sets the base URL for the test application.
	 */
	protected function setUp()
	{
		parent::setUp();
		$this->setBrowserUrl(TEST_BASE_URL . '/' . TEST_BASE_INDEX . '/');
	}
}
