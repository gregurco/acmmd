<?php
/**
 * User: Gregurco Vlad
 * Date: 06.11.14
 * Time: 22:59
 */

class ProfileTest extends WebTestCase
{
    public $fixtures=array(
        'users'=>':a_user',
    );

    public function testLogin(){
        $this->open('profile/login');
        $this->assertTextPresent('Авторизация');
        $this->assertTextPresent('Зарегистрироваться');
        $this->assertTextPresent('Восстановить пароль');
        $this->assertTextPresent('Войти');

        $this->assertElementPresent('name=LoginForm[username]');
        $this->assertElementPresent('name=LoginForm[password]');
        $this->assertElementPresent('name=LoginForm[rememberMe]');

        $this->type('name=LoginForm[username]', 'user');
        $this->type('name=LoginForm[password]', 'wrongPassword');
        $this->clickAndWait("//button[@type='submit']");
        $this->assertTextPresent('Incorrect username or password.');

        $this->type('name=LoginForm[username]', 'user_not_active');
        $this->type('name=LoginForm[password]', 'password_not_active');
        $this->clickAndWait("//button[@type='submit']");
        $this->assertTextPresent(Yii::t('interface','Пользователь не активен. Для восстановления свяжитесь с администрацией по e-mail ' . Yii::app()->params->adminEmail));

        $this->type('name=LoginForm[username]', 'user');
        $this->type('name=LoginForm[password]', 'password');
        $this->clickAndWait("//button[@type='submit']");
        $this->assertTextNotPresent('Incorrect username or password.');
        $this->assertElementNotPresent("//a[@href='/" . TEST_BASE_INDEX . "/admin']");
        $this->clickAndWait("//a[@href='/" . TEST_BASE_INDEX . "/profile/logout']");

        $this->open('profile/login');
        $this->type('name=LoginForm[username]', 'admin');
        $this->type('name=LoginForm[password]', 'password_admin');
        $this->clickAndWait("//button[@type='submit']");
        $this->assertTextNotPresent('Incorrect username or password.');
        $this->assertElementPresent("//a[@href='/" . TEST_BASE_INDEX . "/admin']");
    }
}