<?php

class SiteTest extends WebTestCase
{
	public function testFAQ()
	{
		$this->open('site/FAQ');
        $this->assertTextPresent('FAQ');
	}

	public function testError()
	{
		$this->open('site/page_not_found');
        $this->assertTextPresent('Error 404');
	}
}